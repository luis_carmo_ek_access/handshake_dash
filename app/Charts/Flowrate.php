<?php

namespace App\Charts;

use ConsoleTVs\Charts\Classes\Chartjs\Chart;

class Flowrate extends Chart
{
    /**
     * Initializes the chart.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function api() {


        $chart = new Flowrate();

        $chart->labels(['test1', 'test2', 'test3']);
        $chart->dataset('Sample Test', 'line', [rand(0, 100),rand(0, 100),rand(0, 100)])->color('#ff0000');

        return view('chart_view', ['chart' => $chart]);

    }

}
