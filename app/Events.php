<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Events extends Model
{
    protected $connection = 'access';
    protected $table = 'gestacess_evento';
}
