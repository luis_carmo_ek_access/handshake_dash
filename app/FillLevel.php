<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FillLevel extends Model 
{
    protected $connection = 'access';
    protected $table = 'gestacess_transacoes';
}