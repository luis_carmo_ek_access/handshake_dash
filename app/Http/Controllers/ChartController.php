<?php
/**
 * Created by PhpStorm.
 * User: luis
 * Date: 15-07-2018
 * Time: 19:57
 */

namespace App\Http\Controllers;

use App\Charts\Flowrate;
use ConsoleTVs\Charts\Classes\Chartjs\Chart;
use Illuminate\Http\Request;

use Charts;

class ChartController extends Controller
{

    /**
     * Show a sample chart.
     *
     * @return Response
     */

    /**
    *   @return Flowrate
    */

    public function getFlowrate() {

        return [rand(0, 100), rand(0, 100), rand(0, 100), rand(0, 100), rand(0, 100), rand(0, 100), rand(0, 100)]; 

    }

    public function chart() {

        //Additional logic depending on the chart approach

        $chart = new Flowrate();

        $api = url('/test_data');
        $chart->labels(['test1', 'test2', 'test3'])
            ->load($api);
        //$chart->dataset('Sample Test', 'line', [65, 10, 15])->color('#ff0000');

        return view('chart_view', ['chart' => $chart]);

    }

    public function response() {

        $chart = new Flowrate();
        $chart->dataset('Sample Test', 'bar', [3,4,1])->color('#00ff00');
        $chart->dataset('Sample Test', 'line', [1,4,3])->color('#ff0000');
        return $chart->api();

    }

    public function index()
    {

        /**
         * Create Random Data to Line Chart
         * 
         * @return LineChart;
         */


        $chartjsline = app()->chartjs
        ->name('flowrateChart')
        ->type('line')
        ->size(['width' => 400, 'height' => 200])
        ->labels(['18h', '19h', '20h', '21h', '22h', '23h', '24h'])
        ->datasets([])
        ->options([]);

        /* BAR CHART */

        $chartjsbar = app()->chartjs
            ->name('barChartTest')
            ->type('bar')
            ->size(['width' => 400, 'height' => 200])
            ->labels(['Label x', 'Label y'])
            ->datasets([
                [
                    "label" => "My First dataset",
                    'backgroundColor' => ['rgba(255, 99, 132, 0.2)', 'rgba(54, 162, 235, 0.2)'],
                    'data' => [69, 59]
                ],
                [
                    "label" => "My First dataset",
                    'backgroundColor' => ['rgba(255, 99, 132, 0.3)', 'rgba(54, 162, 235, 0.3)'],
                    'data' => [65, 12]
                ]
            ])
            ->options([]);

        //return view('cards.chart-graph-card', compact('chartjs'));



        /* Pie Chart / Doughnut Chart */

        $chartjs = app()->chartjs
            ->name('pieChart')
            ->type('doughnut')
            ->size(['width' => 400, 'height' => 200])
            ->labels(['Label x', 'Label y'])
            ->datasets([
                [
                    'backgroundColor' => ['#FF6384', '#36A2EB'],
                    'hoverBackgroundColor' => ['#FF6384', '#36A2EB'],
                    'data' => [69, 59]
                ]
            ])
            ->options([]);


        /**
        * @return flowrate
        */

        $flowratejs = app()->chartjs
            ->name('pieChart')
            ->type('doughnut')
            ->size(['width' => 400, 'height' => 200])
            ->labels(['Flowrate', 'Current'])
            ->datasets([
                'backgroundColor' => ['#00A3E5', '#36A2EB'],
                'hoverBackgroundColor' => ['#00A3F9', '#36A2EB'],
                'data' => [rand(0, 50), 12]
            ])
            ->options([]);


        $current_flowrate = rand(0, 100);

        return view('cards.chart-graph-card', compact('chartjs', 'chartjsbar', 'chartjsline'));

    }

}
