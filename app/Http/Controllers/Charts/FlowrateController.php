<?php 

namespace App\Http\Controllers\Charts;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Exception\GuzzleException;
use Mockery\Exception;
use App\Flowrate;

class FlowrateController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getFlowrate(Request $request, $event_id) {

        try {

            /**
             * 
             * @return flowrate
             * 
             */

            $options = DB::connection('access')->select("
                SELECT
                    dataInicio,
                    dataFim,
                    horaAberturaPortas
                FROM
                    gestacess_evento
                WHERE
                    id = '" . (int)$event_id . "'
            ");

            $event_options = (array) $options[0];

            $start_date = new \DateTime($event_options['dataInicio'] . ' ' . $event_options['horaAberturaPortas']);
            $time_zone  = new \DateTimeZone('Europe/Lisbon');
            $date_now   = new \DateTime(date("Y-m-d H:i:s"), $time_zone);

            $interval = $date_now->diff($start_date);

            $diff_day       = $interval->d;
            $diff_hour      = $interval->h;
            $diff_minute    = $interval->i;
            $diff_seconds   = $interval->s;

            $seconds_filter = 0;

            if ($diff_hour <= 1) {

                if ($diff_hour == 1) {

                    $seconds_filter = 600;

                } else {

                    if ($diff_minute <= 30) {

                        $seconds_filter = 300;

                    }

                }

            } else {

                $seconds_filter = ((($diff_day * 24) * 60) * 60);
                
            }

            $less_minutes = $seconds_filter / 60;

            $start_graph_at = new \DateTime($event_options['dataInicio'] . ' ' . $event_options['horaAberturaPortas']);
            $start_graph_at = $start_graph_at->modify("-" . $less_minutes . " minutes");

            $day_now   = date("Y-m-d");
            $hour_now  = date("H:i:s");

            $flowrate_value = DB::connection('access')->select("
                SELECT
                    DATE_FORMAT(
                        MIN(cast(concat(gestacess_transacoes.dataTransac, ' ', gestacess_transacoes.horaTransac) as datetime)), '%Y-%m-%d %H:%i:00'
                    ) AS 'hour_transaction',
                    COUNT(DISTINCT gestacess_transacoes.titulo) AS 'entries'
                FROM
                    gestacess_transacoes
                    LEFT JOIN gestacess_evento ON (
                        gestacess_transacoes.eventId_id = gestacess_evento.id
                    )
                    LEFT JOIN gestacess_template_inst ON (
                        gestacess_transacoes.template_inst_id = gestacess_template_inst.id
                        AND
                        gestacess_template_inst.id = gestacess_transacoes.template_inst_id
                    )
                    LEFT JOIN gestacess_dac ON (
                        gestacess_dac.id = gestacess_template_inst.dac_id
                    )
                    LEFT JOIN gestacess_regras_lista_ticket ON (
                        gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo
                        AND
                        gestacess_regras_lista_ticket.VA1 = gestacess_evento.code
                    )
                    LEFT JOIN gestacess_valores_variaveis_acesso ON (
                        gestacess_valores_variaveis_acesso.valor = gestacess_regras_lista_ticket.VA3
                        AND
                        gestacess_valores_variaveis_acesso.variavelAcesso_id = '3'
                    )
                WHERE
                    gestacess_evento.id = '" . (int)$event_id . "'
                    AND
                    gestacess_evento.code = gestacess_regras_lista_ticket.evento
                    AND
                    gestacess_transacoes.eventId_id = gestacess_evento.id
                    AND
                    gestacess_transacoes.entry = '1'
                    AND
                    gestacess_transacoes.apagado = '0'
                    AND
                    gestacess_transacoes.isEntry = '1'
                    AND
                    gestacess_transacoes.dataTransac >= '" . $event_options['dataInicio'] . "'
                    AND
                    gestacess_transacoes.dataTransac <= '" . $event_options['dataFim'] .  "'
                    AND
                    gestacess_transacoes.dataTransac <= '" . $day_now . "'
                    AND
                    gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                    AND
                    gestacess_transacoes.testMode = '0'
                    AND
                    gestacess_transacoes.areaEntryId <> '330'
                GROUP BY
                     DATE_FORMAT(concat(gestacess_transacoes.dataTransac, ' ', gestacess_transacoes.horaTransac), '%Y-%m-%d %H:%i:00')
            "); // UNIX_TIMESTAMP(gestacess_transacoes.horaTransac) DIV " . $less_minutes . "

            return response()->json(['flowrate' => $flowrate_value], 200);

        } catch (\Exception $e) {

            Log::error('Flowrate Request Error');
            Log::error($e);

            return response()->json(['error' => 'unavailable'], 200);

        }

    }

    public function getFlowrateFiltered(Request $request, $event_id, $filter_type) {

        $tickets_filter = null;
        $hours_filter = array();
        $filter = '';
        $search_type = '';
        
        $selection = '';
        $group_by = '';
        $order_by = '';

        try {

            $options = DB::connection('access')->select("
                SELECT
                    dataInicio,
                    dataFim,
                    horaAberturaPortas
                FROM
                    gestacess_evento
                WHERE
                    id = '" . (int)$event_id . "'
            ");

            $event_options = (array) $options[0];

            if ($filter_type == 'tickettype') {

                $this->validate($request, [
                    'ticket_types'  => 'required'
                ]);

                $tickets_filter = $request['ticket_types'];

                $selection = "
                    gestacess_valores_variaveis_acesso.id AS 'ticket_type_id',
                    gestacess_valores_variaveis_acesso.descricao AS 'tickettype',
                ";

                $group_by = "
                    gestacess_valores_variaveis_acesso.id,
                    gestacess_valores_variaveis_acesso.descricao,
                ";

                $order_by = "
                    ORDER BY
                    gestacess_valores_variaveis_acesso.descricao,
                    hour_transaction
                ";

            } else if ($filter_type == 'hour') {

                $this->validate($request, [
                    'hours'         => 'required',
                    'type_search'   => 'required'
                ]);

                $hours_filter = explode(',', $request['hours']);
                $search_type = $request['type_search'];

                if ($search_type == 'tickettype') {

                    $selection = "
                        gestacess_valores_variaveis_acesso.id AS 'ticket_type_id',
                        gestacess_valores_variaveis_acesso.descricao AS 'tickettype',
                    ";

                    $group_by = "
                        gestacess_valores_variaveis_acesso.id,
                        gestacess_valores_variaveis_acesso.descricao,
                    ";

                    $order_by = "
                        ORDER BY
                        gestacess_valores_variaveis_acesso.descricao,
                        hour_transaction
                    ";

                }

            }

            /**
             * 
             * @return flowrate
             * 
             */

            $seconds_filter = 0;

            if (count($hours_filter) > 0 && $hours_filter !== null) {

                $start_filter = new \DateTime($hours_filter[0]);
                $end_filter = new \DateTime($hours_filter[1]);

                $interval = $end_filter->diff($start_filter);

                $diff_day = $interval->d;
                $diff_hour = $interval->h;
                $diff_minute = $interval->i;
                $diff_seconds = $interval->s;

                if ($diff_hour <= 1) {

                    if ($diff_hour == 1) {

                        $seconds_filter = 600;

                    } else {

                        if ($diff_minute <= 30) {

                            $seconds_filter = 300;

                        }

                    }

                } else {

                    if ($diff_day >= 1) {

                        $seconds_filter = ((($diff_day * 24) * 60) * 60);

                    } else {

                        $seconds_filter = (($diff_minute * 60) * 60);

                    }

                }

            } else {

                $start_date = new \DateTime($event_options['dataInicio'] . ' ' . $event_options['horaAberturaPortas']);
                $time_zone = new \DateTimeZone('Europe/Lisbon');
                $date_now = new \DateTime(date("Y-m-d H:i:s"), $time_zone);

                $interval = $date_now->diff($start_date);

                $diff_day = $interval->d;
                $diff_hour = $interval->h;
                $diff_minute = $interval->i;
                $diff_seconds = $interval->s;

                if ($diff_hour <= 1) {

                    if ($diff_hour == 1) {

                        $seconds_filter = 600;

                    } else {

                        if ($diff_minute <= 30) {

                            $seconds_filter = 300;

                        }

                    }

                } else {

                    if ($diff_day >= 1) {

                        $seconds_filter = ((($diff_day * 24) * 60) * 60);

                    } else {

                        $seconds_filter = (($diff_minute * 60) * 60);

                    }

                }

            }

            $less_minutes = $seconds_filter / 60;

            $start_graph_at = new \DateTime($event_options['dataInicio'] . ' ' . $event_options['horaAberturaPortas']);
            $start_graph_at = $start_graph_at->modify("-" . $less_minutes . " minutes");

            $day_now   = date("Y-m-d");
            $hour_now  = date("H:i:s");

            if (strlen($tickets_filter) > 0 && $tickets_filter !== null) {

                $filters = "
                    AND
                    gestacess_transacoes.dataTransac >= '" . $event_options['dataInicio'] . "'
                    AND
                    gestacess_transacoes.dataTransac <= '" . $day_now . "'
                    AND
                    gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                    AND
                    gestacess_valores_variaveis_acesso.id IN (" . $tickets_filter . ")
                ";
    
            } else if (count($hours_filter) > 0 && $hours_filter !== null) {
    
                $filters = "
                    AND
                    TIMESTAMP(gestacess_transacoes.dataTransac, gestacess_transacoes.horaTransac) BETWEEN '" . $hours_filter[0] . "' AND '" . $hours_filter[1] . "'
                ";
    
            } else {
    
                $filters = "
                    AND
                    gestacess_transacoes.dataTransac >= '" . $event_options['dataInicio'] . "'
                    AND
                    gestacess_transacoes.dataTransac <= '" . $day_now . "'
                    AND
                    gestacess_transacoes.horaTransac >= '" . $event_options['horaAberturaPortas'] . "'
                ";
    
            }

            $flowrate_value_filtered = DB::connection('access')->select("
                SELECT
                    " . $selection . "
                    DATE_FORMAT(
                        MIN(cast(concat(gestacess_transacoes.dataTransac, ' ', gestacess_transacoes.horaTransac) as datetime)), '%Y-%m-%d %H:%i:00'
                    ) AS 'hour_transaction',
                    COUNT(DISTINCT gestacess_transacoes.titulo) AS 'entries'
                FROM
                    gestacess_transacoes
                    LEFT JOIN gestacess_evento ON (
                        gestacess_transacoes.eventId_id = gestacess_evento.id
                    )
                    LEFT JOIN gestacess_template_inst ON (
                        gestacess_transacoes.template_inst_id = gestacess_template_inst.id
                        AND
                        gestacess_template_inst.id = gestacess_transacoes.template_inst_id
                    )
                    LEFT JOIN gestacess_dac ON (
                        gestacess_dac.id = gestacess_template_inst.dac_id
                    )
                    LEFT JOIN gestacess_regras_lista_ticket ON (
                        gestacess_transacoes.titulo = gestacess_regras_lista_ticket.titulo
                        AND
                        gestacess_regras_lista_ticket.VA1 = gestacess_evento.code
                    )
                    LEFT JOIN gestacess_valores_variaveis_acesso ON (
                        gestacess_valores_variaveis_acesso.valor = gestacess_regras_lista_ticket.VA3
                        AND
                        gestacess_valores_variaveis_acesso.variavelAcesso_id = '3'
                    )
                WHERE
                    gestacess_evento.id = '" . (int)$event_id . "'
                    AND
                    gestacess_evento.code = gestacess_regras_lista_ticket.evento
                    AND
                    gestacess_transacoes.eventId_id = gestacess_evento.id
                    AND
                    gestacess_transacoes.entry = '1'
                    AND
                    gestacess_transacoes.apagado = '0'
                    AND
                    gestacess_transacoes.isEntry = '1'
                    AND
                    gestacess_transacoes.testMode = '0'
                    AND
                    gestacess_transacoes.areaEntryId <> '330'
                    " . $filters . "
                GROUP BY
                    " . $group_by . "
                     DATE_FORMAT(concat(gestacess_transacoes.dataTransac, ' ', gestacess_transacoes.horaTransac), '%Y-%m-%d %H:%i:00')
                " . $order_by . "
            "); // UNIX_TIMESTAMP(gestacess_transacoes.horaTransac) DIV " . $less_minutes . "

            if ($filter_type == 'hour' && $search_type == 'default') {
                return response()->json(['flowrate' => $flowrate_value_filtered], 200);
            }

            $chart_tickets = [];
            $chart_labels = [];
            
            $chart_values = [];

            $ticket_number = -1;

            foreach($flowrate_value_filtered as $flowrate) {

                if (!in_array($flowrate->hour_transaction, $chart_labels)) {

                    $chart_labels[] = $flowrate->hour_transaction;

                }

                if (!in_array($flowrate->tickettype, $chart_values)) {

                    $ticket_number++;

                    $chart_values[$ticket_number] = $flowrate->tickettype;

                    $chart_tickets[$ticket_number] = new \stdClass();
                    
                    $chart_tickets[$ticket_number]->data = [];
                    array_push( $chart_tickets[$ticket_number]->data, $flowrate->entries);

                    $chart_tickets[$ticket_number]->label = $flowrate->tickettype;
                    $chart_tickets[$ticket_number]->borderColor = sprintf("#%06x",rand(0,16777215));;
                    $chart_tickets[$ticket_number]->fill = false;

                } else {

                    array_push($chart_tickets[$ticket_number]->data, $flowrate->entries);

                }

            }

            return response()->json(['chart_labels' => $chart_labels, 'dataset' => $chart_tickets], 200);

        } catch (\Exception $e) {

            Log::error('Flowrate Request Error');
            Log::error($e);

            return response()->json(['error' => 'unavailable'], 200);

        }

    }

}