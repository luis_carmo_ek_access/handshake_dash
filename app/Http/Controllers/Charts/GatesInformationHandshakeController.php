<?php

namespace App\Http\Controllers\Charts;

use App\Http\Controllers\Controller;
use App\Http\Controllers\InstallationsController;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Mockery\Exception;
use App\Flowrate;

class GatesInformationHandshakeController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct() {
        $this->middleware('auth');
    }

    private function getInstallationData($user_id) {

        $user = User::findOrFail($user_id);

        $currentInstallation = $user->installation_id;

        $installation = (new InstallationsController)->getInstallation($currentInstallation);

        return $installation;

    }

    private function getUserVenue($user_id) {

        $user = User::findOrFail($user_id);

        return $user->venue_id;

    }

    public function getGatesCounters() {

        try {

            $userId = Auth::id();

            $uri = $this->getInstallationData($userId);

            $venue_id = $this->getUserVenue($userId);

            $client = new \GuzzleHttp\Client([
                'base_uri' => $uri[0]->host . ':' . $uri[0]->port,
                'timeout' => 1.0,
                'auth' => [$uri[0]->username, $uri[0]->password],
                'verify' => (($uri[0]->verify_certificate == '0') ? false : true)
            ]);

            $request_uri = $uri[0]->host . ':' . $uri[0]->port . '/venue/' . $venue_id . '/gates/counters';

            $request = $client->request('GET', $request_uri);

            $body = $request->getBody();
            $content = $body->getContents();

            if (!$request->getStatusCode() == 200 && $content !== true) {

                return array('error' => 'unavailable');

            } elseif ($request->getStatusCode() == 200 && $content == true) {

                return json_decode($content, true);

            }

        } catch (\Exception $e) {

            return []; //response()->json(['error' => 'unavailable'], 200);

        }

    }

}