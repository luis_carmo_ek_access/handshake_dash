<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use PDO;

class InstallationsController extends Controller {


    public function __construct()
    {

        $this->middleware('auth');

    }

    public function getInstallations() {

        $installations = DB::connection('mysql')->select("
            SELECT
                installation_id,
                code,
                name
            FROM
                installations
            WHERE
                status = 1
        ");

        return $installations;

    }

    public function getInstallation($installation_id) {

        $installation = DB::connection('mysql')->select("
            SELECT
                *
            FROM
                installations
            WHERE
                installation_id = '" . $installation_id . "'
        ");

        return $installation;

    }

}