<?php
/**
 * Created by PhpStorm.
 * User: luis
 * Date: 02-10-2018
 * Time: 13:08
 */

namespace App\Http\Controllers\Transactions;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Mockery\Exception;
use App\FillLevel;
use Illuminate\Pagination\LengthAwarePaginator;

class Search extends Controller {

    /**
     * Create a new controller instance...
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    private function getAllTransactionsList($event_id) {

        $transactions = DB::connection('access')->select("
            SELECT
                trans.id,
                trans.titulo,
                CONCAT(
                    trans.dataTransac,
                    ' ',
                    trans.horaTransac
                ) AS date,
                trans.isEntry,
                trans.template_inst_id,
                erro.id AS erro_id,
                erro.descricao AS erro_descricao,
                erro.mensagem1Dac AS erro_mensagem1Dac,
                erro.mensagem2Dac AS erro_mensagem2Dac,
                erro.mensagemOperador AS erro_mensagemOperador,
                temp.id AS zonaID,
                temp.descricao AS zonaDescricao,
                dac.id AS realDacId,
                dac.descricao AS dacDescricao
            FROM
                gestacess_transacoes AS trans
            LEFT JOIN gestacess_codigosmensagem AS erro
            ON
                erro.id = trans.erro_id
            LEFT JOIN gestacess_template_inst AS temp
            ON
                temp.id = trans.areaEntryId
                OR 
                temp.id = trans.areaExitId
            LEFT JOIN gestacess_dac AS dac
            ON
                dac.id = (
                    SELECT
                        dac_id
                    FROM
                        gestacess_template_inst
                    WHERE
                        gestacess_template_inst.id = trans.template_inst_id
                )
            WHERE
                trans.eventId_id = '" . $event_id  ."'
                AND 
                trans.testMode = 0
            ORDER BY
                trans.id
            DESC
        ");

        return (!empty($transactions)) ? $transactions : array();

    }

    public function getAllTransactions(Request $request, $event_id) {

        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $perPage = 10;

        $col = new Collection($this->getAllTransactionsList($event_id));

        $currentPageSearchResults = $col->slice(($currentPage - 1) * $perPage, $perPage)->all();

        $paginatedSearchResults = new LengthAwarePaginator(array_values($currentPageSearchResults), count($col), $perPage);
        $paginatedSearchResults->setPath($request->url());

        return response()->json($paginatedSearchResults, 200);

    }

    public function searchTicket(Request $request, $event_id, $search_type) {

        try {

            $query_compose = "
                gestacess_transacoes.eventoId_id = '" . (int)$event_id . "'
            ";

            if ($search_type == 'barcode') {

                $this->validate($request, [
                    'barcode' => 'required'
                ]);

                $barcode = $request['barcode'];

                $transactions = DB::connection('access')->select("
                    SELECT
                        trans.id,
                        trans.titulo,
                        CONCAT(
                            trans.dataTransac,
                            ' ',
                            trans.horaTransac
                        ) AS date,
                        trans.isEntry,
                        trans.template_inst_id,
                        erro.id AS erro_id,
                        erro.descricao AS erro_descricao,
                        erro.mensagem1Dac AS erro_mensagem1Dac,
                        erro.mensagem2Dac AS erro_mensagem2Dac,
                        erro.mensagemOperador AS erro_mensagemOperador,
                        temp.id AS zonaID,
                        temp.descricao AS zonaDescricao,
                        dac.id AS realDacId,
                        dac.descricao AS dacDescricao
                    FROM
                        gestacess_transacoes AS trans
                    LEFT JOIN gestacess_codigosmensagem AS erro
                    ON
                        erro.id = trans.erro_id
                    LEFT JOIN gestacess_template_inst AS temp
                    ON
                        temp.id = trans.areaEntryId
                        OR 
                        temp.id = trans.areaExitId
                    LEFT JOIN gestacess_dac AS dac
                    ON
                        dac.id = (
                            SELECT
                                dac_id
                            FROM
                                gestacess_template_inst
                            WHERE
                                gestacess_template_inst.id = trans.template_inst_id
                        )
                    WHERE
                        trans.eventId_id = '" . $event_id  ."' 
                        AND 
                        trans.titulo = '" . $barcode . "' 
                        AND 
                        trans.testMode = 0
                    ORDER BY
                        trans.id
                    DESC
                ");

                return response()->json($transactions, 200);

            } else {

                if ($search_type == 'member') {

                } else if ($search_type == 'non_member') {

                } else if ($search_type == 'stand_row_seat') {

                }

            }

        } catch (\Exception $e) {

            return response()->json(['error' => 'unavailable'], 200);

        }

    }

}