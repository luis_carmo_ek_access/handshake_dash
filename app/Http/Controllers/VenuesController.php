<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use PDO;

class VenuesController extends Controller {

    public function __construct()
    {

        $this->middleware('auth');

    }

    private function getInstallationData($user_id) {

        $user = User::findOrFail($user_id);

        $currentInstallation = $user->installation_id;

        $installation = (new InstallationsController)->getInstallation($currentInstallation);

        return $installation;

    }

    public function getVenues() {

        try {

            $userId = Auth::id();

            $uri = $this->getInstallationData($userId);

            $client = new \GuzzleHttp\Client([
                'base_uri' => $uri[0]->host . ':' . $uri[0]->port,
                'timeout' => 1.0,
                'auth' => [$uri[0]->username, $uri[0]->password],
                'verify' => (($uri[0]->verify_certificate == '0') ? false : true)
            ]);

            $request_uri = $uri[0]->host . ':' . $uri[0]->port . '/venues/associate';

            $request = $client->request('GET', $request_uri);

            $body = $request->getBody();
            $content = $body->getContents();

            if (!$request->getStatusCode() == 200 && $content !== true) {

                return array('error' => 'unavailable');

            } elseif ($request->getStatusCode() == 200 && $content == true) {

                return json_decode($content, true);

            }

        } catch (\Exception $e) {

            return []; //response()->json(['error' => 'unavailable'], 200);

        }

    }

    public function getVenueLayouts(Request $request) {

        try {

            $user_id = $userId = Auth::id();

            $user = User::findOrFail($user_id);

            $currentVenue = $user->venue_id;

            $layouts = array();

            $client = new \GuzzleHttp\Client([
                'base_uri' => env('HANDSHAKE_SERVICE'),
                'timeout' => 1.0,
                'auth' => [env('HANDSHAKE_USERNAME'), env('HANDSHAKE_PASSWORD')],
                'verify' => (false)
            ]);

            $request_uri = env('HANDSHAKE_SERVICE') . '/venue/' . $currentVenue . '/layouts';

            $request = $client->request('GET', $request_uri);

            $body = $request->getBody();
            $content = $body->getContents();

            if (!$request->getStatusCode() == 200 && $content !== true) {

                return array('error' => 'unavailable');

            } elseif ($request->getStatusCode() == 200 && $content == true) {

                return json_decode($content, true);

            }

            //return response()->json($layouts, 200);

        } catch (\Exception $e) {

            return response()->json(['error' => 'unavailable'], 200);

        }

    }

}