<?php

namespace App\Http\ViewComposers;

use App\Models\User;
use App\Models\TicketTypes;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use Illuminate\Support\Facades\Log;

class TicketTypesComposer {

    private $_tickettype;
    private $_userRole;

    public function __construct() {

        $currentUser = \Auth::user();
        $this->user = $currentUser;
        
        if ($currentUser) {
            $this->userRole = strtolower($currentUser->roles[0]->name);
        }

    }

    /**
     * 
     * Bind data to the view.
     * 
     * @param View view
     * 
     * @return void
     * 
     */

     public function compose(View $view) {

        $user = $this->user;
        
        if (!$user) {
        
            return false;
        
        }

        $totalTicketTypes = $this->getTotalTicketTypes($user);
        
        $data = [
            'totalTicketTypes'          => $totalTicketTypes,
        ];

        $view->with($data);

     }

     private function getTotalTicketTypes($user) {

        if (Auth::check() && $user->hasRole('admin')) {
            return count($this->getAllTicketTypes());
        }

     }

     private function getAllTicketTypes() {

        $total = TicketTypes::all();
                            
        return $total;

     }

}