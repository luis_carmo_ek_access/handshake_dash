<?php
/**
 * Created by PhpStorm.
 * User: luis
 * Date: 14-07-2018
 * Time: 13:18
 */

return [

    /*
    |--------------------------------------------------------------------------
    | Modals Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which modals share.
    |
     */

    // CONFIRM SAVE DIALOG
    'confirm_modal_title_text'    => 'Guardar',
    'confirm_modal_title_std_msg' => 'Por favor confirme',

    'confirm_modal_title_save_msg'   => 'Por favor confirme as alterações.',
    'confirm_modal_button_save_text' => 'Guardar',
    'confirm_modal_button_save_icon' => 'save',

    'confirm_modal_button_cancel_text' => 'Cancelar',
    'confirm_modal_button_cancel_icon' => 'fa-close',

    // USER EDIT DIALOG
    'edit_user__modal_text_confirm_message' => 'Tem a certeza que deseja guardar as alterações?',
    'edit_user__modal_text_confirm_btn'     => 'Guardas Alterações',

    // DELETE DIALOG
    'confirm_delete_title_text'        => 'Remover Utilizador',
    'confirm_modal_button_delete_text' => 'Remover',

    // DELETE RESTORE
    'confirm_restore_title_text'        => 'Restaurar Utilizador',
    'confirm_modal_button_restore_text' => 'Restaurar',
];
