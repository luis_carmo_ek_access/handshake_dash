<?php
/**
 * Created by PhpStorm.
 * User: luis
 * Date: 14-07-2018
 * Time: 13:23
 */

return [

    /*
    |--------------------------------------------------------------------------
    | Forms Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match forms.
    |
    */

    // CREATE NEW USER FORM
    'create_user_label_email'			=> 'E-mail',
    'create_user_ph_email'				  => 'E-mail',
    'create_user_icon_email'			 => 'fa-envelope',

    'create_user_label_username'		=> 'Username',
    'create_user_ph_username'			  => 'Username',
    'create_user_icon_username'			=> 'fa-user',

    'create_user_label_firstname'		=> 'Primeiro Nome',
    'create_user_ph_firstname'			  => 'Primeiro Nome',
    'create_user_icon_firstname'		 => 'fa-user',

    'create_user_label_lastname'		=> 'Último Nome',
    'create_user_ph_lastname'			  => 'Último Nome',
    'create_user_icon_lastname'			=> 'fa-user',

    'create_user_label_password'		=> 'Password',
    'create_user_ph_password'			  => 'Password',
    'create_user_icon_password'			=> 'fa-lock',

    'create_user_label_pw_confirmation'	=> 'Confirmar Password',
    'create_user_ph_pw_confirmation'	   => 'Confirmar Password',
    'create_user_icon_pw_confirmation'	 => 'fa-lock',

    'create_user_label_location'		=> 'Localização',
    'create_user_ph_location'			  => 'Localização',
    'create_user_icon_location'			=> 'fa-map-marker',

    'create_user_label_bio'				=> 'Biografia',
    'create_user_ph_bio'				   => 'Biografia',
    'create_user_icon_bio'				 => 'fa-pencil',

    'create_user_label_twitter_username'=> 'Twitter Username',
    'create_user_ph_twitter_username'	  => 'Twitter Username',
    'create_user_icon_twitter_username'	=> 'fa-twitter',

    'create_user_label_github_username'	=> 'GitHub Username',
    'create_user_ph_github_username'	   => 'GitHub Username',
    'create_user_icon_github_username'	 => 'fa-github',

    'create_user_label_career_title'	=> 'Profissão',
    'create_user_ph_career_title'		  => 'Profissão',
    'create_user_icon_career_title'		=> 'fa-briefcase',

    'create_user_label_education'		=> 'Educação',
    'create_user_ph_education'			  => 'Educação',
    'create_user_icon_education'		 => 'fa-graduation-cap',

    'create_user_label_role'			=> 'Grupo',
    'create_user_ph_role'				  => 'Selecione o Grupo',
    'create_user_icon_role'				=> 'fa-shield',

    'create_user_button_text'			=> 'Criar novo Utilizador',

    // EDIT USER AS ADMINISTRATOR FORM
    'edit-user-admin-title'				=> 'Editar Informações Utilizador',

    'label-username'					=> 'Username',
    'ph-username'						  => 'Username',

    'label-useremail'					=> 'E-mail',
    'ph-useremail'						  => 'User Email',

    'label-userrole_id'					=> 'Nível de Acesso',
    'option-label'						    => 'Selecionar Nível',
    'option-user'						     => 'Utilizador',
    'option-editor'						   => 'Editor',
    'option-admin'						    => 'Administrador',
    'submit-btn-text'					  => 'Editar Utilizador!',

    'submit-btn-icon'					=> 'fa-save',
    'username-icon'						 => 'fa-user',
    'useremail-icon'					 => 'fa-envelope-o',

    'label-event_id'                    => 'Evento',
    'label-event_park_id'               => 'Evento Parque',
    'label-event_credential_id'         => 'Evento Credenciais',

];
