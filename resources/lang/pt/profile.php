<?php
/**
 * Created by PhpStorm.
 * User: luis
 * Date: 14-07-2018
 * Time: 13:31
 */

return [

    /*
    |--------------------------------------------------------------------------
    | User Profiles Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match user profile
    | pages, messages, forms, labels, placeholders, links, and buttons.
    |
     */

    'templateTitle' => 'Editar Perfil',

    // profile erros
    'notYourProfile'      => 'Este não é o perfil que está à procura.',
    'notYourProfileTitle' => 'Hmmm, algo está errado ...',
    'noProfileYet'        => 'Ainda Sem Perfil.',

    // USER profile title
    'showProfileUsername'        => 'Username',
    'showProfileFirstName'       => 'Primeiro Nome',
    'showProfileLastName'        => 'Último Nome',
    'showProfileEmail'           => 'E-mail',
    'showProfileLocation'        => 'Localização',
    'showProfileBio'             => 'Biografia',
    'showProfileTheme'           => 'Tema',
    'showProfileTwitterUsername' => 'Twitter Username',
    'showProfileGitHubUsername'  => 'Github Username',

    // USER profile page
    'showProfileTitle' => ':username\'s Perfil',

    // USER EDIT profile page
    'editProfileTitle' => 'Configurações Perfil',

    // User edit profile form
    'label-theme' => 'Tema:',
    'ph-theme'    => 'Selecione um Tema',

    'label-location' => 'Localização:',
    'ph-location'    => 'A sua localização',

    'label-bio' => 'Biografia:',
    'ph-bio'    => 'A sua biografia',

    'label-github_username' => 'GitHub username:',
    'ph-github_username'    => 'GitHub username',

    'label-twitter_username' => 'Twitter username:',
    'ph-twitter_username'    => 'Twitter username',

    // User Account Settings Tab
    'editTriggerAlt'        => 'Alterar Menú',
    'editAccountTitle'      => 'Configurações Conta',
    'editAccountAdminTitle' => 'Administração Conta',
    'updateAccountSuccess'  => 'Conta atualizada com sucesso',
    'submitProfileButton'   => 'Guardar Alterações',

    // User Account Admin Tab
    'submitPWButton'    => 'Atualizar Password',
    'changePwTitle'     => 'Alterar Password',
    'changePwPill'      => 'Alterar Password',
    'deleteAccountPill' => 'Remover Conta',
    'updatePWSuccess'   => 'Password atualizada com sucesso',

    // Delete Account Tab
    'deleteAccountTitle'        => 'Remover Conta',
    'deleteAccountBtn'          => 'Remover Conta',
    'deleteAccountBtnConfirm'   => 'Remover Conta',
    'deleteAccountConfirmTitle' => 'Confirmar Remoção da Conta',
    'deleteAccountConfirmMsg'   => 'Tem a certeza que pretende remover a conta?',
    'confirmDeleteRequired'     => 'Confirmação Remoção da Conta Requerida',

    'errorDeleteNotYour'        => 'Não tem permissões para remover outras contas',
    'successUserAccountDeleted' => 'Conta Removida com Sucesso',

    // Messages
    'updateSuccess' => 'Perfil atualizado com sucesso',
    'submitButton'  => 'Guardar Alterações',

    // Restore User Account
    'errorRestoreUserTime' => 'Desculpe, não é possível restaurar a conta',
    'successUserRestore'   => 'Seja bem-vindo :username! Conta Restaurada com Sucesso',

    // Save button
    'submitButton'        => 'Guardar',
    'submitChangesButton' => 'Guardar Alterações',

    // User Account
    'accountTitle' => 'Configurações de Conta para :username\'s',

];
