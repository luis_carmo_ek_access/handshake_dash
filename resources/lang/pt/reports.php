<?php
/**
 * User: luis carmo
 */

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'templateTitle'                     => 'Consultar Relatório',
    'editTicketTypeTitle'               => 'Configurações Relatório',
    'reports_text'                      => 'Relatórios',
    'report_text'                       => 'Relatório: ',
    'showing_reports'                   => 'Lista Relatórios',
    'generate_report'                   => 'Gerar Relatório',
    'view_report'                       => 'Ver Relatório',
    'single_total_row'                  => 'Tipo Relatório',
    'total_rows'                        => 'Tipos Relatório',
    'empty_rows'                        => 'Não Existem Relatórios Disponiveis',
    'description'                       => 'Descrição',
    'value'                             => 'Valor',
    'available_filters'                 => 'Disponível p/ Filtragem',
    'gate_filter'                       => 'Porta',
    'active'                            => 'Ativo',
    'created_at'                        => 'Adicionado',
    'updated_at'                        => 'Atualizado',
    'actions'                           => 'Ações',
    'view'                              => 'Ver Tipo Bilhete',
    'edit'                              => 'Editar Tipo Bilhete',
    'delete'                            => 'Eliminar Tipo Bilhete',
    'add_new_tickettype'                => 'Adicionar Tipo Bilhete',
    'show_deleted_tickettypes'          => 'Tipos Bilhete Eliminados',
    'details'                           => 'Detalhes',
    'return'                            => 'Voltar Tipos Bilhete',
    'cantAccessPage'                    => 'Impossivel entrar na página',
    'no_download'                       => ' Ficheiro Não Seleccionado',
    'request_report'                    => ' Processar Pedido',
    'request_report_error'              => ' Falha Processar Pedido',
    'process_file'                      => ' Processar Ficheiro',
    'process_file_error'                => ' Falha Processar Ficheiro',
    'success_download'                  => ' Ficheiro Processado com Sucesso',

    'error_description'                 => 'Letras e Números apenas',
    'choose_event'                      => 'Escolher Evento',
    'generated_successfully'            => 'Relatório Gerado com Sucesso!',
    'back_to_reports'                   => 'Voltar',
    'event_datetime'                    => 'Data / Hora Evento',
    'event_report_name'                 => 'Nome Relatório',
    'event_competition'                 => 'Competição',
    'event_installation'                => 'Instalação',
    'activity_report'                   => 'Atividades',
    'occurrencies_report'               => 'Ocorrências',
    'error_event'                       => 'Seleccione um Evento Válido!',
    'error_loading_report_data'         => 'Indisponivel! Por favor tente mais tarde.',
    'please_choose_event'               => 'Por Favor Escolha Pelo Menos Um Relatório',
    'stop_report'                       => 'Cancelar',

    /**
     * Report Variables
     */

    'report_extra_tcp_header'           => 'Entradas Extra (TCP)',
    'report_box_resume_header'          => 'Resumo Camarotes',
    'report_header'                     => 'Relatório de Operações',
    'text_competition'                  => 'Competição: ',
    'text_event'                        => 'Evento: ',
    'text_competition'                  => 'Evento: ',
    'text_event'                        => 'Evento: ',
    'text_installation'                 => 'Instalação: ',
    'text_date'                         => 'Data: ',
    'text_activity_registry'            => 'Registo de Atividades',
    'text_occurencies_registry'         => 'Registo de Ocorrências',
    'loading_charts'                    => 'A carregar ...',
    'text_zone'                         => 'Zona',
    'text_imported_tickets'             => 'Títulos Importados',
    'text_total_entries'                => 'Total Entradas',
    'text_total_masters'                => 'Total Masters',
    'text_master'                       => 'Masters',
    'text_entries'                      => 'Entradas',
    'text_entity'                       => 'Entidade',
    'text_cam_name'                     => 'Camarote',
    'text_entity_gate'                  => 'Entidade / Porta',
    'text_ticket_types'                 => 'Tipo de Titulo',
    'text_quantity'                     => 'Quantidade',
    'text_percentage'                   => '%',
    'text_total'                        => 'Total',
    'text_total_with_extras'            => 'Total + Extras',
    'text_maximum_flowrate'             => 'Flowrate Máximo',
    'text_total_entries_group'          => 'Total de Entradas',
    'text_hour'                         => 'Hora',
    'text_global'                       => 'Global *',
    'text_app'                          => 'APP **',
    'text_box'                          => 'Camarotes',
    'text_box_extra'                    => 'Livre Trânsito_Eletrónicos',
    'text_credentials'                  => 'Credenciais',
    'text_park'                         => 'Parque',
    'text_app_info'                     => '** Este valor refere-se apenas às entradas com o tipo de acesso cartão de sócio via APP',
    'text_global_info'                  => '* Inlcui APP, Camarotes e Masters',
    'text_imported_titles'              => 'Totais de Títulos Importados',
    'text_whitelist'                    => 'Whitelist',
    'text_blacklist'                    => 'Blacklist',
    'text_stadium'                      => 'Estádio',
    'text_entries_by_zone'              => 'Entradas por Zona',
    'text_entries_by_master'            => 'Entradas por Master',
    'text_entries_by_tickettype'        => 'Entradas por Tipo Titulo',
    'text_top_credentials_entries_list' => 'Entradas de Credenciais: Top 10 Entidades',
    'text_gates_error'                  => 'Erros por Porta',
    'text_errors_gate'                  => 'Estatística Erro / Porta',
    'text_cam_entries'                  => 'Entradas por Camarote',
    'text_transactions_error_by_message'=> 'Transações de Erro por Mensagem',
    'text_unique_title_error_by_message'=> 'Titulos Unicos por Mensagem de Erro',
    'text_message'                      => 'Mensagem',
    'text_report'                       => 'Relatório',
    'text_cam_nascente'                 => 'Camarotes Nascente',
    'text_cam_poente'                   => 'Camarotes Poente',
    'text_create_report'                => 'Criar Relatório',
    'text_extra_child'                  => 'Extra Crianças',
    'text_extra_adult'                  => 'Extra Adultos',

    /**
     *
     * Report Types
     *
     */

    'text_whitelist'                    => 'Whitelist',
    'text_whitelist_masters'            => 'Whitelist Masters',
    'text_blacklist'                    => 'Blacklist',
    'text_unique_ticket_errors'         => 'Erros Títulos Únicos',
    'text_transactions_errors'          => 'Erros Transações',
    'text_entries_app'                  => 'Entradas APP',
    'text_entries_cam_presidencial'     => 'Entradas Camarote Presidencial',
    'text_entries_cam'                  => 'Entradas Camarotes',
    'text_entries_gate_category'        => 'Entradas por Porta e Categoria',
    'text_fill_level_minute'            => 'Fill Level Minuto',
    'text_occurrences_tcp'              => 'Occorências TCP',
    'text_ticket_entries_minute'        => 'Titulos Já Entrou 1 Minuto',
    'text_transactions_ticket_entry_min'=> 'Transações Titulos Já Entrou 1 Minuto',

];