<?php
/**
 * Created by PhpStorm.
 * User: luis
 * Date: 14-07-2018
 * Time: 13:42
 */

return [

    /*
    |--------------------------------------------------------------------------
    | Themes Management Language Lines
    |--------------------------------------------------------------------------
    |
    */

    // Messages
    'createSuccess'     => 'Tema Adicionado! ',
    'updateSuccess'     => 'Tema Modificado! ',
    'deleteSuccess'     => 'Tema Removido! ',
    'deleteSelfError'   => 'Não pode remover o tema (padrão). ',

    // Shared
    'statusLabel'       => 'Estado Tema',
    'statusEnabled'     => 'Ativo',
    'statusDisabled'    => 'Desativo',

    'nameLabel'    	 	  => 'Nome Tema *',
    'namePlaceholder'   => 'Insira o nome do tema',

    'linkLabel'    	 	  => 'Link CSS Tema *',
    'linkPlaceholder'   => 'Insira o link css do tema',

    'notesLabel'    	   => 'Notas Tema',
    'notesPlaceholder'  => 'Insira notas do tema',

    'themes'			=> 'Temas',

    // Add Theme
    'btnAddTheme'    	=> 'Adicionar Tema',

    // Edit Theme
    'editTitle'    	 	=> 'A Editar Tema:',
    'editSave'    	 	 => 'Guardar Alterações Tema',

    // Show Theme
    'showHeadTitle'		   => 'Tema',
    'showTitle'    	 	  => 'Informações Tema',
    'showBackBtn'  	 	  => 'Voltar a Temas',
    'showUsers' 	       => 'Utilizadores',
    'showStatus'   	 	  => 'Estado',
    'showLink'    	 	   => 'CSS Link',
    'showNotes'    	 	  => 'Notas',
    'showAdded'    	 	  => 'Adicionado',
    'showUpdated'    	  => 'Atualizado',
    'confirmDeleteHdr'  => 'Remover Tema',
    'confirmDelete'    	=> 'Tem a certeza que pretende remover o tema?',

    // Show Themes
    'themesTitle'		   => 'Mostrar Todos',
    'themesStatus'		  => 'Estado',
    'themesUsers'		   => 'Utilizadores',
    'themesName'		    => 'Nome',
    'themesLink'		    => 'CSS Link',
    'themesActions'		 => 'Ações',
    'themesBtnShow'		 => 'Ver Tema',
    'themesBtnEdit'		 => 'Editar Tema',
    'themesBtnDelete'	=> 'Remover Tema',
    'themesBtnEdits'	 => '',

];
