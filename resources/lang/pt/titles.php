<?php
/**
 * Created by PhpStorm.
 * User: luis
 * Date: 14-07-2018
 * Time: 13:11
 */

return [

    'app'			        => 'Dashboard Monitor',
    'app2'			        => 'Dashboard Monitor',
    'home'			        => 'Home',
    'login'			        => 'Login',
    'logout'		        => 'Logout',
    'register'		        => 'Registar',
    'resetPword'	        => 'Alterar Password',
    'toggleNav'		        => 'Alternar Navegação',
    'profile'		        => 'Perfil',
    'editProfile'	        => 'Editar Perfil',
    'createProfile'	        => 'Criar Perfil',
    'dashboard'		        => 'Dashboard',
    'account' 		        => 'Conta',

    'activation'	        => 'Registo Iniciado  | Ativação Requerida',
    'exceeded'		        => 'Erro Ativação',

    'editProfile'	        => 'Editar Perfil',
    'createProfile'	        => 'Criar Perfil',
    'adminUserList'	        => 'Utilizadores',
    'adminEditUsers'        => 'Editar Utilizadores',
    'adminNewUser'	        => 'Adicionar Utilizador',

    'adminThemesList'       => 'Temas',
    'adminThemesAdd'        => 'Adicionar Tema',

    'adminLogs'		        => 'Logs',
    'adminPHP'		        => 'Informações PHP',
    'adminRoutes'	        => 'Routing Info',

    'editTicketType'        => 'Editar Tipo Bilhete',
    'ticketTypes'           => 'Tipos de Bilhete',
    'view-ticket-type'      => 'Consultar Tipo de Bilhete',
    'reports'               => 'Relatórios'

];
