<?php
/**
 * Created by PhpStorm.
 * User: luis
 * Date: 14-07-2018
 * Time: 13:52
 */

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'O(A) :attribute deve ser aceite.',
    'active_url'           => 'O(A) :attribute não é uma URL válida.',
    'after'                => 'O(A) :attribute deve ser uma data após :date.',
    'after_or_equal'       => 'O(A) :attribute deve ser uma data após ou igual a :date.',
    'alpha'                => 'O(A) :attribute deve conter apenas letras.',
    'alpha_dash'           => 'O(A) :attribute deve conter apenas letras, números, e _',
    'alpha_num'            => 'O(A) :attribute deve conter apenas letras e números.',
    'array'                => 'O(A) :attribute deve ser um array.',
    'before'               => 'O(A) :attribute deve ser uma data anterior a :date.',
    'before_or_equal'      => 'O(A) :attribute deve ser uma data anterior ou igual a :date.',
    'between'              => [
        'numeric' => 'O(A) :attribute deverá ter entre :min e :max.',
        'file'    => 'O(A) :attribute deverá ter entre :min e :max kilobytes.',
        'string'  => 'O(A) :attribute deverá ter entre :min e :max caracteres.',
        'array'   => 'O(A) :attribute deverá ter entre :min e :max items.',
    ],
    'boolean'              => 'O(A) :attribute deverá ser verdadeiro ou falso.',
    'confirmed'            => 'O(A) :attribute não é igual à confirmação.',
    'date'                 => 'O(A) :attribute não é uma data válida.',
    'date_format'          => 'O(A) :attribute não tem um formato válido ( :format ) .',
    'different'            => 'O(A) :attribute e :other devem ser diferentes.',
    'digits'               => 'O(A) :attribute deve conter :digits digitos.',
    'digits_between'       => 'O(A) :attribute deve conter entre :min e :max digitos.',
    'dimensions'           => 'O(A) :attribute tem dimensões inválidas.',
    'distinct'             => 'O(A) :attribute tem um valor duplicado.',
    'email'                => 'O(A) :attribute deve ser um e-mail válido.',
    'exists'               => 'O(A) :attribute selecionado é inválido.',
    'file'                 => 'O(A) :attribute deve ser um ficheiro.',
    'filled'               => 'O(A) :attribute deverá conter um valor.',
    'image'                => 'O(A) :attribute dever´a ser uma imagem.',
    'in'                   => 'O(A) :attribute selecionado é inválido.',
    'in_array'             => 'O(A) :attribute não existem em :other.',
    'integer'              => 'O(A) :attribute deverá ser um inteiro.',
    'ip'                   => 'O(A) :attribute deverá ser um IP válido.',
    'json'                 => 'O(A) :attribute deverá ser um JSON válido.',
    'max'                  => [
        'numeric' => 'O(A) :attribute não deverá ser maior que :max.',
        'file'    => 'O(A) :attribute não deverá ser maior que :max kilobytes.',
        'string'  => 'O(A) :attribute não deverá ser maior que :max caracteres.',
        'array'   => 'O(A) :attribute não deverá ser maior que :max items.',
    ],
    'mimes'                => 'O(A) :attribute deverá ser um ficheiro do tipo: :values.',
    'mimetypes'            => 'O(A) :attribute deverá ser um ficheiro do tipo: :values.',
    'min'                  => [
        'numeric' => 'O(A) :attribute deverá ter no mínimo :min.',
        'file'    => 'O(A) :attribute deverá ter no mínimo :min kilobytes.',
        'string'  => 'O(A) :attribute deverá ter no mínimo :min caracteres.',
        'array'   => 'O(A) :attribute deverá ter no mínimo :min items.',
    ],
    'not_in'               => 'O(A) :attribute selecionado é inválido.',
    'numeric'              => 'O(A) :attribute deverá ser um número.',
    'present'              => 'O(A) :attribute deverá estar presente.',
    'regex'                => 'O(A) :attribute contém um formato inválido.',
    'required'             => 'O(A) :attribute é requerido.',
    'required_if'          => 'O(A) :attribute é requerido quando :other é :value.',
    'required_unless'      => 'O(A) :attribute é requerido a não ser que :other esteja em :values.',
    'required_with'        => 'O(A) :attribute é requerido quando :values está presente.',
    'required_with_all'    => 'O(A) :attribute é requerido quando :values está presente.',
    'required_without'     => 'O(A) :attribute é requerido quando :values não está presente.',
    'required_without_all' => 'O(A) :attribute é requerido quando nenhum dos :values está presente.',
    'same'                 => 'O(A) :attribute e :other deverão ser iguais.',
    'size'                 => [
        'numeric' => 'O(A) :attribute deverá ter :size.',
        'file'    => 'O(A) :attribute deverá ter :size kilobytes.',
        'string'  => 'O(A) :attribute deverá ter :size caracteres.',
        'array'   => 'O(A) :attribute deverá ter :size items.',
    ],
    'string'               => 'O(A) :attribute deverá ser do tipo texto.',
    'timezone'             => 'O(A) :attribute deverá ser uma zona válida.',
    'unique'               => 'O(A) :attribute já se encontra em uso.',
    'uploaded'             => 'O(A) :attribute falhou a carregar.',
    'url'                  => 'O(A) :attribute contém um formato inválido.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
