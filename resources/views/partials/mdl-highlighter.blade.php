<div class="mdl-textfield mdl-js-textfield mdl-textfield--expandable search-white">
	<label class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect mdl-button--icon" for="highlight_info" style="top: 16px" title="{{ trans('general.highlight') }}">
	  	<i class="material-icons">highlight</i>
	  	<span class="sr-only">{{ trans('general.highlight') }}</span>
	</label>
	<div class="mdl-textfield__expandable-holder">
	  	<input class="mdl-textfield__input" type="search" id="highlight_info" name="highlight_info" placeholder="{{ trans('general.highlight') }}">
	  	<label class="mdl-textfield__label" for="highlight_info">
			{{ trans('general.highlight') }}
	  	</label>
	</div>
</div>