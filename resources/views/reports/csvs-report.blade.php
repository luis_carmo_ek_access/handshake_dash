<style scooped>

    .spinner {
        margin-top: -35px;
        text-align: center;
    }

    .flatpickr-calendar.hasTime.animate.showTimeInput.arrowTop.open {
        top: 55px !important;
        left: 0px !important;
    }

    .flatpickr-calendar.hasTime.animate.arrowTop.open {
        top: 55px !important;
        left: 0px !important;
    }

    .flatpickr-calendar.hasTime.animate.open.arrowBottom {
        top: 55px !important;
        left: 0px !important;
    }

    .flatpickr-calendar.animate.open.arrowTop {
        display: none;
    }

    .flatpickr-calendar .flatpickr-calendar.open {
        display: none;
    }

    div#event-datetime .flatpickr-calendar.open {
        display: inline-block;
    }

    .flatpickr-calendar.animate.open.arrowBottom {
        display: none;
    }

    #report-body #header-logo #header-image {

        width: 100%;

    }

    #report-dialog-content {

        width: 250mm;

    }

    #report-header {

        width: 100%;
        padding-top: 40px;
        padding-bottom: 40px;

    }

    .report-header-title {

        width: 100%;
        text-align: center;
        /*color: #30BDCC;*/

    }

    .mdl-color--primary {
        /*background: #30BDCC !important;*/
    }

    #report-event-details{

        width: 100%;
        padding: 0 40px 0 40px;

    }

    #report-activity-registry {

        width: 100%;
        padding: 0px 40px 0 40px;

    }
    #report-data {

        width: 100%;
        padding: 80px 40px 0 0px;

    }

    #imports-entries-by-ticket-type {
        display: none;
        width: 100%;
    }

    #entries-by-masters {

        display: none;
        width: 100%;

    }

    #ticket-types-entries-list {

        display: none;
        width: 100%;

    }

    #maximum-flowrate {

        display: none;
        width: 100%;

    }

    #total-entries-group {

        display: none;
        width: 100%;

    }

    #total-imports-group {

        display: none;
        width: 100%;

    }

    #report-occurencies-registry {

        width: 100%;
        padding: 0px 40px 0px 40px;

    }

    .align-left {
        float: left;
    }

    .align-right {
        float: right;
    }

    #report-dialog {

        width: 250mm; /*210.00197555866663mm; /*80%;*/
        /*max-height: 297.0006773335mm; /*600px;*/

    }

    .mdl-dialog__content {
        padding: 0 !important;
    }

    div#report-body {
        overflow-x: auto;
        overflow-y: auto;
        max-height: 700px;
    }

    #report-load-charts > .mdl-snackbar__text {

        width: 100%;
        text-align: center;

    }

    #report-load-charts > .mdl-snackbar__action {

        display: none;

    }

    .logo-style {
        width: 90% !important;
    }

    .mdl-spinner__layer-1 {
        border-color: #ffffff !important;
    }
    .mdl-spinner__layer-2 {
        border-color: #ffffff !important;
    }

    .mdl-spinner__layer-3 {
        border-color: #ffffff !important;
    }

    .mdl-spinner__layer-4 {
        border-color: #ffffff !important;
    }

    #report-load-charts {
        border-radius: 100px;
    }

    .datetimepicker-event-date {
        display: none;
    }

    input.flatpickr-event-date-report.mdl-textfield.mdl-js-textfield.mdl-textfield--floating-label.is-upgraded.is-focused.flatpickr-input.flatpickr-mobile {
        display: none;
    }

    .mdl-data-table tbody tr {
        height: 30px !important;
        font-size: 10px;
    }

    .mdl-data-table td {
        height: 30px !important;
    }

    @media only screen and (max-width: 720px) {

        #report-dialog {
            background: #ffffff !important;
            -webkit-print-color-adjust: exact;
        }

        .mdl-color--primary {
            background-color: #30BDCC; /* rgb(63,81,181)!important;*/
        }

        .custom-header-color {
            background: #30BDCC;
        }

        #report-event-details{

            width: 100%;
            padding: 0px 60px 0px 0px;

        }

        #report-activity-registry {

            width: 100%;
            padding: 00px 40px 0px 40px;

        }

        #report-data {

            width: 100%;
            padding: 40px 60px 0px 0px;

        }

        #report-occurencies-registry {

            width: 100%;
            padding: 0px 40px 0px 40px;

        }

        dialog#report-dialog {

            width: 100%;

        }

        #report-dialog {

            width: 100%;

        }

        #report-dialog-content {

            width: 100%;

        }

        #report-header {

            width: 100%;
            padding-top: 40px;
            padding-bottom: 40px;

        }

        .report-header-title {

            width: 100%;
            text-align: center;
            color: #30BDCC;

        }

        #report-event-details{

            width: 100%;
            padding: 0 40px 0 40px;

        }

        #report-activity-registry {

            width: 100%;
            padding: 0px 40px 0 40px;
            page-break-after: always !important;

        }

        #report-occurencies-registry {

            width: 100%;
            padding: 0px 40px 0px 40px;

        }

        .logo-style {
            width: 65% !important;
        }

    }

    #imports-entries-by-ticket-type.mdl-data-table th {
        /*color: #ffffff;*/
        text-align: center;
    }

    #ticket-types-entries-list.mdl-data-table th {
        /*color: #ffffff;*/
        text-align: center;
    }

    #maximum-flowrate.mdl-data-table th {
        /*color: #ffffff;*/
        text-align: center;
    }

    #total-entries-group.mdl-data-table th {
        /*color: #ffffff;*/
        text-align: center;
    }

    #total-imports-group.mdl-data-table th {
        /*color: #ffffff;*/
        text-align: center;
    }

    #entries-by-masters.mdl-data-table th {
        /*color: #ffffff;*/
        text-align: center;
    }

    #imports-entries-by-ticket-type.mdl-data-table td {
        text-align: center;
    }

    #ticket-types-entries-list.mdl-data-table td {
        text-align: center;
    }

    #maximum-flowrate.mdl-data-table td {
        text-align: center;
    }

    #total-entries-group.mdl-data-table td {
        text-align: center;
    }

    #total-imports-group.mdl-data-table td {
        text-align: center;
    }

    #entries-by-masters.mdl-data-table td {
        text-align: center;
    }

    td.description {
        text-align: left !important;
    }

    @media print {

        .mdl-data-table td {

            padding: 0px 0px;

        }

        .report-header-title:first-letter {
            color: #30BDCC !important;
        }

        #report-dialog {
            background: #ffffff !important;
            -webkit-print-color-adjust: exact;
        }

        #report-options {
            display: none !important;
        }

        .mdl-color--primary {
            background-color: #30BDCC !important; /* rgb(63,81,181)!important;*/
        }

        .custom-header-color {
            background: #30BDCC;
        }

        #imports-entries-by-ticket-type.mdl-data-table th {
            /*color: #ffffff !important;*/
            text-align: center !important;
        }

        .report-header-title {
            width: 100% !important;
            text-align: center !important;
            color: #30BDCC !important;
            page-break-after: always !important;
        }

        #imports-entries-by-ticket-type {
            display: table !important;
            width: 100% !important;
        }

        #ticket-types-entries-list {
            display: table !important;
            width: 100% !important;
        }

        #maximum-flowrate {
            display: table !important;
            width: 100% !important;
        }

        #total-entries-group {
            display: table !important;
            width: 100% !important;
        }

        #total-imports-group {
            display: table !important;
            width: 100% !important;
        }

        #entries-by-masters {
            display: table !important;
            width: 100% !important;
        }

        #loading-report-animation {
            background: #30BDCC !important; /*rgb(63,81,181)!important;*/
        }

        td.description {
            text-align: left !important;
        }

    }

    #entries-by-ticket-chart {

        width: 50%;
        margin: auto;

    }

    .break-page {
        page-break-before: always;
    }

    #top-entities-credentials {

        width: 50%;
        margin-left: 25%;
        margin-right: 25%;

    }

    #top-entities-credentials.mdl-data-table th:first-of-type {
        text-align: left;
    }

    td.entity-name {
        text-align: left !important;
    }

    td.gate-name {
        text-align: left !important;
    }

    tr.entity-credential {
        background: #30BDCC;

    }

    #csv-reports-type-table {
        width: 100%;
        overflow-x: auto;
        overflow-y: auto;
    }

    .mdl-data-table th {

        text-align: left !important;
        
    }

    .mdl-data-table td, .mdl-data-table td .mdl-data-table__select {

        text-align: left !important;

    }

    #loading-report-animation {

        display: none;

    }

    #download-reports-dialog {

        width: 80% !important;

    }

    #csv-download-table {
        width: 100% !important;
    }

    #download-report-body {

        max-height: 600px;
        overflow: auto;

    }

    .mdl-progress {

        width: 100% !important;

    }

</style>

@extends('layouts.dashboard')

@section('template_title')
    {{ trans('reports.showing_reports') }}
@endsection

@section('header')
    {{ trans('reports.showing_reports') }}
@endsection

@section('breadcrumbs')

    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <a itemprop="item" href="{{url('/')}}">
            <span itemprop="name">
                {{ trans('titles.app') }}
            </span>
        </a>
        <i class="material-icons">chevron_right</i>
        <meta itemprop="position" content="1" />
    </li>
    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <a itemprop="item" href="/reports">
            <span itemprop="name">
                {{ trans('reports.reports_text')  }}
            </span>
        </a>
        <i class="material-icons">chevron_right</i>
        <meta itemprop="position" content="2" />
    </li>
    <li class="active" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <a itemprop="item" href="/reports/{{ $report->id }} . /generate">
        <span itemprop="name">
            {{ $report->description }}
        </span>
        </a>
        <meta itemprop="position" content="3" />
    </li>

@endsection

@section('content')

    <div id="report-options" class="mdl-cell mdl-cell--12-col">

        <div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--12-col-desktop mdl-card mdl-shadow--3dp margin-top-0 padding-top-0">

            <div class="mdl-card card-new-report" style="width:100%;" itemscope itemtype="http://schema.org/Person">

                <div class="mdl-card__title mdl-card--expand mdl-color--primary mdl-color-text--white">
                    <h2 class="mdl-card__title-text logo-style">{{ trans('reports.generate_report') }}</h2>
                </div>

                <div class="mdl-card__supporting-text">

                    <div class="mdl-grid full-grid padding-0">

                        <div class="mdl-cell mdl-cell--12-col-phone mdl-cell--12-col-tablet mdl-cell--12-col-desktop">

                            <div id="report-details" class="mdl-grid ">

                                <div class="mdl-cell mdl-cell--12-col-tablet mdl-cell--12-col-desktop">
                                    <div class="event-options mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-select mdl-select__fullwidth is-dirty">
                                        <select id="report-event" class="mdl-selectfield__select mdl-textfield__input" name="report-event">
                                        </select>
                                        <label for="report-event">
                                            <i class="mdl-icon-toggle__label material-icons">arrow_drop_down</i>
                                        </label>
                                        {!! Form::label('event', trans('forms.label-event_id'), array('class' => 'mdl-textfield__label mdl-selectfield__label')); !!}
                                        <span id="error-select-event" class="mdl-textfield__error">{{ trans('reports.error_event')  }}</span>
                                    </div>
                                </div>

                                <div class="mdl-cell mdl-cell--12-col-tablet mdl-cell--12-col-desktop">
                                    <div class="event-options mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-select mdl-select__fullwidth is-dirty">
                                        <select id="report-event-park" class="mdl-selectfield__select mdl-textfield__input" name="report-event-park">
                                        </select>
                                        <label for="report-event-park">
                                            <i class="mdl-icon-toggle__label material-icons">arrow_drop_down</i>
                                        </label>
                                        {!! Form::label('event-park', trans('forms.label-event_park_id'), array('class' => 'mdl-textfield__label mdl-selectfield__label')); !!}
                                        <span id="error-select-event-park" class="mdl-textfield__error">{{ trans('reports.error_event')  }}</span>
                                    </div>
                                </div>

                                <div class="mdl-cell mdl-cell--12-col-tablet mdl-cell--12-col-desktop">
                                    <div class="event-options mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-select mdl-select__fullwidth is-dirty">
                                        <select id="report-event-credential" class="mdl-selectfield__select mdl-textfield__input" name="report-event-credential">
                                        </select>
                                        <label for="report-event-credential">
                                            <i class="mdl-icon-toggle__label material-icons">arrow_drop_down</i>
                                        </label>
                                        {!! Form::label('event-credential', trans('forms.label-event_credential_id'), array('class' => 'mdl-textfield__label mdl-selectfield__label')); !!}
                                        <span id="error-select-event-credential" class="mdl-textfield__error">{{ trans('reports.error_event')  }}</span>
                                    </div>
                                </div>

                                <div class="csv-reports-type-table-scrollable" style="width: 100%; overflow-x: auto; overflow-y: auto">
                                    <table id="csv-reports-type-table" class="mdl-data-table mdl-js-data-table mdl-data-table--selectable mdl-shadow--2dp">

                                        <thead class="mdl-color--primary custom-header-color">
                                        <tr>
                                            <th style="color: #ffffff !important;">{{trans('reports.text_report')}}</th>
                                            <th style="color: #ffffff !important;">{{trans('forms.label-event_id')}}</th>
                                            <th style="color: #ffffff !important;">{{trans('forms.label-event_park_id')}}</th>
                                            <th style="color: #ffffff !important;">{{trans('forms.label-event_credential_id')}}</th>
                                        </tr>
                                        </thead>

                                        <tbody>

                                        {{-- Report Whitelist --}}

                                        <tr id="report-whitelist">
                                            <td class="report-name">{{trans('reports.text_whitelist')}}</td>
                                            <td class="report-event">
                                                <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-event-whitelist">
                                                    <input type="checkbox" id="switch-event-whitelist" class="mdl-switch__input">
                                                    <span class="mdl-switch__label"></span>
                                                </label>
                                            </td>
                                            <td class="report-event-park">
                                                <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-event-park-whitelist">
                                                    <input type="checkbox" id="switch-event-park-whitelist" class="mdl-switch__input">
                                                    <span class="mdl-switch__label"></span>
                                                </label>
                                            </td>
                                            <td class="report-event-credential">
                                                <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-event-credential-whitelist">
                                                    <input type="checkbox" id="switch-event-credential-whitelist" class="mdl-switch__input">
                                                    <span class="mdl-switch__label"></span>
                                                </label>
                                            </td>
                                        </tr>

                                        {{-- Report Whitelist Masters --}}

                                        <tr id="report-whitelist-masters">
                                            <td class="report-name">{{trans('reports.text_whitelist_masters')}}</td>
                                            <td class="report-event">
                                                <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-event-whitelist-master">
                                                    <input type="checkbox" id="switch-event-whitelist-master" class="mdl-switch__input">
                                                    <span class="mdl-switch__label"></span>
                                                </label>
                                            </td>
                                            <td class="report-event-park">
                                                <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-event-park-whitelist-master">
                                                    <input type="checkbox" id="switch-event-park-whitelist-master" class="mdl-switch__input">
                                                    <span class="mdl-switch__label"></span>
                                                </label>
                                            </td>
                                            <td class="report-event-credential">
                                                <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-event-credential-whitelist-master">
                                                    <input type="checkbox" id="switch-event-credential-whitelist-master" class="mdl-switch__input">
                                                    <span class="mdl-switch__label"></span>
                                                </label>
                                            </td>
                                        </tr>

                                        {{-- Report Blacklist --}}

                                        <tr id="report-blacklist">
                                            <td class="report-name">{{trans('reports.text_blacklist')}}</td>
                                            <td class="report-event">
                                                <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-event-blacklist">
                                                    <input type="checkbox" id="switch-event-blacklist" class="mdl-switch__input">
                                                    <span class="mdl-switch__label"></span>
                                                </label>
                                            </td>
                                            <td class="report-event-park">
                                                <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-event-park-blacklist">
                                                    <input type="checkbox" id="switch-event-park-blacklist" class="mdl-switch__input">
                                                    <span class="mdl-switch__label"></span>
                                                </label>
                                            </td>
                                            <td class="report-event-credential">
                                                <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-event-credential-blacklist">
                                                    <input type="checkbox" id="switch-event-credential-blacklist" class="mdl-switch__input">
                                                    <span class="mdl-switch__label"></span>
                                                </label>
                                            </td>
                                        </tr>

                                        {{-- Report Entries --}}

                                        <tr id="report-entries">
                                            <td class="report-name">{{trans('reports.text_entries')}}</td>
                                            <td class="report-event">
                                                <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-event-entries">
                                                    <input type="checkbox" id="switch-event-entries" class="mdl-switch__input">
                                                    <span class="mdl-switch__label"></span>
                                                </label>
                                            </td>
                                            <td class="report-event-park">
                                                <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-event-park-entries">
                                                    <input type="checkbox" id="switch-event-park-entries" class="mdl-switch__input">
                                                    <span class="mdl-switch__label"></span>
                                                </label>
                                            </td>
                                            <td class="report-event-credential">
                                                <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-event-credential-entries">
                                                    <input type="checkbox" id="switch-event-credential-entries" class="mdl-switch__input">
                                                    <span class="mdl-switch__label"></span>
                                                </label>
                                            </td>
                                        </tr>

                                        {{-- Report Entries App --}}

                                        <tr id="report-entries-app">
                                            <td class="report-name">{{trans('reports.text_entries_app')}}</td>
                                            <td class="report-event">
                                                <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-event-entries-app">
                                                    <input type="checkbox" id="switch-event-entries-app" class="mdl-switch__input">
                                                    <span class="mdl-switch__label"></span>
                                                </label>
                                            </td>
                                            <td class="report-event-park">
                                                <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-event-park-entries-app">
                                                    <input type="checkbox" id="switch-event-park-entries-app" class="mdl-switch__input">
                                                    <span class="mdl-switch__label"></span>
                                                </label>
                                            </td>
                                            <td class="report-event-credential">
                                                <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-event-credential-entries-app">
                                                    <input type="checkbox" id="switch-event-credential-entries-app" class="mdl-switch__input">
                                                    <span class="mdl-switch__label"></span>
                                                </label>
                                            </td>
                                        </tr>

                                        {{-- Report Entries Camarotes Presidencial --}}

                                        <tr id="report-entries-presidential-box">
                                            <td class="report-name">{{trans('reports.text_entries_cam_presidencial')}}</td>
                                            <td class="report-event">
                                                <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-event-entries-presidential-box">
                                                    <input type="checkbox" id="switch-event-entries-presidential-box" class="mdl-switch__input">
                                                    <span class="mdl-switch__label"></span>
                                                </label>
                                            </td>
                                            <td class="report-event-park">
                                                <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-event-park-entries-presidential-box">
                                                    <input type="checkbox" id="switch-event-park-entries-presidential-box" class="mdl-switch__input">
                                                    <span class="mdl-switch__label"></span>
                                                </label>
                                            </td>
                                            <td class="report-event-credential">
                                                <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-event-credential-entries-presidential-box">
                                                    <input type="checkbox" id="switch-event-credential-entries-presidential-box" class="mdl-switch__input">
                                                    <span class="mdl-switch__label"></span>
                                                </label>
                                            </td>
                                        </tr>

                                        {{-- Report Entries Camarotes --}}

                                        <tr id="report-entries-box">
                                            <td class="report-name">{{trans('reports.text_entries_cam')}}</td>
                                            <td class="report-event">
                                                <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-event-entries-box">
                                                    <input type="checkbox" id="switch-event-entries-box" class="mdl-switch__input">
                                                    <span class="mdl-switch__label"></span>
                                                </label>
                                            </td>
                                            <td class="report-event-park">
                                                <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-event-park-entries-box">
                                                    <input type="checkbox" id="switch-event-park-entries-box" class="mdl-switch__input">
                                                    <span class="mdl-switch__label"></span>
                                                </label>
                                            </td>
                                            <td class="report-event-credential">
                                                <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-event-credential-entries-box">
                                                    <input type="checkbox" id="switch-event-credential-entries-box" class="mdl-switch__input">
                                                    <span class="mdl-switch__label"></span>
                                                </label>
                                            </td>
                                        </tr>

                                        {{-- Report Gate / Category --}}

                                        <tr id="report-entries-gate-category">
                                            <td class="report-name">{{trans('reports.text_entries_gate_category')}}</td>
                                            <td class="report-event">
                                                <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-event-entries-gate-category">
                                                    <input type="checkbox" id="switch-event-entries-gate-category" class="mdl-switch__input">
                                                    <span class="mdl-switch__label"></span>
                                                </label>
                                            </td>
                                            <td class="report-event-park">
                                                <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-event-park-entries-gate-category">
                                                    <input type="checkbox" id="switch-event-park-entries-gate-category" class="mdl-switch__input">
                                                    <span class="mdl-switch__label"></span>
                                                </label>
                                            </td>
                                            <td class="report-event-credential">
                                                <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-event-credential-entries-gate-category">
                                                    <input type="checkbox" id="switch-event-credential-entries-gate-category" class="mdl-switch__input">
                                                    <span class="mdl-switch__label"></span>
                                                </label>
                                            </td>
                                        </tr>

                                        {{-- Report Unique Title Errors --}}

                                        <tr id="report-unique-title-errors">
                                            <td class="report-name">{{trans('reports.text_unique_ticket_errors')}}</td>
                                            <td class="report-event">
                                                <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-event-unique-title-errors">
                                                    <input type="checkbox" id="switch-event-unique-title-errors" class="mdl-switch__input">
                                                    <span class="mdl-switch__label"></span>
                                                </label>
                                            </td>
                                            <td class="report-event-park">
                                                <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-event-park-unique-title-errors">
                                                    <input type="checkbox" id="switch-event-park-unique-title-errors" class="mdl-switch__input">
                                                    <span class="mdl-switch__label"></span>
                                                </label>
                                            </td>
                                            <td class="report-event-credential">
                                                <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-event-credential-unique-title-errors">
                                                    <input type="checkbox" id="switch-event-credential-unique-title-errors" class="mdl-switch__input">
                                                    <span class="mdl-switch__label"></span>
                                                </label>
                                            </td>
                                        </tr>

                                        {{-- Report Transactions Errors --}}

                                        <tr id="report-transactions-errors">
                                            <td class="report-name">{{trans('reports.text_transactions_errors')}}</td>
                                            <td class="report-event">
                                                <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-event-transactions-errors">
                                                    <input type="checkbox" id="switch-event-transactions-errors" class="mdl-switch__input">
                                                    <span class="mdl-switch__label"></span>
                                                </label>
                                            </td>
                                            <td class="report-event-park">
                                                <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-event-park-transactions-errors">
                                                    <input type="checkbox" id="switch-event-park-transactions-errors" class="mdl-switch__input">
                                                    <span class="mdl-switch__label"></span>
                                                </label>
                                            </td>
                                            <td class="report-event-credential">
                                                <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-event-credential-transactions-errors">
                                                    <input type="checkbox" id="switch-event-credential-transactions-errors" class="mdl-switch__input">
                                                    <span class="mdl-switch__label"></span>
                                                </label>
                                            </td>
                                        </tr>

                                        {{-- Report Fill Level Minute --}}

                                        <tr id="report-filllevel-minute">
                                            <td class="report-name">{{trans('reports.text_fill_level_minute')}}</td>
                                            <td class="report-event">
                                                <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-event-filllevel-minute">
                                                    <input type="checkbox" id="switch-event-filllevel-minute" class="mdl-switch__input">
                                                    <span class="mdl-switch__label"></span>
                                                </label>
                                            </td>
                                            <td class="report-event-park">
                                                <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-event-park-filllevel-minute">
                                                    <input type="checkbox" id="switch-event-park-filllevel-minute" class="mdl-switch__input">
                                                    <span class="mdl-switch__label"></span>
                                                </label>
                                            </td>
                                            <td class="report-event-credential">
                                                <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-event-credential-filllevel-minute">
                                                    <input type="checkbox" id="switch-event-credential-filllevel-minute" class="mdl-switch__input">
                                                    <span class="mdl-switch__label"></span>
                                                </label>
                                            </td>
                                        </tr>

                                        {{-- Report Occurencies TCP --}}

                                        <tr id="report-occurences-tcp" style="display: none;">
                                            <td class="report-name">{{trans('reports.text_occurrences_tcp')}}</td>
                                            <td class="report-event">
                                                <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-event-occurences-tcp">
                                                    <input type="checkbox" id="switch-event-occurences-tcp" class="mdl-switch__input">
                                                    <span class="mdl-switch__label"></span>
                                                </label>
                                            </td>
                                            <td class="report-event-park">
                                                <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-event-park-occurences-tcp">
                                                    <input type="checkbox" id="switch-event-park-occurences-tcp" class="mdl-switch__input">
                                                    <span class="mdl-switch__label"></span>
                                                </label>
                                            </td>
                                            <td class="report-event-credential">
                                                <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-event-credential-occurences-tcp">
                                                    <input type="checkbox" id="switch-event-credential-occurences-tcp" class="mdl-switch__input">
                                                    <span class="mdl-switch__label"></span>
                                                </label>
                                            </td>
                                        </tr>

                                        {{-- Report Ticket Entries Minute --}}

                                        <tr id="report-ticket-entries-minute">
                                            <td class="report-name">{{trans('reports.text_ticket_entries_minute')}}</td>
                                            <td class="report-event">
                                                <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-event-ticket-entries-minute">
                                                    <input type="checkbox" id="switch-event-ticket-entries-minute" class="mdl-switch__input">
                                                    <span class="mdl-switch__label"></span>
                                                </label>
                                            </td>
                                            <td class="report-event-park">
                                                <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-event-park-ticket-entries-minute">
                                                    <input type="checkbox" id="switch-event-park-ticket-entries-minute" class="mdl-switch__input">
                                                    <span class="mdl-switch__label"></span>
                                                </label>
                                            </td>
                                            <td class="report-event-credential">
                                                <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-event-credential-ticket-entries-minute">
                                                    <input type="checkbox" id="switch-event-credential-ticket-entries-minute" class="mdl-switch__input">
                                                    <span class="mdl-switch__label"></span>
                                                </label>
                                            </td>
                                        </tr>

                                        {{-- Transactions Ticket Entry Minute --}}

                                        <tr id="report-transactions-ticket-entry-minute">
                                            <td class="report-name">{{trans('reports.text_transactions_ticket_entry_min')}}</td>
                                            <td class="report-event">
                                                <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-event-transactions-ticket-entry-minute">
                                                    <input type="checkbox" id="switch-event-transactions-ticket-entry-minute" class="mdl-switch__input">
                                                    <span class="mdl-switch__label"></span>
                                                </label>
                                            </td>
                                            <td class="report-event-park">
                                                <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-event-park-transactions-ticket-entry-minute">
                                                    <input type="checkbox" id="switch-event-park-transactions-ticket-entry-minute" class="mdl-switch__input">
                                                    <span class="mdl-switch__label"></span>
                                                </label>
                                            </td>
                                            <td class="report-event-credential">
                                                <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch-event-credential-transactions-ticket-entry-minute">
                                                    <input type="checkbox" id="switch-event-credential-transactions-ticket-entry-minute" class="mdl-switch__input">
                                                    <span class="mdl-switch__label"></span>
                                                </label>
                                            </td>
                                        </tr>

                                        </tbody>

                                    </table>
                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <div class="mdl-card__menu mdl-color-text--white">

                    <div id="loading-report-animation" class="mdl-spinner mdl-spinner--single-color mdl-js-spinner is-active"></div>

                    <span class="save-actions">
                        {!! Form::button('<i class="material-icons">vertical_align_bottom</i>', array('id' => 'download-reports', 'class' => 'dialog-button-icon-save mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect', 'disabled' => 'disabled', 'title' => trans('reports.view_report')  )) !!}
                    </span>

                    {{-- <span class="save-actions">
                        {!! Form::button('<i class="material-icons">vertical_align_bottom</i>', array('id' => 'download-report', 'class' => 'dialog-button-icon-save mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect', 'disabled' => 'disabled', 'title' => trans('reports.view_report')  )) !!}
                    </span> --}}

                    <a href="{{ url('/reports/') }}" class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect mdl-color-text--white" title="{{ trans('reports.back_to_reports') }}">
                        <i class="material-icons">reply</i>
                        <span class="sr-only">{{ trans('reports.back_to_reports') }}</span>
                    </a>

                </div>

            </div>

        </div>

    </div>

    {{-- Download Reports Dialog --}}

    <dialog id="download-reports-dialog" class="mdl-dialog">

        <div id="download-report-body" class="mdl-dialog__content mdl-grid full-grid padding-0">

            <div class="mdl-cell mdl-cell--12-col-phone mdl-cell--12-col-tablet mdl-cell--12-col-desktop">

                <div id="loading-csvs" class="mdl-progress mdl-js-progress mdl-progress__indeterminate"></div>

                <div id="report-download-details" class="mdl-grid ">

                    <div class="csv-download-table-scrollable" style="width: 100%; overflow-x: auto; overflow-y: auto">
                        <table id="csv-download-table" class="mdl-data-table mdl-js-data-table mdl-shadow--2dp">

                            <thead class="mdl-color--primary custom-header-color">
                            <tr>
                                <th style="color: #ffffff !important;">{{trans('reports.text_report')}}</th>
                                <th style="color: #ffffff !important;">{{trans('forms.label-event_id')}}</th>
                                <th style="color: #ffffff !important;">{{trans('forms.label-event_park_id')}}</th>
                                <th style="color: #ffffff !important;">{{trans('forms.label-event_credential_id')}}</th>
                            </tr>
                            </thead>

                            <tbody>

                            {{-- Report Whitelist --}}

                            <tr id="report-whitelist">
                                <td class="report-name">{{trans('reports.text_whitelist')}}</td>
                                <td class="report-event"></td>
                                <td class="report-event-park"></td>
                                <td class="report-event-credential"></td>
                            </tr>

                            {{-- Report Whitelist Masters --}}

                            <tr id="report-whitelist-masters">
                                <td class="report-name">{{trans('reports.text_whitelist_masters')}}</td>
                                <td class="report-event"></td>
                                <td class="report-event-park"></td>
                                <td class="report-event-credential"></td>
                            </tr>

                            {{-- Report Blacklist --}}

                            <tr id="report-blacklist">
                                <td class="report-name">{{trans('reports.text_blacklist')}}</td>
                                <td class="report-event"></td>
                                <td class="report-event-park"></td>
                                <td class="report-event-credential"></td>
                            </tr>

                            {{-- Report Entries --}}

                            <tr id="report-entries">
                                <td class="report-name">{{trans('reports.text_entries')}}</td>
                                <td class="report-event"></td>
                                <td class="report-event-park"></td>
                                <td class="report-event-credential"></td>
                            </tr>

                            {{-- Report Entries App --}}

                            <tr id="report-entries-app">
                                <td class="report-name">{{trans('reports.text_entries_app')}}</td>
                                <td class="report-event"></td>
                                <td class="report-event-park"></td>
                                <td class="report-event-credential"></td>
                            </tr>

                            {{-- Report Entries Camarotes Presidencial --}}

                            <tr id="report-entries-presidential-box">
                                <td class="report-name">{{trans('reports.text_entries_cam_presidencial')}}</td>
                                <td class="report-event"></td>
                                <td class="report-event-park"></td>
                                <td class="report-event-credential"></td>
                            </tr>

                            {{-- Report Entries Camarotes --}}

                            <tr id="report-entries-box">
                                <td class="report-name">{{trans('reports.text_entries_cam')}}</td>
                                <td class="report-event"></td>
                                <td class="report-event-park"></td>
                                <td class="report-event-credential"></td>
                            </tr>

                            {{-- Report Gate / Category --}}

                            <tr id="report-entries-gate-category">
                                <td class="report-name">{{trans('reports.text_entries_gate_category')}}</td>
                                <td class="report-event"></td>
                                <td class="report-event-park"></td>
                                <td class="report-event-credential"></td>
                            </tr>

                            {{-- Report Unique Title Errors --}}

                            <tr id="report-unique-title-errors">
                                <td class="report-name">{{trans('reports.text_unique_ticket_errors')}}</td>
                                <td class="report-event"></td>
                                <td class="report-event-park"></td>
                                <td class="report-event-credential"></td>
                            </tr>

                            {{-- Report Transactions Errors --}}

                            <tr id="report-transactions-errors">
                                <td class="report-name">{{trans('reports.text_transactions_errors')}}</td>
                                <td class="report-event"></td>
                                <td class="report-event-park"></td>
                                <td class="report-event-credential"></td>
                            </tr>

                            {{-- Report Fill Level Minute --}}

                            <tr id="report-filllevel-minute">
                                <td class="report-name">{{trans('reports.text_fill_level_minute')}}</td>
                                <td class="report-event"></td>
                                <td class="report-event-park"></td>
                                <td class="report-event-credential"></td>
                            </tr>

                            {{-- Report Occurencies TCP --}}

                            <tr id="report-occurences-tcp" style="display: none;">
                                <td class="report-name">{{trans('reports.text_occurrences_tcp')}}</td>
                                <td class="report-event"></td>
                                <td class="report-event-park"></td>
                                <td class="report-event-credential"></td>
                            </tr>

                            {{-- Report Ticket Entries Minute --}}

                            <tr id="report-ticket-entries-minute">
                                <td class="report-name">{{trans('reports.text_ticket_entries_minute')}}</td>
                                <td class="report-event"></td>
                                <td class="report-event-park"></td>
                                <td class="report-event-credential"></td>
                            </tr>

                            {{-- Transactions Ticket Entry Minute --}}

                            <tr id="report-transactions-ticket-entry-minute">
                                <td class="report-name">{{trans('reports.text_transactions_ticket_entry_min')}}</td>
                                <td class="report-event"></td>
                                <td class="report-event-park"></td>
                                <td class="report-event-credential"></td>
                            </tr>

                            </tbody>

                            <tfoot>
                                <tr>
                                    <td colspan="4"><i class="material-icons">remove</i> {{trans('reports.no_download')}}</td>
                                </tr>
                                <tr>
                                    <td colspan="4"><i class="material-icons">show_chart</i> {{trans('reports.request_report')}}</td>
                                </tr>
                                {{--<tr>
                                    <td colspan="4"><i class="material-icons">multiline_chart</i> {{trans('reports.request_report_error')}}</td>
                                </tr>--}}
                                <tr>
                                    <td colspan="4"><i class="material-icons">file_copy</i> {{trans('reports.process_file')}}</td>
                                </tr>
                                {{--<tr>
                                    <td colspan="4"><i class="material-icons">clear</i> {{trans('reports.process_file_error')}}</td>
                                </tr>--}}
                                <tr>
                                    <td colspan="4"><i class="material-icons">save_alt</i>  {{trans('reports.success_download')}}</td>
                                </tr>
                            </tfoot>

                        </table>
                    </div>

                </div>

            </div>

        </div>

        <div class="mdl-dialog__actions--full-width">

            <button type="button" id="close-csv-download-button" class="mdl-button close">{{ trans('charts.button_cancel') }}</button>

        </div>

    </dialog>

    <div id="report-load-charts" class="mdl-js-snackbar mdl-snackbar">
        <div class="mdl-snackbar__text"></div>
        <button class="mdl-snackbar__action" type="button"></button>
    </div>

@endsection

@section('footer_scripts')

    <script>

        $('#report-event').on('change', function () {

            removeSelectionError('event');
            validateReportLoading('event');
            checkReports('event');

        });

        $('#close-csv-download-button').on('click', function() {

            var dialog = document.querySelector('#download-reports-dialog');

            dialog.close();

        });

        $('#report-event-park').on('change', function () {

            removeSelectionError('park');
            validateReportLoading('park');
            checkReports('park');

        });

        $('#report-event-credential').on('change', function () {

            removeSelectionError('credential');
            validateReportLoading('credential');
            checkReports('credential');

        });

        function removeSelectionError(event_type) {

            if (event_type == 'event') {

                var event_id = $('#report-event option:selected').val();

                if (event_id !== "undefined" && event_id != '') {

                    document.getElementById("error-select-event").style.visibility = "hidden";
                    $('#report-event').parent().removeClass('is-invalid');

                }

            } else if (event_type == 'park') {

                var event_park_id = $('#report-event-park option:selected').val();

                if (event_park_id !== "undefined" && event_park_id != '') {

                    document.getElementById("error-select-event-park").style.visibility = "hidden";
                    $('#report-event-park').parent().removeClass('is-invalid');

                }

            } else if (event_type == 'credential') {

                var event_credential_id = $('#report-event-credential option:selected').val();

                if (event_credential_id !== "undefined" && event_credential_id != '') {

                    document.getElementById("error-select-event-credential").style.visibility = "hidden";
                    $('#report-event-credential').parent().removeClass('is-invalid');

                }

            }

        }

        function validateReportLoading(event_type) {

            if (event_type == 'event') {

                var event_id = $('#report-event option:selected').val();

                if (event_id !== "undefined" && event_id != '') {

                    eventsSelectionError(event_type, event_id);
                    removeSelectionError(event_type);
                    allowDownload();

                } else {

                    eventsSelectionError(event_type, event_id);

                }

            }

            if (event_type == 'park') {

                var event_park_id = $('#report-event-park option:selected').val();

                if (event_park_id !== "undefined" && event_park_id != '') {

                    eventsSelectionError(event_type, event_park_id);
                    removeSelectionError(event_type);
                    allowDownload();

                } else {

                    eventsSelectionError(event_type, event_park_id);

                }

            }

            if (event_type == 'credential') {

                var event_credential_id = $('#report-event-credential option:selected').val();

                if (event_credential_id !== "undefined" && event_credential_id != '') {

                    eventsSelectionError(event_type, event_credential_id);
                    removeSelectionError(event_type);
                    allowDownload();

                } else {

                    eventsSelectionError(event_type, event_credential_id);

                }

            }

        }

        function checkReports(event_type) {

            var event_id = $('#report-event option:selected').val();
            var event_park_id = $('#report-event-park option:selected').val();
            var event_credential_id = $('#report-event-credential option:selected').val();

            if (event_id == "undefined" && event_id == '' && event_park_id == "undefined" && event_park_id == '' && event_credential_id == "undefined" && event_credential_id == '') {

                $('#download-report').attr('disabled', true);
                $('#download-reports').attr('disabled', true);

            }

            if (event_type == 'event') {

                if (event_id !== "undefined" && event_id != '') {

                    allowDownload();

                    $('#csv-reports-type-table tbody tr td.report-event label').each(function(row, item) {

                        $(item).attr('checked', 'checked');
                        $(item).addClass('is-checked');

                    });

                } else {

                    $('#csv-reports-type-table tbody tr td.report-event label').each(function(row, item) {

                        $(item).removeAttr('checked');
                        $(item).removeClass('is-checked');

                    });

                }

            }

            if (event_type == 'park') {

                if (event_park_id !== "undefined" && event_park_id != '') {

                    allowDownload();

                    $('#csv-reports-type-table tbody tr td.report-event-park label').each(function(row, item) {

                        var id =  $(item).parent().parent().attr('id');

                        switch (id) {

                            case 'report-whitelist':
                                $(item).attr('checked', 'checked');
                                $(item).addClass('is-checked');
                                break;
                            case 'report-entries':
                                $(item).attr('checked', 'checked');
                                $(item).addClass('is-checked');
                                break;
                            case 'report-unique-title-errors':
                                $(item).attr('checked', 'checked');
                                $(item).addClass('is-checked');
                                break;
                            case 'report-transactions-errors':
                                $(item).attr('checked', 'checked');
                                $(item).addClass('is-checked');
                                break;
                        }

                    });

                } else {

                    $('#csv-reports-type-table tbody tr td.report-event-park label').each(function(row, item) {

                        $(item).removeAttr('checked');
                        $(item).removeClass('is-checked');

                    });

                }

            }

            if (event_type == 'credential') {

                var event_credential_id = $('#report-event-credential option:selected').val();

                if (event_credential_id !== "undefined" && event_credential_id != '') {

                    allowDownload();

                    $('#csv-reports-type-table tbody tr td.report-event-credential label').each(function(row, item) {

                        var id =  $(item).parent().parent().attr('id');

                        switch (id) {

                            case 'report-entries':
                                $(item).attr('checked', 'checked');
                                $(item).addClass('is-checked');
                                break;
                            case 'report-unique-title-errors':
                                $(item).attr('checked', 'checked');
                                $(item).addClass('is-checked');
                                break;
                            case 'report-transactions-errors':
                                $(item).attr('checked', 'checked');
                                $(item).addClass('is-checked');
                                break;
                        }

                    });

                } else {

                    $('#csv-reports-type-table tbody tr td.report-event-credential label').each(function(row, item) {

                        $(item).removeAttr('checked');
                        $(item).removeClass('is-checked');

                    });

                }

            }

            componentHandler.upgradeDom();

        }

        function eventsSelectionError(event_type, event_id) {

            if (event_type == 'event' && (event_id == "undefined" || !event_id)) {
                document.getElementById("error-select-event").style.visibility = "visible";
                $('#report-event').parent().addClass('is-invalid');
                checkReports(event_type);
            }

            if (event_type == 'park' && (event_id == "undefined" || !event_id)) {
                document.getElementById("error-select-event-park").style.visibility = "visible";
                $('#report-event-park').parent().addClass('is-invalid');
            }

            if (event_type == 'credential' && (event_id == "undefined" || !event_id)) {
                document.getElementById("error-select-event-credential").style.visibility = "visible";
                $('#report-event-credential').parent().addClass('is-invalid');
            }

        }

        function allowDownload() {

            $('#download-report').removeAttr('disabled');
            $('#download-reports').removeAttr('disabled');

        }

        $('#download-report').on('click', function() {

            var event_id = $('#report-event option:selected').val();
            var event_park_id = $('#report-event-park option:selected').val();
            var event_credential_id = $('#report-event-credential option:selected').val();

            var event_csvs = [];
            var event_park_csvs = [];
            var event_credentials_csvs = [];

            $('#csv-reports-type-table tbody tr td:first-of-type label.is-checked').each(function(row, item) {

                var row_report = $(item).parent().parent().attr('id');

                if (event_id !== "undefined" && event_id != '') {

                    var clicked = $('tr#'+row_report+' > td.report-event > label.is-checked');

                    if (clicked.length > 0) {
                        event_csvs.push(row_report);
                    }

                }

                if (event_park_id !== "undefined" && event_park_id != '') {

                    var clicked = $('tr#'+row_report+' > td.report-event-park > label.is-checked');

                    if (clicked.length > 0) {
                        event_park_csvs.push(row_report);
                    }

                }

                if (event_credential_id !== "undefined" && event_credential_id != '') {

                    var clicked = $('tr#'+row_report+' > td.report-event-credential > label.is-checked');

                    if (clicked.length > 0) {
                        event_credentials_csvs.push(row_report);
                    }

                }

            });

            $.ajax({
                type: 'POST',
                url: '/reports/csvs/generate',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                dataType: 'json',
                data: {
                    event_id: event_id,
                    event: event_csvs,
                    event_park_id: event_park_id,
                    event_park: event_park_csvs,
                    event_credential_id: event_credential_id,
                    event_credential: event_credentials_csvs
                },
                beforeSend: function() {

                    $('#loading-report-animation').css('display', 'inline-block');

                    $('#download-report').attr('disabled', 'disabled');

                },
                success: function (data) {

                    console.log(data);

                    $('#loading-report-animation').css('display', 'none');

                    $('#download-report').removeAttr('disabled');

                    try {

                        data['event'].forEach(function (csv) {

                            var report_file = csv.header + '\n';

                            if (csv.from == 'report-whitelist' || csv.from == 'report-whitelist-masters' || csv.from == 'report-blacklist') {

                                csv.file.forEach(function(line) {

                                    report_file += '' + line.id + ',' + line.titulo + ',' + line.isWhiteList + ',' + line.dataExpiracao + ',' + line.importSystem_id + ',' + line.lockImp + ',' + line.blMessage + ',' + line.fromBL + ',' + line.toBL + ',' + line.upid + ',' + line.ativo + ',' + line.apagado + ',' + line.codEvent + ',' + line.propriedades + ',' + line.valores + ',' + line.dataHoraCriacao + ',' + line.utilizadorCriacao + ',' + line.dataHoraAlteracao + ',' + line.utilizadorAlteracao + ',' + line.utid1 + ',' + line.socio + ',' + line.nsocio + ',' + line.categoria + ',' + line.porta + '\n';

                                });

                            }

                            if (csv.from == 'report-entries' || csv.from == 'report-entries-app' || csv.from == 'report-entries-presidential-box') {

                                csv.file.forEach(function(line) {

                                    report_file += '' + line.jogo + ',' + line.datahora + ',' + line.porta + ',' + line.dac + ',' + line.ip + ',' + line.codigobarras + ',' + line.socio + ',' + line.nsocio + ',' + line.tipotitulo + ',' + line.categoria + ',' + line.tipoleitor + '\n';

                                });

                            }

                            if (csv.from == 'report-entries-box') {

                                csv.file.forEach(function(line) {

                                    report_file += '' + line.entidade + ',' + line.jogo + ',' + line.datahora + ',' + line.porta + ',' + line.dac + ',' + line.ip + ',' + line.titulo + ',' + line.socio + ',' + line.nsocio + ',' + line.tipotitulo + ',' + line.categoria + ',' + line.tipoleitor + ',' + line.camarote + '\n';

                                });

                            }

                            if (csv.from == 'report-entries-gate-category') {

                                csv.file.forEach(function(line) {

                                    report_file += '' + line['Porta'] + ',' + line['53'] + ',' + line['54'] + ',' + line['57'] + ',' + line['59'] + ',' + line['99'] + ',' + line['Total'] + '\n';

                                });

                            }

                            if (csv.from == 'report-unique-title-errors') {

                                csv.file.forEach(function(line) {

                                    report_file += '' + line['Socio_N'] + ',' + line['NSocio'] + ',' + line['Categoria'] + ',' + line['Titulo'] + ',' + line['Tipo'] + ',' + line['Erro'] + ',' + line['Mensagem_Blacklist'] + '\n';

                                });

                            }

                            if (csv.from == 'report-transactions-errors') {

                                csv.file.forEach(function(line) {

                                    report_file += '' + line['Socio'] + ',' + line['NSocio'] + ',' + line['Categoria'] + ',' + line['Porta'] + ',' + line['Titulo'] + ',' + line['Tipo'] + ',' + line['Erro'] + ',' + line['Mensagem_Blacklist'] + ',' + line['Datetime'] + ',' + line['DAC'] + '\n';

                                });

                            }

                            if (csv.from == 'report-filllevel-minute') {

                                csv.file.forEach(function(line) {

                                    report_file += '' + line['hour_transaction'] + ',' + line['entries'] + '\n';

                                });

                            }

                            if (csv.from == 'report-ticket-entries-minute') {

                                csv.file.forEach(function(line) {

                                    report_file += '' + line['Socio'] + ',' + line['NSocio'] + ',' + line['Categoria'] + ',' + line['Titulo'] + '\n';

                                });

                            }

                            if (csv.from == 'report-transactions-ticket-entry-minute') {

                                csv.file.forEach(function(line) {

                                    report_file += '' + line['datahora'] + ',' + line['tipo'] + ',' + line['porta'] + ',' + line['dac'] + ',' + line['socio'] + ',' + line['nsocio'] + ',' + line['categoria'] + '\n';

                                });

                            }

                            var hiddenElement = document.createElement('a');
                            var blob = new Blob([report_file],{type: 'text/csv;charset=utf-8;'});
                            hiddenElement.href = URL.createObjectURL(blob);
                            hiddenElement.target = '_blank';
                            hiddenElement.download = csv.filename;
                            hiddenElement.click();

                        });

                        data['event_park'].forEach(function (csv) {

                            var report_file = csv.header + '\n';

                            if (csv.from == 'report-whitelist' || csv.from == 'report-whitelist-masters' || csv.from == 'report-blacklist') {

                                csv.file.forEach(function (line) {

                                    report_file += '' + line.id + ',' + line.titulo + ',' + line.isWhiteList + ',' + line.dataExpiracao + ',' + line.importSystem_id + ',' + line.lockImp + ',' + line.blMessage + ',' + line.fromBL + ',' + line.toBL + ',' + line.upid + ',' + line.ativo + ',' + line.apagado + ',' + line.codEvent + ',' + line.propriedades + ',' + line.valores + ',' + line.dataHoraCriacao + ',' + line.utilizadorCriacao + ',' + line.dataHoraAlteracao + ',' + line.utilizadorAlteracao + ',' + line.utid1 + ',' + line.socio + ',' + line.nsocio + ',' + line.categoria + ',' + line.porta + '\n';

                                });

                            }

                            if (csv.from == 'report-entries' || csv.from == 'report-entries-app' || csv.from == 'report-entries-presidential-box') {

                                csv.file.forEach(function(line) {

                                    report_file += '' + line.jogo + ',' + line.datahora + ',' + line.porta + ',' + line.dac + ',' + line.ip + ',' + line.codigobarras + ',' + line.socio + ',' + line.nsocio + ',' + line.tipotitulo + ',' + line.categoria + ',' + line.tipoleitor + '\n';

                                });

                            }

                            if (csv.from == 'report-entries-box') {

                                csv.file.forEach(function(line) {

                                    report_file += '' + line.entidade + ',' + line.jogo + ',' + line.datahora + ',' + line.porta + ',' + line.dac + ',' + line.ip + ',' + line.titulo + ',' + line.socio + ',' + line.nsocio + ',' + line.tipotitulo + ',' + line.categoria + ',' + line.tipoleitor + ',' + line.camarote + '\n';

                                });

                            }

                            if (csv.from == 'report-entries-gate-category') {

                                csv.file.forEach(function(line) {

                                    report_file += '' + line['Porta'] + ',' + line['53'] + ',' + line['54'] + ',' + line['57'] + ',' + line['59'] + ',' + line['99'] + ',' + line['Total'] + '\n';

                                });

                            }

                            if (csv.from == 'report-unique-title-errors') {

                                csv.file.forEach(function(line) {

                                    report_file += '' + line['Socio_N'] + ',' + line['NSocio'] + ',' + line['Categoria'] + ',' + line['Titulo'] + ',' + line['Tipo'] + ',' + line['Erro'] + ',' + line['Mensagem_Blacklist'] + '\n';

                                });

                            }

                            if (csv.from == 'report-transactions-errors') {

                                csv.file.forEach(function(line) {

                                    report_file += '' + line['Socio'] + ',' + line['NSocio'] + ',' + line['Categoria'] + ',' + line['Porta'] + ',' + line['Titulo'] + ',' + line['Tipo'] + ',' + line['Erro'] + ',' + line['Mensagem_Blacklist'] + ',' + line['Datetime'] + ',' + line['DAC'] + '\n';

                                });

                            }

                            if (csv.from == 'report-filllevel-minute') {

                                csv.file.forEach(function(line) {

                                    report_file += '' + line['hour_transaction'] + ',' + line['entries'] + '\n';

                                });

                            }

                            if (csv.from == 'report-ticket-entries-minute') {

                                csv.file.forEach(function(line) {

                                    report_file += '' + line['Socio'] + ',' + line['NSocio'] + ',' + line['Categoria'] + ',' + line['Titulo'] + '\n';

                                });

                            }

                            if (csv.from == 'report-transactions-ticket-entry-minute') {

                                csv.file.forEach(function(line) {

                                    report_file += '' + line['datahora'] + ',' + line['tipo'] + ',' + line['porta'] + ',' + line['dac'] + ',' + line['socio'] + ',' + line['nsocio'] + ',' + line['categoria'] + '\n';

                                });

                            }

                            var hiddenElement = document.createElement('a');
                            var blob = new Blob([report_file],{type: 'text/csv;charset=utf-8;'});
                            hiddenElement.href = URL.createObjectURL(blob);
                            hiddenElement.target = '_blank';
                            hiddenElement.download = csv.filename;
                            hiddenElement.click();

                        });

                        data['event_credential'].forEach(function (csv) {

                            var report_file = csv.header + '\n';

                            if (csv.from == 'report-whitelist' || csv.from == 'report-whitelist-masters' || csv.from == 'report-blacklist') {

                                csv.file.forEach(function (line) {

                                    report_file += '' + line.id + ',' + line.titulo + ',' + line.isWhiteList + ',' + line.dataExpiracao + ',' + line.importSystem_id + ',' + line.lockImp + ',' + line.blMessage + ',' + line.fromBL + ',' + line.toBL + ',' + line.upid + ',' + line.ativo + ',' + line.apagado + ',' + line.codEvent + ',' + line.propriedades + ',' + line.valores + ',' + line.dataHoraCriacao + ',' + line.utilizadorCriacao + ',' + line.dataHoraAlteracao + ',' + line.utilizadorAlteracao + ',' + line.utid1 + ',' + line.socio + ',' + line.nsocio + ',' + line.categoria + ',' + line.porta + '\n';

                                });

                            }

                            if (csv.from == 'report-entries') {

                                csv.file.forEach(function(line) {

                                    report_file += '' + line.evento + ',' + line.datahora + ',' + line.codigobarras + ',' + line.tipotitulo + ',' + line.entidade + ',' + line.credenciado + ',' + line.reentrada + ',' + line.porta + ',' + line.dac + ',' + line.ip + '\n';

                                });

                            }

                            if (csv.from == 'report-entries-app' || csv.from == 'report-entries-presidential-box') {

                                csv.file.forEach(function(line) {

                                    report_file += '' + line.jogo + ',' + line.datahora + ',' + line.porta + ',' + line.dac + ',' + line.ip + ',' + line.codigobarras + ',' + line.socio + ',' + line.nsocio + ',' + line.tipotitulo + ',' + line.categoria + ',' + line.tipoleitor + '\n';

                                });

                            }

                            if (csv.from == 'report-entries-box') {

                                csv.file.forEach(function(line) {

                                    report_file += '' + line.entidade + ',' + line.jogo + ',' + line.datahora + ',' + line.porta + ',' + line.dac + ',' + line.ip + ',' + line.titulo + ',' + line.socio + ',' + line.nsocio + ',' + line.tipotitulo + ',' + line.categoria + ',' + line.tipoleitor + ',' + line.camarote + '\n';

                                });

                            }

                            if (csv.from == 'report-entries-gate-category') {

                                csv.file.forEach(function(line) {

                                    report_file += '' + line['Porta'] + ',' + line['53'] + ',' + line['54'] + ',' + line['57'] + ',' + line['59'] + ',' + line['99'] + ',' + line['Total'] + '\n';

                                });

                            }

                            if (csv.from == 'report-unique-title-errors') {

                                csv.file.forEach(function(line) {

                                    report_file += '' + line['Socio_N'] + ',' + line['NSocio'] + ',' + line['Categoria'] + ',' + line['Titulo'] + ',' + line['Tipo'] + ',' + line['Erro'] + ',' + line['Mensagem_Blacklist'] + '\n';

                                });

                            }

                            if (csv.from == 'report-transactions-errors') {

                                csv.file.forEach(function(line) {

                                    report_file += '' + line['Socio'] + ',' + line['NSocio'] + ',' + line['Categoria'] + ',' + line['Porta'] + ',' + line['Titulo'] + ',' + line['Tipo'] + ',' + line['Erro'] + ',' + line['Mensagem_Blacklist'] + ',' + line['Datetime'] + ',' + line['DAC'] + '\n';

                                });

                            }

                            if (csv.from == 'report-filllevel-minute') {

                                csv.file.forEach(function(line) {

                                    report_file += '' + line['hour_transaction'] + ',' + line['entries'] + '\n';

                                });

                            }

                            if (csv.from == 'report-ticket-entries-minute') {

                                csv.file.forEach(function(line) {

                                    report_file += '' + line['Socio'] + ',' + line['NSocio'] + ',' + line['Categoria'] + ',' + line['Titulo'] + '\n';

                                });

                            }

                            if (csv.from == 'report-transactions-ticket-entry-minute') {

                                csv.file.forEach(function(line) {

                                    report_file += '' + line['datahora'] + ',' + line['tipo'] + ',' + line['porta'] + ',' + line['dac'] + ',' + line['socio'] + ',' + line['nsocio'] + ',' + line['categoria'] + '\n';

                                });

                            }

                            var hiddenElement = document.createElement('a');
                            var blob = new Blob([report_file],{type: 'text/csv;charset=utf-8;'});
                            hiddenElement.href = URL.createObjectURL(blob);
                            hiddenElement.target = '_blank';
                            hiddenElement.download = csv.filename;
                            hiddenElement.click();

                        });

                    } catch (error) {

                        console.log(error.message);

                    }

                }

            });

        });

        $('#download-reports').on('click', function() {

            var event_id = $('#report-event option:selected').val();
            var event_park_id = $('#report-event-park option:selected').val();
            var event_credential_id = $('#report-event-credential option:selected').val();

            var whitelist = [];
            var whitelist_masters = [];
            var blacklist = [];
            var entries = [];
            var entries_app = [];
            var entries_presidential_box = [];
            var entries_boxes = [];
            var entries_gate_category = [];
            var errors_unique_title = [];
            var errors_transactions = [];
            var tcp_occurrences = [];
            var fill_level_minute = [];
            var entries_minute_by_title = [];
            var transactions_minute_entries = [];

            $('#csv-reports-type-table tbody tr td:first-of-type label.is-checked').each(function(row, item) {

                var row_report = $(item).parent().parent().attr('id');

                if (event_id !== "undefined" && event_id != '') {

                    var clicked = $('tr#'+row_report+' > td.report-event > label.is-checked');

                    if (clicked.length > 0) {

                        switch (row_report) {

                            case 'report-whitelist':
                                whitelist.push('event');
                                break;
                            case 'report-whitelist-masters':
                                whitelist_masters.push('event');
                                break;
                            case 'report-blacklist':
                                blacklist.push('event');
                                break;
                            case 'report-entries':
                                entries.push('event');
                                break;
                            case 'report-entries-app':
                                entries_app.push('event');
                                break;
                            case 'report-entries-presidential-box':
                                entries_presidential_box.push('event');
                                break;
                            case 'report-entries-box':
                                entries_boxes.push('event');
                                break;
                            case 'report-entries-gate-category':
                                entries_gate_category.push('event');
                                break;
                            case 'report-unique-title-errors':
                                errors_unique_title.push('event');
                                break;
                            case 'report-transactions-errors':
                                errors_transactions.push('event');
                                break;
                            case 'report-filllevel-minute':
                                fill_level_minute.push('event');
                                break;
                            case 'report-occurences-tcp':
                                tcp_occurrences.push('event');
                                break;
                            case 'report-ticket-entries-minute':
                                entries_minute_by_title.push('event');
                                break;
                            case 'report-transactions-ticket-entry-minute':
                                transactions_minute_entries.push('event');
                                break;
                        }

                    }

                }

                if (event_park_id !== "undefined" && event_park_id != '') {

                    var clicked = $('tr#'+row_report+' > td.report-event-park > label.is-checked');

                    if (clicked.length > 0) {

                        switch (row_report) {

                            case 'report-whitelist':
                                whitelist.push('event-park');
                                break;
                            case 'report-whitelist-masters':
                                whitelist_masters.push('event-park');
                                break;
                            case 'report-blacklist':
                                blacklist.push('event-park');
                                break;
                            case 'report-entries':
                                entries.push('event-park');
                                break;
                            case 'report-entries-app':
                                entries_app.push('event-park');
                                break;
                            case 'report-entries-presidential-box':
                                entries_presidential_box.push('event-park');
                                break;
                            case 'report-entries-box':
                                entries_boxes.push('event-park');
                                break;
                            case 'report-entries-gate-category':
                                entries_gate_category.push('event-park');
                                break;
                            case 'report-unique-title-errors':
                                errors_unique_title.push('event-park');
                                break;
                            case 'report-transactions-errors':
                                errors_transactions.push('event-park');
                                break;
                            case 'report-filllevel-minute':
                                fill_level_minute.push('event-park');
                                break;
                            case 'report-occurences-tcp':
                                tcp_occurrences.push('event-park');
                                break;
                            case 'report-ticket-entries-minute':
                                entries_minute_by_title.push('event-park');
                                break;
                            case 'report-transactions-ticket-entry-minute':
                                transactions_minute_entries.push('event-park');
                                break;

                        }

                    }

                }

                if (event_credential_id !== "undefined" && event_credential_id != '') {

                    var clicked = $('tr#'+row_report+' > td.report-event-credential > label.is-checked');

                    if (clicked.length > 0) {

                        switch (row_report) {

                            case 'report-whitelist':
                                whitelist.push('event-credential');
                                break;
                            case 'report-whitelist-masters':
                                whitelist_masters.push('event-credential');
                                break;
                            case 'report-blacklist':
                                blacklist.push('event-credential');
                                break;
                            case 'report-entries':
                                entries.push('event-credential');
                                break;
                            case 'report-entries-app':
                                entries_app.push('event-credential');
                                break;
                            case 'report-entries-presidential-box':
                                entries_presidential_box.push('event-credential');
                                break;
                            case 'report-entries-box':
                                entries_boxes.push('event-credential');
                                break;
                            case 'report-entries-gate-category':
                                entries_gate_category.push('event-credential');
                                break;
                            case 'report-unique-title-errors':
                                errors_unique_title.push('event-credential');
                                break;
                            case 'report-transactions-errors':
                                errors_transactions.push('event-credential');
                                break;
                            case 'report-filllevel-minute':
                                fill_level_minute.push('event-credential');
                                break;
                            case 'report-occurences-tcp':
                                tcp_occurrences.push('event-credential');
                                break;
                            case 'report-ticket-entries-minute':
                                entries_minute_by_title.push('event-credential');
                                break;
                            case 'report-transactions-ticket-entry-minute':
                                transactions_minute_entries.push('event-credential');
                                break;

                        }

                    }

                }

            });

            if (whitelist.length < 1 && whitelist_masters.length < 1 && blacklist.length < 1 && entries.length < 1 && entries_app.length < 1 && entries_presidential_box.length < 1 && entries_boxes.length < 1 && entries_gate_category.length < 1 && errors_unique_title.length < 1 && errors_transactions.length < 1 && tcp_occurrences.length < 1 && fill_level_minute.length < 1 && entries_minute_by_title.length < 1 && transactions_minute_entries.length < 1) {

                var snackbarContainer = document.querySelector('#report-load-charts');

                var data = {message: '{{ trans('reports.please_choose_event') }}' };

                snackbarContainer.MaterialSnackbar.showSnackbar(data);

            } else {

                var dialog = document.querySelector('#download-reports-dialog');

                dialog.showModal();

                $('#loading-csvs').css('display', 'block');

                $.when(

                    $.ajax({
                        type: 'POST',
                        url: '/reports/csvs/generate/whitelist',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        dataType: 'json',
                        data: {
                            event_id: event_id,
                            event_park_id: event_park_id,
                            event_credential_id: event_credential_id,
                            generate: whitelist
                        },
                        beforeSend: function() {

                            $('#csv-download-table > tbody > #report-whitelist > .report-event > *').remove();
                            $('#csv-download-table > tbody > #report-whitelist > .report-event-park > *').remove();
                            $('#csv-download-table > tbody > #report-whitelist > .report-event-credential > *').remove();

                            if (whitelist.includes('event')) {

                                $('#csv-download-table > tbody > #report-whitelist > .report-event').append('<i class="material-icons">show_chart</i>');

                            } else {

                                $('#csv-download-table > tbody > #report-whitelist > .report-event').append('<i class="material-icons">remove</i>');

                            }

                            if (whitelist.includes('event-park')) {

                                $('#csv-download-table > tbody > #report-whitelist > .report-event-park').append('<i class="material-icons">show_chart</i>');

                            } else {

                                $('#csv-download-table > tbody > #report-whitelist > .report-event-park').append('<i class="material-icons">remove</i>');

                            }

                            if (whitelist.includes('event-credential')) {

                                $('#csv-download-table > tbody > #report-whitelist > .report-event-credential').append('<i class="material-icons">show_chart</i>');

                            } else {

                                $('#csv-download-table > tbody > #report-whitelist > .report-event-credential').append('<i class="material-icons">remove</i>');

                            }

                        },
                        success: function(data) {

                            try {

                                if (whitelist.includes('event')) {

                                    $('#csv-download-table > tbody > #report-whitelist > .report-event > *').remove();
                                    $('#csv-download-table > tbody > #report-whitelist > .report-event').append('<i class="material-icons">file_copy</i>');

                                    data['event'].forEach(function (csv) {

                                        var report_file = csv.header + '\n';

                                        csv.file.forEach(function(line) {

                                            report_file += '' + line.id + ',' + line.titulo + ',' + line.isWhiteList + ',' + line.dataExpiracao + ',' + line.importSystem_id + ',' + line.lockImp + ',' + line.blMessage + ',' + line.fromBL + ',' + line.toBL + ',' + line.upid + ',' + line.ativo + ',' + line.apagado + ',' + line.codEvent + ',' + line.propriedades + ',' + line.valores + ',' + line.dataHoraCriacao + ',' + line.utilizadorCriacao + ',' + line.dataHoraAlteracao + ',' + line.utilizadorAlteracao + ',' + line.utid1 + ',' + line.socio + ',' + line.nsocio + ',' + line.categoria + ',' + line.porta + '\n';

                                        });

                                        var hiddenElement = document.createElement('a');
                                        var blob = new Blob([report_file],{type: 'text/csv;charset=utf-8;'});
                                        hiddenElement.href = URL.createObjectURL(blob);
                                        hiddenElement.target = '_blank';
                                        hiddenElement.download = csv.filename;
                                        hiddenElement.click();

                                        $('#csv-download-table > tbody > #report-whitelist > .report-event > *').remove();
                                        $('#csv-download-table > tbody > #report-whitelist > .report-event').append('<i class="material-icons">save_alt</i>');

                                    });

                                }

                                if (whitelist.includes('event-park')) {

                                    $('#csv-download-table > tbody > #report-whitelist > .report-event-park > *').remove();
                                    $('#csv-download-table > tbody > #report-whitelist > .report-event-park').append('<i class="material-icons">file_copy</i>');

                                    data['event_park'].forEach(function (csv) {

                                        var report_file = csv.header + '\n';

                                        csv.file.forEach(function(line) {

                                            report_file += '' + line.id + ',' + line.titulo + ',' + line.isWhiteList + ',' + line.dataExpiracao + ',' + line.importSystem_id + ',' + line.lockImp + ',' + line.blMessage + ',' + line.fromBL + ',' + line.toBL + ',' + line.upid + ',' + line.ativo + ',' + line.apagado + ',' + line.codEvent + ',' + line.propriedades + ',' + line.valores + ',' + line.dataHoraCriacao + ',' + line.utilizadorCriacao + ',' + line.dataHoraAlteracao + ',' + line.utilizadorAlteracao + ',' + line.utid1 + ',' + line.socio + ',' + line.nsocio + ',' + line.categoria + ',' + line.porta + '\n';

                                        });

                                        var hiddenElement = document.createElement('a');
                                        var blob = new Blob([report_file],{type: 'text/csv;charset=utf-8;'});
                                        hiddenElement.href = URL.createObjectURL(blob);
                                        hiddenElement.target = '_blank';
                                        hiddenElement.download = csv.filename;
                                        hiddenElement.click();

                                        $('#csv-download-table > tbody > #report-whitelist > .report-event-park > *').remove();
                                        $('#csv-download-table > tbody > #report-whitelist > .report-event-park').append('<i class="material-icons">save_alt</i>');

                                    });

                                }

                                if (whitelist.includes('event-credential')) {

                                    $('#csv-download-table > tbody > #report-whitelist > .report-event-credential > *').remove();
                                    $('#csv-download-table > tbody > #report-whitelist > .report-event-credential').append('<i class="material-icons">file_copy</i>');

                                    data['event_credential'].forEach(function (csv) {

                                        var report_file = csv.header + '\n';

                                        csv.file.forEach(function(line) {

                                            report_file += '' + line.id + ',' + line.titulo + ',' + line.isWhiteList + ',' + line.dataExpiracao + ',' + line.importSystem_id + ',' + line.lockImp + ',' + line.blMessage + ',' + line.fromBL + ',' + line.toBL + ',' + line.upid + ',' + line.ativo + ',' + line.apagado + ',' + line.codEvent + ',' + line.propriedades + ',' + line.valores + ',' + line.dataHoraCriacao + ',' + line.utilizadorCriacao + ',' + line.dataHoraAlteracao + ',' + line.utilizadorAlteracao + ',' + line.utid1 + ',' + line.socio + ',' + line.nsocio + ',' + line.categoria + ',' + line.porta + '\n';

                                        });

                                        var hiddenElement = document.createElement('a');
                                        var blob = new Blob([report_file],{type: 'text/csv;charset=utf-8;'});
                                        hiddenElement.href = URL.createObjectURL(blob);
                                        hiddenElement.target = '_blank';
                                        hiddenElement.download = csv.filename;
                                        hiddenElement.click();

                                        $('#csv-download-table > tbody > #report-whitelist > .report-event-credential > *').remove();
                                        $('#csv-download-table > tbody > #report-whitelist > .report-event-credential').append('<i class="material-icons">save_alt</i>');

                                    });

                                }

                                return true;

                            } catch (error) {

                                console.log(error.message);

                                return false;

                            }

                        },
                        error: function(error) {

                            console.log('Error Whitelist');
                            console.log(error);

                            return false;

                        }

                    }),

                    $.ajax({
                        type: 'POST',
                        url: '/reports/csvs/generate/whitelist_masters',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        dataType: 'json',
                        data: {
                            event_id: event_id,
                            event_park_id: event_park_id,
                            event_credential_id: event_credential_id,
                            generate: whitelist_masters
                        },
                        beforeSend: function() {

                            $('#csv-download-table > tbody > #report-whitelist-masters > .report-event > *').remove();
                            $('#csv-download-table > tbody > #report-whitelist-masters > .report-event-park > *').remove();
                            $('#csv-download-table > tbody > #report-whitelist-masters > .report-event-credential > *').remove();


                            if (whitelist_masters.includes('event')) {

                                $('#csv-download-table > tbody > #report-whitelist-masters > .report-event').append('<i class="material-icons">show_chart</i>');

                            } else {

                                $('#csv-download-table > tbody > #report-whitelist-masters > .report-event').append('<i class="material-icons">remove</i>');

                            }

                            if (whitelist_masters.includes('event-park')) {

                                $('#csv-download-table > tbody > #report-whitelist-masters > .report-event-park').append('<i class="material-icons">show_chart</i>');

                            } else {

                                $('#csv-download-table > tbody > #report-whitelist-masters > .report-event-park').append('<i class="material-icons">remove</i>');

                            }

                            if (whitelist_masters.includes('event-credential')) {

                                $('#csv-download-table > tbody > #report-whitelist-masters > .report-event-credential').append('<i class="material-icons">show_chart</i>');

                            } else {

                                $('#csv-download-table > tbody > #report-whitelist-masters > .report-event-credential').append('<i class="material-icons">remove</i>');

                            }

                        },
                        success: function (data) {

                            try {

                                if (whitelist_masters.includes('event')) {

                                    $('#csv-download-table > tbody > #report-whitelist-masters > .report-event > *').remove();
                                    $('#csv-download-table > tbody > #report-whitelist-masters > .report-event').append('<i class="material-icons">file_copy</i>');


                                    data['event'].forEach(function (csv) {

                                        var report_file = csv.header + '\n';

                                        csv.file.forEach(function(line) {

                                            report_file += '' + line.id + ',' + line.titulo + ',' + line.isWhiteList + ',' + line.dataExpiracao + ',' + line.importSystem_id + ',' + line.lockImp + ',' + line.blMessage + ',' + line.fromBL + ',' + line.toBL + ',' + line.upid + ',' + line.ativo + ',' + line.apagado + ',' + line.codEvent + ',' + line.propriedades + ',' + line.valores + ',' + line.dataHoraCriacao + ',' + line.utilizadorCriacao + ',' + line.dataHoraAlteracao + ',' + line.utilizadorAlteracao + ',' + line.utid1 + ',' + line.socio + ',' + line.nsocio + ',' + line.categoria + ',' + line.porta + '\n';

                                        });


                                        var hiddenElement = document.createElement('a');
                                        var blob = new Blob([report_file],{type: 'text/csv;charset=utf-8;'});
                                        hiddenElement.href = URL.createObjectURL(blob);
                                        hiddenElement.target = '_blank';
                                        hiddenElement.download = csv.filename;
                                        hiddenElement.click();

                                        $('#csv-download-table > tbody > #report-whitelist-masters > .report-event > *').remove();
                                        $('#csv-download-table > tbody > #report-whitelist-masters > .report-event').append('<i class="material-icons">save_alt</i>');

                                    });

                                }

                                if (whitelist_masters.includes('event-park')) {

                                    $('#csv-download-table > tbody > #report-whitelist-masters > .report-event-park > *').remove();
                                    $('#csv-download-table > tbody > #report-whitelist-masters > .report-event-park').append('<i class="material-icons">file_copy</i>');

                                    data['event_park'].forEach(function (csv) {

                                        var report_file = csv.header + '\n';

                                        csv.file.forEach(function(line) {

                                            report_file += '' + line.id + ',' + line.titulo + ',' + line.isWhiteList + ',' + line.dataExpiracao + ',' + line.importSystem_id + ',' + line.lockImp + ',' + line.blMessage + ',' + line.fromBL + ',' + line.toBL + ',' + line.upid + ',' + line.ativo + ',' + line.apagado + ',' + line.codEvent + ',' + line.propriedades + ',' + line.valores + ',' + line.dataHoraCriacao + ',' + line.utilizadorCriacao + ',' + line.dataHoraAlteracao + ',' + line.utilizadorAlteracao + ',' + line.utid1 + ',' + line.socio + ',' + line.nsocio + ',' + line.categoria + ',' + line.porta + '\n';

                                        });

                                        var hiddenElement = document.createElement('a');
                                        var blob = new Blob([report_file],{type: 'text/csv;charset=utf-8;'});
                                        hiddenElement.href = URL.createObjectURL(blob);
                                        hiddenElement.target = '_blank';
                                        hiddenElement.download = csv.filename;
                                        hiddenElement.click();

                                        $('#csv-download-table > tbody > #report-whitelist-masters > .report-event-park > *').remove();
                                        $('#csv-download-table > tbody > #report-whitelist-masters > .report-event-park').append('<i class="material-icons">save_alt</i>');

                                    });

                                }

                                if (whitelist_masters.includes('event-credential')) {

                                    data['event_credential'].forEach(function (csv) {

                                        $('#csv-download-table > tbody > #report-whitelist-masters > .report-event-credential > *').remove();
                                        $('#csv-download-table > tbody > #report-whitelist-masters > .report-event-credential').append('<i class="material-icons">file_copy</i>');


                                        var report_file = csv.header + '\n';

                                        csv.file.forEach(function (line) {

                                            report_file += '' + line.id + ',' + line.titulo + ',' + line.isWhiteList + ',' + line.dataExpiracao + ',' + line.importSystem_id + ',' + line.lockImp + ',' + line.blMessage + ',' + line.fromBL + ',' + line.toBL + ',' + line.upid + ',' + line.ativo + ',' + line.apagado + ',' + line.codEvent + ',' + line.propriedades + ',' + line.valores + ',' + line.dataHoraCriacao + ',' + line.utilizadorCriacao + ',' + line.dataHoraAlteracao + ',' + line.utilizadorAlteracao + ',' + line.utid1 + ',' + line.socio + ',' + line.nsocio + ',' + line.categoria + ',' + line.porta + '\n';

                                        });

                                        var hiddenElement = document.createElement('a');
                                        var blob = new Blob([report_file], {type: 'text/csv;charset=utf-8;'});
                                        hiddenElement.href = URL.createObjectURL(blob);
                                        hiddenElement.target = '_blank';
                                        hiddenElement.download = csv.filename;
                                        hiddenElement.click();

                                        $('#csv-download-table > tbody > #report-whitelist-masters > .report-event-credential > *').remove();
                                        $('#csv-download-table > tbody > #report-whitelist-masters > .report-event-credential').append('<i class="material-icons">save_alt</i>');

                                    });

                                }

                                return true;

                            } catch (error) {

                                console.log(error.message);

                                return false;

                            }

                        },
                        error: function(error) {

                            console.log('Error Whitelist Master');
                            console.log(error);

                            return false;

                        }

                    }),

                    $.ajax({
                        type: 'POST',
                        url: '/reports/csvs/generate/blacklist',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        dataType: 'json',
                        data: {
                            event_id: event_id,
                            event_park_id: event_park_id,
                            event_credential_id: event_credential_id,
                            generate: blacklist
                        },
                        beforeSend: function() {

                            $('#csv-download-table > tbody > #report-blacklist > .report-event > *').remove();
                            $('#csv-download-table > tbody > #report-blacklist > .report-event-park > *').remove();
                            $('#csv-download-table > tbody > #report-blacklist > .report-event-credential > *').remove();

                            if (blacklist.includes('event')) {

                                $('#csv-download-table > tbody > #report-blacklist > .report-event').append('<i class="material-icons">show_chart</i>');

                            } else {

                                $('#csv-download-table > tbody > #report-blacklist > .report-event').append('<i class="material-icons">remove</i>');

                            }

                            if (blacklist.includes('event-park')) {

                                $('#csv-download-table > tbody > #report-blacklist > .report-event-park').append('<i class="material-icons">show_chart</i>');

                            } else {

                                $('#csv-download-table > tbody > #report-blacklist > .report-event-park').append('<i class="material-icons">remove</i>');

                            }

                            if (blacklist.includes('event-credential')) {

                                $('#csv-download-table > tbody > #report-blacklist > .report-event-credential').append('<i class="material-icons">show_chart</i>');

                            } else {

                                $('#csv-download-table > tbody > #report-blacklist > .report-event-credential').append('<i class="material-icons">remove</i>');

                            }

                        },
                        success: function (data) {

                            try {

                                if (blacklist.includes('event')) {

                                    $('#csv-download-table > tbody > #report-blacklist > .report-event > *').remove();
                                    $('#csv-download-table > tbody > #report-blacklist > .report-event').append('<i class="material-icons">file_copy</i>');

                                    data['event'].forEach(function (csv) {

                                        var report_file = csv.header + '\n';

                                        csv.file.forEach(function(line) {

                                            report_file += '' + line.id + ',' + line.titulo + ',' + line.isWhiteList + ',' + line.dataExpiracao + ',' + line.importSystem_id + ',' + line.lockImp + ',' + line.blMessage + ',' + line.fromBL + ',' + line.toBL + ',' + line.upid + ',' + line.ativo + ',' + line.apagado + ',' + line.codEvent + ',' + line.propriedades + ',' + line.valores + ',' + line.dataHoraCriacao + ',' + line.utilizadorCriacao + ',' + line.dataHoraAlteracao + ',' + line.utilizadorAlteracao + ',' + line.utid1 + ',' + line.socio + ',' + line.nsocio + ',' + line.categoria + ',' + line.porta + '\n';

                                        });


                                        var hiddenElement = document.createElement('a');
                                        var blob = new Blob([report_file],{type: 'text/csv;charset=utf-8;'});
                                        hiddenElement.href = URL.createObjectURL(blob);
                                        hiddenElement.target = '_blank';
                                        hiddenElement.download = csv.filename;
                                        hiddenElement.click();

                                        $('#csv-download-table > tbody > #report-blacklist > .report-event > *').remove();
                                        $('#csv-download-table > tbody > #report-blacklist > .report-event').append('<i class="material-icons">save_alt</i>');

                                    });

                                }

                                if (blacklist.includes('event-park')) {

                                    $('#csv-download-table > tbody > #report-blacklist > .report-event-park > *').remove();
                                    $('#csv-download-table > tbody > #report-blacklist > .report-event-park').append('<i class="material-icons">file_copy</i>');

                                    data['event_park'].forEach(function (csv) {

                                        var report_file = csv.header + '\n';

                                        csv.file.forEach(function (line) {

                                            report_file += '' + line.id + ',' + line.titulo + ',' + line.isWhiteList + ',' + line.dataExpiracao + ',' + line.importSystem_id + ',' + line.lockImp + ',' + line.blMessage + ',' + line.fromBL + ',' + line.toBL + ',' + line.upid + ',' + line.ativo + ',' + line.apagado + ',' + line.codEvent + ',' + line.propriedades + ',' + line.valores + ',' + line.dataHoraCriacao + ',' + line.utilizadorCriacao + ',' + line.dataHoraAlteracao + ',' + line.utilizadorAlteracao + ',' + line.utid1 + ',' + line.socio + ',' + line.nsocio + ',' + line.categoria + ',' + line.porta + '\n';

                                        });

                                        var hiddenElement = document.createElement('a');
                                        var blob = new Blob([report_file],{type: 'text/csv;charset=utf-8;'});
                                        hiddenElement.href = URL.createObjectURL(blob);
                                        hiddenElement.target = '_blank';
                                        hiddenElement.download = csv.filename;
                                        hiddenElement.click();

                                        $('#csv-download-table > tbody > #report-blacklist > .report-event-park > *').remove();
                                        $('#csv-download-table > tbody > #report-blacklist > .report-event-park').append('<i class="material-icons">save_alt</i>');

                                    });

                                }

                                if (blacklist.includes('event-park')) {

                                    $('#csv-download-table > tbody > #report-blacklist > .report-event-credential > *').remove();
                                    $('#csv-download-table > tbody > #report-blacklist > .report-event-credential').append('<i class="material-icons">file_copy</i>');

                                    data['event_credential'].forEach(function (csv) {

                                        var report_file = csv.header + '\n';

                                        csv.file.forEach(function (line) {

                                            report_file += '' + line.id + ',' + line.titulo + ',' + line.isWhiteList + ',' + line.dataExpiracao + ',' + line.importSystem_id + ',' + line.lockImp + ',' + line.blMessage + ',' + line.fromBL + ',' + line.toBL + ',' + line.upid + ',' + line.ativo + ',' + line.apagado + ',' + line.codEvent + ',' + line.propriedades + ',' + line.valores + ',' + line.dataHoraCriacao + ',' + line.utilizadorCriacao + ',' + line.dataHoraAlteracao + ',' + line.utilizadorAlteracao + ',' + line.utid1 + ',' + line.socio + ',' + line.nsocio + ',' + line.categoria + ',' + line.porta + '\n';

                                        });

                                        var hiddenElement = document.createElement('a');
                                        var blob = new Blob([report_file], {type: 'text/csv;charset=utf-8;'});
                                        hiddenElement.href = URL.createObjectURL(blob);
                                        hiddenElement.target = '_blank';
                                        hiddenElement.download = csv.filename;
                                        hiddenElement.click();

                                        $('#csv-download-table > tbody > #report-blacklist > .report-event-credential > *').remove();
                                        $('#csv-download-table > tbody > #report-blacklist > .report-event-credential').append('<i class="material-icons">save_alt</i>');

                                    });

                                }

                                return true;

                            } catch (error) {

                                console.log(error.message);

                                return false;

                            }

                        },
                        error: function(error) {

                            console.log('Error Blacklist');
                            console.log(error);

                            return false;

                        }

                    }),

                    $.ajax({
                        type: 'POST',
                        url: '/reports/csvs/generate/entries',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        dataType: 'json',
                        data: {
                            event_id: event_id,
                            event_park_id: event_park_id,
                            event_credential_id: event_credential_id,
                            generate: entries
                        },
                        beforeSend: function() {

                            $('#csv-download-table > tbody > #report-entries > .report-event > *').remove();
                            $('#csv-download-table > tbody > #report-entries > .report-event-park > *').remove();
                            $('#csv-download-table > tbody > #report-entries > .report-event-credential > *').remove();

                            if (entries.includes('event')) {

                                $('#csv-download-table > tbody > #report-entries > .report-event').append('<i class="material-icons">show_chart</i>');

                            } else {

                                $('#csv-download-table > tbody > #report-entries > .report-event').append('<i class="material-icons">remove</i>');

                            }

                            if (entries.includes('event-park')) {

                                $('#csv-download-table > tbody > #report-entries > .report-event-park').append('<i class="material-icons">show_chart</i>');

                            } else {

                                $('#csv-download-table > tbody > #report-entries > .report-event-park').append('<i class="material-icons">remove</i>');

                            }

                            if (entries.includes('event-credential')) {

                                $('#csv-download-table > tbody > #report-entries > .report-event-credential').append('<i class="material-icons">show_chart</i>');

                            } else {

                                $('#csv-download-table > tbody > #report-entries > .report-event-credential').append('<i class="material-icons">remove</i>');

                            }

                        },
                        success: function (data) {

                            try {

                                if (entries.includes('event')) {

                                    $('#csv-download-table > tbody > #report-entries > .report-event > *').remove();
                                    $('#csv-download-table > tbody > #report-entries > .report-event').append('<i class="material-icons">file_copy</i>');

                                    data['event'].forEach(function (csv) {

                                        var report_file = csv.header + '\n';

                                        csv.file.forEach(function(line) {

                                            report_file += '' + line.jogo + ',' + line.datahora + ',' + line.porta + ',' + line.camarote + ',' + line.dac + ',' + line.ip + ',' + line.codigobarras + ',' + line.socio + ',' + line.nsocio + ',' + line.tipotitulo + ',' + line.categoria + ',' + line.tipoleitor + '\n';

                                        });

                                        var hiddenElement = document.createElement('a');
                                        var blob = new Blob([report_file],{type: 'text/csv;charset=utf-8;'});
                                        hiddenElement.href = URL.createObjectURL(blob);
                                        hiddenElement.target = '_blank';
                                        hiddenElement.download = csv.filename;
                                        hiddenElement.click();

                                        $('#csv-download-table > tbody > #report-entries > .report-event > *').remove();
                                        $('#csv-download-table > tbody > #report-entries > .report-event').append('<i class="material-icons">save_alt</i>');

                                    });

                                }

                                if (entries.includes('event-park')) {

                                    $('#csv-download-table > tbody > #report-entries > .report-event-park > *').remove();
                                    $('#csv-download-table > tbody > #report-entries > .report-event-park').append('<i class="material-icons">file_copy</i>');

                                    data['event_park'].forEach(function (csv) {

                                        var report_file = csv.header + '\n';

                                        csv.file.forEach(function(line) {

                                            report_file += '' + line.jogo + ',' + line.datahora + ',' + line.porta + ',' + line.dac + ',' + line.ip + ',' + line.codigobarras + ',' + line.socio + ',' + line.nsocio + ',' + line.tipotitulo + ',' + line.categoria + ',' + line.tipoleitor + '\n';

                                        });

                                        var hiddenElement = document.createElement('a');
                                        var blob = new Blob([report_file],{type: 'text/csv;charset=utf-8;'});
                                        hiddenElement.href = URL.createObjectURL(blob);
                                        hiddenElement.target = '_blank';
                                        hiddenElement.download = csv.filename;
                                        hiddenElement.click();

                                        $('#csv-download-table > tbody > #report-entries > .report-event-park > *').remove();
                                        $('#csv-download-table > tbody > #report-entries > .report-event-park').append('<i class="material-icons">save_alt</i>');

                                    });

                                }

                                if (entries.includes('event-credential')) {

                                    $('#csv-download-table > tbody > #report-entries > .report-event-credential > *').remove();
                                    $('#csv-download-table > tbody > #report-entries > .report-event-credential').append('<i class="material-icons">file_copy</i>');

                                    data['event_credential'].forEach(function (csv) {

                                        var report_file = csv.header + '\n';

                                        csv.file.forEach(function (line) {

                                            report_file += '' + line.evento + ',' + line.datahora + ',' + line.codigobarras + ',' + line.tipotitulo + ',' + line.entidade + ',' + line.credenciado + ',' + line.permite_reentrada + ',' + line.porta + ',' + line.dac + ',' + line.ip + '\n';

                                        });

                                        var hiddenElement = document.createElement('a');
                                        var blob = new Blob([report_file], {type: 'text/csv;charset=utf-8;'});
                                        hiddenElement.href = URL.createObjectURL(blob);
                                        hiddenElement.target = '_blank';
                                        hiddenElement.download = csv.filename;
                                        hiddenElement.click();

                                        $('#csv-download-table > tbody > #report-entries > .report-event-credential > *').remove();
                                        $('#csv-download-table > tbody > #report-entries > .report-event-credential').append('<i class="material-icons">save_alt</i>');

                                    });

                                }

                                return true;

                            } catch (error) {

                                console.log(error.message);

                                return false;

                            }

                        },
                        error: function(error) {

                            console.log('Error Entries');
                            console.log(error);

                            return false;

                        }

                    }),

                    $.ajax({
                        type: 'POST',
                        url: '/reports/csvs/generate/entries_app',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        dataType: 'json',
                        data: {
                            event_id: event_id,
                            event_park_id: event_park_id,
                            event_credential_id: event_credential_id,
                            generate: entries_app
                        },
                        beforeSend: function() {

                            $('#csv-download-table > tbody > #report-entries-app > .report-event > *').remove();
                            $('#csv-download-table > tbody > #report-entries-app > .report-event-park > *').remove();
                            $('#csv-download-table > tbody > #report-entries-app > .report-event-credential > *').remove();

                            if (entries_app.includes('event')) {

                                $('#csv-download-table > tbody > #report-entries-app > .report-event').append('<i class="material-icons">show_chart</i>');

                            } else {

                                $('#csv-download-table > tbody > #report-entries-app > .report-event').append('<i class="material-icons">remove</i>');

                            }

                            if (entries_app.includes('event-park')) {

                                $('#csv-download-table > tbody > #report-entries-app > .report-event-park').append('<i class="material-icons">show_chart</i>');

                            } else {

                                $('#csv-download-table > tbody > #report-entries-app > .report-event-park').append('<i class="material-icons">remove</i>');

                            }

                            if (entries_app.includes('event-credential')) {

                                $('#csv-download-table > tbody > #report-entries-app > .report-event-credential').append('<i class="material-icons">show_chart</i>');

                            } else {

                                $('#csv-download-table > tbody > #report-entries-app > .report-event-credential').append('<i class="material-icons">remove</i>');

                            }

                        },
                        success: function (data) {

                            try {

                                if (entries_app.includes('event')) {

                                    $('#csv-download-table > tbody > #report-entries-app > .report-event > *').remove();
                                    $('#csv-download-table > tbody > #report-entries-app > .report-event').append('<i class="material-icons">file_copy</i>');

                                    data['event'].forEach(function (csv) {

                                        var report_file = csv.header + '\n';

                                        csv.file.forEach(function (line) {

                                            report_file += '' + line.jogo + ',' + line.datahora + ',' + line.porta + ',' + line.dac + ',' + line.ip + ',' + line.codigobarras + ',' + line.socio + ',' + line.nsocio + ',' + line.tipotitulo + ',' + line.categoria + ',' + line.tipoleitor + '\n';

                                        });


                                        var hiddenElement = document.createElement('a');
                                        var blob = new Blob([report_file], {type: 'text/csv;charset=utf-8;'});
                                        hiddenElement.href = URL.createObjectURL(blob);
                                        hiddenElement.target = '_blank';
                                        hiddenElement.download = csv.filename;
                                        hiddenElement.click();

                                        $('#csv-download-table > tbody > #report-entries-app > .report-event > *').remove();
                                        $('#csv-download-table > tbody > #report-entries-app > .report-event').append('<i class="material-icons">save_alt</i>');

                                    });

                                }

                                if (entries_app.includes('event-park')) {

                                    $('#csv-download-table > tbody > #report-entries-app > .report-event-park > *').remove();
                                    $('#csv-download-table > tbody > #report-entries-app > .report-event-park').append('<i class="material-icons">file_copy</i>');

                                    data['event_park'].forEach(function (csv) {

                                        var report_file = csv.header + '\n';

                                        csv.file.forEach(function (line) {

                                            report_file += '' + line.jogo + ',' + line.datahora + ',' + line.porta + ',' + line.dac + ',' + line.ip + ',' + line.codigobarras + ',' + line.socio + ',' + line.nsocio + ',' + line.tipotitulo + ',' + line.categoria + ',' + line.tipoleitor + '\n';

                                        });

                                        var hiddenElement = document.createElement('a');
                                        var blob = new Blob([report_file], {type: 'text/csv;charset=utf-8;'});
                                        hiddenElement.href = URL.createObjectURL(blob);
                                        hiddenElement.target = '_blank';
                                        hiddenElement.download = csv.filename;
                                        hiddenElement.click();

                                        $('#csv-download-table > tbody > #report-entries-app > .report-event-park > *').remove();
                                        $('#csv-download-table > tbody > #report-entries-app > .report-event-park').append('<i class="material-icons">save_alt</i>');

                                    });

                                }

                                if (entries_app.includes('event-credential')) {

                                    $('#csv-download-table > tbody > #report-entries-app > .report-event-credential > *').remove();
                                    $('#csv-download-table > tbody > #report-entries-app > .report-event-credential').append('<i class="material-icons">file_copy</i>');

                                    data['event_credential'].forEach(function (csv) {

                                        var report_file = csv.header + '\n';

                                        csv.file.forEach(function (line) {

                                            report_file += '' + line.jogo + ',' + line.datahora + ',' + line.porta + ',' + line.dac + ',' + line.ip + ',' + line.codigobarras + ',' + line.socio + ',' + line.nsocio + ',' + line.tipotitulo + ',' + line.categoria + ',' + line.tipoleitor + '\n';

                                        });

                                        var hiddenElement = document.createElement('a');
                                        var blob = new Blob([report_file], {type: 'text/csv;charset=utf-8;'});
                                        hiddenElement.href = URL.createObjectURL(blob);
                                        hiddenElement.target = '_blank';
                                        hiddenElement.download = csv.filename;
                                        hiddenElement.click();

                                        $('#csv-download-table > tbody > #report-entries-app > .report-event-credential > *').remove();
                                        $('#csv-download-table > tbody > #report-entries-app > .report-event-credential').append('<i class="material-icons">save_alt</i>');

                                    });

                                }

                                return true;

                            } catch (error) {

                                console.log(error.message);

                                return false;

                            }

                        },
                        error: function(error) {

                            console.log('Error Entries APP');
                            console.log(error);

                            return false;

                        }

                    }),

                    $.ajax({
                        type: 'POST',
                        url: '/reports/csvs/generate/entries_presidential_box',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        dataType: 'json',
                        data: {
                            event_id: event_id,
                            event_park_id: event_park_id,
                            event_credential_id: event_credential_id,
                            generate: entries_presidential_box
                        },
                        beforeSend: function() {

                            $('#csv-download-table > tbody > #report-entries-presidential-box > .report-event > *').remove();
                            $('#csv-download-table > tbody > #report-entries-presidential-box > .report-event-park > *').remove();
                            $('#csv-download-table > tbody > #report-entries-presidential-box > .report-event-credential > *').remove();

                            if (entries_presidential_box.includes('event')) {

                                $('#csv-download-table > tbody > #report-entries-presidential-box > .report-event').append('<i class="material-icons">show_chart</i>');

                            } else {

                                $('#csv-download-table > tbody > #report-entries-presidential-box > .report-event').append('<i class="material-icons">remove</i>');

                            }

                            if (entries_presidential_box.includes('event-park')) {

                                $('#csv-download-table > tbody > #report-entries-presidential-box > .report-event-park').append('<i class="material-icons">show_chart</i>');

                            } else {

                                $('#csv-download-table > tbody > #report-entries-presidential-box > .report-event-park').append('<i class="material-icons">remove</i>');

                            }

                            if (entries_presidential_box.includes('event-credential')) {

                                $('#csv-download-table > tbody > #report-entries-presidential-box > .report-event-credential').append('<i class="material-icons">show_chart</i>');

                            } else {

                                $('#csv-download-table > tbody > #report-entries-presidential-box > .report-event-credential').append('<i class="material-icons">remove</i>');

                            }

                        },
                        success: function (data) {

                            try {

                                if (entries_presidential_box.includes('event')) {

                                    $('#csv-download-table > tbody > #report-entries-presidential-box > .report-event > *').remove();
                                    $('#csv-download-table > tbody > #report-entries-presidential-box > .report-event').append('<i class="material-icons">file_copy</i>');


                                    data['event'].forEach(function (csv) {

                                        var report_file = csv.header + '\n';

                                        csv.file.forEach(function (line) {

                                            report_file += '' + line.jogo + ',' + line.datahora + ',' + line.porta + ',' + line.dac + ',' + line.ip + ',' + line.codigobarras + ',' + line.socio + ',' + line.nsocio + ',' + line.tipotitulo + ',' + line.categoria + ',' + line.tipoleitor + '\n';

                                        });


                                        var hiddenElement = document.createElement('a');
                                        var blob = new Blob([report_file], {type: 'text/csv;charset=utf-8;'});
                                        hiddenElement.href = URL.createObjectURL(blob);
                                        hiddenElement.target = '_blank';
                                        hiddenElement.download = csv.filename;
                                        hiddenElement.click();

                                        $('#csv-download-table > tbody > #report-entries-presidential-box > .report-event > *').remove();
                                        $('#csv-download-table > tbody > #report-entries-presidential-box > .report-event').append('<i class="material-icons">save_alt</i>');

                                    });

                                }

                                if (entries_presidential_box.includes('event-park')) {

                                    $('#csv-download-table > tbody > #report-entries-presidential-box > .report-event-park > *').remove();
                                    $('#csv-download-table > tbody > #report-entries-presidential-box > .report-event-park').append('<i class="material-icons">file_copy</i>');


                                    data['event_park'].forEach(function (csv) {

                                        var report_file = csv.header + '\n';

                                        csv.file.forEach(function (line) {

                                            report_file += '' + line.jogo + ',' + line.datahora + ',' + line.porta + ',' + line.dac + ',' + line.ip + ',' + line.codigobarras + ',' + line.socio + ',' + line.nsocio + ',' + line.tipotitulo + ',' + line.categoria + ',' + line.tipoleitor + '\n';

                                        });

                                        var hiddenElement = document.createElement('a');
                                        var blob = new Blob([report_file], {type: 'text/csv;charset=utf-8;'});
                                        hiddenElement.href = URL.createObjectURL(blob);
                                        hiddenElement.target = '_blank';
                                        hiddenElement.download = csv.filename;
                                        hiddenElement.click();

                                        $('#csv-download-table > tbody > #report-entries-presidential-box > .report-event-park > *').remove();
                                        $('#csv-download-table > tbody > #report-entries-presidential-box > .report-event-park').append('<i class="material-icons">save_alt</i>');

                                    });

                                }

                                if (entries_presidential_box.includes('event-credential')) {

                                    $('#csv-download-table > tbody > #report-entries-presidential-box > .report-event-credential > *').remove();
                                    $('#csv-download-table > tbody > #report-entries-presidential-box > .report-event-credential').append('<i class="material-icons">file_copy</i>');


                                    data['event_credential'].forEach(function (csv) {

                                        var report_file = csv.header + '\n';

                                        csv.file.forEach(function (line) {

                                            report_file += '' + line.jogo + ',' + line.datahora + ',' + line.porta + ',' + line.dac + ',' + line.ip + ',' + line.codigobarras + ',' + line.socio + ',' + line.nsocio + ',' + line.tipotitulo + ',' + line.categoria + ',' + line.tipoleitor + '\n';

                                        });

                                        var hiddenElement = document.createElement('a');
                                        var blob = new Blob([report_file], {type: 'text/csv;charset=utf-8;'});
                                        hiddenElement.href = URL.createObjectURL(blob);
                                        hiddenElement.target = '_blank';
                                        hiddenElement.download = csv.filename;
                                        hiddenElement.click();

                                        $('#csv-download-table > tbody > #report-entries-presidential-box > .report-event-credential > *').remove();
                                        $('#csv-download-table > tbody > #report-entries-presidential-box > .report-event-credential').append('<i class="material-icons">save_alt</i>');

                                    });

                                }

                                return true;

                            } catch (error) {

                                console.log(error.message);

                                return false;

                            }

                        },
                        error: function(error) {

                            console.log('Error Entries Presidential Box');
                            console.log(error);

                            return false;

                        }

                    }),

                    $.ajax({
                        type: 'POST',
                        url: '/reports/csvs/generate/entries_boxes',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        dataType: 'json',
                        data: {
                            event_id: event_id,
                            event_park_id: event_park_id,
                            event_credential_id: event_credential_id,
                            generate: entries_boxes
                        },
                        beforeSend: function() {

                            $('#csv-download-table > tbody > #report-entries-box > .report-event > *').remove();
                            $('#csv-download-table > tbody > #report-entries-box > .report-event-park > *').remove();
                            $('#csv-download-table > tbody > #report-entries-box > .report-event-credential > *').remove();

                            if (entries_boxes.includes('event')) {

                                $('#csv-download-table > tbody > #report-entries-box > .report-event').append('<i class="material-icons">show_chart</i>');

                            } else {

                                $('#csv-download-table > tbody > #report-entries-box > .report-event').append('<i class="material-icons">remove</i>');

                            }

                            if (entries_boxes.includes('event-park')) {

                                $('#csv-download-table > tbody > #report-entries-box > .report-event-park').append('<i class="material-icons">show_chart</i>');

                            } else {

                                $('#csv-download-table > tbody > #report-entries-box > .report-event-park').append('<i class="material-icons">remove</i>');

                            }

                            if (entries_boxes.includes('event-credential')) {

                                $('#csv-download-table > tbody > #report-entries-box > .report-event-credential').append('<i class="material-icons">show_chart</i>');

                            } else {

                                $('#csv-download-table > tbody > #report-entries-box > .report-event-credential').append('<i class="material-icons">remove</i>');

                            }

                        },
                        success: function (data) {

                            try {

                                if (entries_boxes.includes('event')) {

                                    $('#csv-download-table > tbody > #report-entries-box > .report-event > *').remove();
                                    $('#csv-download-table > tbody > #report-entries-box > .report-event').append('<i class="material-icons">file_copy</i>');

                                    data['event'].forEach(function (csv) {

                                        var report_file = csv.header + '\n';

                                        csv.file.forEach(function (line) {

                                            report_file += '' + line.entidade + ',' + line.jogo + ',' + line.datahora + ',' + line.porta + ',' + line.dac + ',' + line.ip + ',' + line.titulo + ',' + line.socio + ',' + line.nsocio + ',' + line.tipotitulo + ',' + line.categoria + ',' + line.tipoleitor + ',' + line.camarote + '\n';

                                        });


                                        var hiddenElement = document.createElement('a');
                                        var blob = new Blob([report_file], {type: 'text/csv;charset=utf-8;'});
                                        hiddenElement.href = URL.createObjectURL(blob);
                                        hiddenElement.target = '_blank';
                                        hiddenElement.download = csv.filename;
                                        hiddenElement.click();

                                        $('#csv-download-table > tbody > #report-entries-box > .report-event > *').remove();
                                        $('#csv-download-table > tbody > #report-entries-box > .report-event').append('<i class="material-icons">save_alt</i>');

                                    });

                                }

                                if (entries_boxes.includes('event-park')) {

                                    $('#csv-download-table > tbody > #report-entries-box > .report-event-park > *').remove();
                                    $('#csv-download-table > tbody > #report-entries-box > .report-event-park').append('<i class="material-icons">file_copy</i>');

                                    data['event_park'].forEach(function (csv) {

                                        var report_file = csv.header + '\n';

                                        csv.file.forEach(function (line) {

                                            report_file += '' + line.entidade + ',' + line.jogo + ',' + line.datahora + ',' + line.porta + ',' + line.dac + ',' + line.ip + ',' + line.titulo + ',' + line.socio + ',' + line.nsocio + ',' + line.tipotitulo + ',' + line.categoria + ',' + line.tipoleitor + ',' + line.camarote + '\n';

                                        });

                                        var hiddenElement = document.createElement('a');
                                        var blob = new Blob([report_file], {type: 'text/csv;charset=utf-8;'});
                                        hiddenElement.href = URL.createObjectURL(blob);
                                        hiddenElement.target = '_blank';
                                        hiddenElement.download = csv.filename;
                                        hiddenElement.click();

                                        $('#csv-download-table > tbody > #report-entries-box > .report-event-park > *').remove();
                                        $('#csv-download-table > tbody > #report-entries-box > .report-event-park').append('<i class="material-icons">save_alt</i>');

                                    });

                                }

                                if (entries_boxes.includes('event-credential')) {

                                    $('#csv-download-table > tbody > #report-entries-box > .report-event-credential > *').remove();
                                    $('#csv-download-table > tbody > #report-entries-box > .report-event-credential').append('<i class="material-icons">file_copy</i>');

                                    data['event_credential'].forEach(function (csv) {

                                        var report_file = csv.header + '\n';

                                        csv.file.forEach(function (line) {

                                            report_file += '' + line.entidade + ',' + line.jogo + ',' + line.datahora + ',' + line.porta + ',' + line.dac + ',' + line.ip + ',' + line.titulo + ',' + line.socio + ',' + line.nsocio + ',' + line.tipotitulo + ',' + line.categoria + ',' + line.tipoleitor + ',' + line.camarote + '\n';

                                        });

                                        var hiddenElement = document.createElement('a');
                                        var blob = new Blob([report_file], {type: 'text/csv;charset=utf-8;'});
                                        hiddenElement.href = URL.createObjectURL(blob);
                                        hiddenElement.target = '_blank';
                                        hiddenElement.download = csv.filename;
                                        hiddenElement.click();

                                        $('#csv-download-table > tbody > #report-entries-box > .report-event-credential > *').remove();
                                        $('#csv-download-table > tbody > #report-entries-box > .report-event-credential').append('<i class="material-icons">save_alt</i>');

                                    });

                                }

                                return true;

                            } catch (error) {

                                console.log(error.message);

                                return false;

                            }

                        },
                        error: function(error) {

                            console.log('Error Entries Boxes');
                            console.log(error);

                            return false;

                        }

                    }),

                    $.ajax({

                        type: 'POST',
                        url: '/reports/csvs/generate/entries_gate_category',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        dataType: 'json',
                        data: {
                            event_id: event_id,
                            event_park_id: event_park_id,
                            event_credential_id: event_credential_id,
                            generate: entries_gate_category
                        },
                        beforeSend: function() {

                            $('#csv-download-table > tbody > #report-entries-gate-category > .report-event > *').remove();
                            $('#csv-download-table > tbody > #report-entries-gate-category > .report-event-park > *').remove();
                            $('#csv-download-table > tbody > #report-entries-gate-category > .report-event-credential > *').remove();

                            if (entries_gate_category.includes('event')) {

                                $('#csv-download-table > tbody > #report-entries-gate-category > .report-event').append('<i class="material-icons">show_chart</i>');

                            } else {

                                $('#csv-download-table > tbody > #report-entries-gate-category > .report-event').append('<i class="material-icons">remove</i>');

                            }

                            if (entries_gate_category.includes('event-park')) {

                                $('#csv-download-table > tbody > #report-entries-gate-category > .report-event-park').append('<i class="material-icons">show_chart</i>');

                            } else {

                                $('#csv-download-table > tbody > #report-entries-gate-category > .report-event-park').append('<i class="material-icons">remove</i>');

                            }

                            if (entries_gate_category.includes('event-credential')) {

                                $('#csv-download-table > tbody > #report-entries-gate-category > .report-event-credential').append('<i class="material-icons">show_chart</i>');

                            } else {

                                $('#csv-download-table > tbody > #report-entries-gate-category > .report-event-credential').append('<i class="material-icons">remove</i>');

                            }

                        },
                        success: function (data) {

                            try {

                                if (entries_gate_category.includes('event')) {

                                    $('#csv-download-table > tbody > #report-entries-gate-category > .report-event > *').remove();
                                    $('#csv-download-table > tbody > #report-entries-gate-category > .report-event').append('<i class="material-icons">file_copy</i>');

                                    data['event'].forEach(function (csv) {

                                        var report_file = csv.header + '\n';

                                        csv.file.forEach(function (line) {

                                            report_file += '' + line['Porta'] + ',' + line['53'] + ',' + line['54'] + ',' + line['57'] + ',' + line['59'] + ',' + line['99'] + ',' + line['Total'] + '\n';

                                        });


                                        var hiddenElement = document.createElement('a');
                                        var blob = new Blob([report_file], {type: 'text/csv;charset=utf-8;'});
                                        hiddenElement.href = URL.createObjectURL(blob);
                                        hiddenElement.target = '_blank';
                                        hiddenElement.download = csv.filename;
                                        hiddenElement.click();

                                        $('#csv-download-table > tbody > #report-entries-gate-category > .report-event > *').remove();
                                        $('#csv-download-table > tbody > #report-entries-gate-category > .report-event').append('<i class="material-icons">save_alt</i>');

                                    });

                                }

                                if (entries_gate_category.includes('event-park')) {

                                    $('#csv-download-table > tbody > #report-entries-gate-category > .report-event-park > *').remove();
                                    $('#csv-download-table > tbody > #report-entries-gate-category > .report-event-park').append('<i class="material-icons">file_copy</i>');

                                    data['event_park'].forEach(function (csv) {

                                        var report_file = csv.header + '\n';

                                        csv.file.forEach(function (line) {

                                            report_file += '' + line['Porta'] + ',' + line['53'] + ',' + line['54'] + ',' + line['57'] + ',' + line['59'] + ',' + line['99'] + ',' + line['Total'] + '\n';

                                        });

                                        var hiddenElement = document.createElement('a');
                                        var blob = new Blob([report_file], {type: 'text/csv;charset=utf-8;'});
                                        hiddenElement.href = URL.createObjectURL(blob);
                                        hiddenElement.target = '_blank';
                                        hiddenElement.download = csv.filename;
                                        hiddenElement.click();

                                        $('#csv-download-table > tbody > #report-entries-gate-category > .report-event-park > *').remove();
                                        $('#csv-download-table > tbody > #report-entries-gate-category > .report-event-park').append('<i class="material-icons">save_alt</i>');

                                    });

                                }

                                if (entries_gate_category.includes('event-credential')) {

                                    $('#csv-download-table > tbody > #report-entries-gate-category > .report-event-credential > *').remove();
                                    $('#csv-download-table > tbody > #report-entries-gate-category > .report-event-credential').append('<i class="material-icons">file_copy</i>');

                                    data['event_credential'].forEach(function (csv) {

                                        var report_file = csv.header + '\n';

                                        csv.file.forEach(function (line) {

                                            report_file += '' + line['Porta'] + ',' + line['53'] + ',' + line['54'] + ',' + line['57'] + ',' + line['59'] + ',' + line['99'] + ',' + line['Total'] + '\n';

                                        });

                                        var hiddenElement = document.createElement('a');
                                        var blob = new Blob([report_file], {type: 'text/csv;charset=utf-8;'});
                                        hiddenElement.href = URL.createObjectURL(blob);
                                        hiddenElement.target = '_blank';
                                        hiddenElement.download = csv.filename;
                                        hiddenElement.click();

                                        $('#csv-download-table > tbody > #report-entries-gate-category > .report-event-credential > *').remove();
                                        $('#csv-download-table > tbody > #report-entries-gate-category > .report-event-credential').append('<i class="material-icons">save_alt</i>');

                                    });

                                }

                                return true;

                            } catch (error) {

                                console.log(error.message);

                                return false;

                            }

                        },
                        error: function(error) {

                            console.log('Error Entries Gate Category');
                            console.log(error);

                            return false;

                        }

                    }),

                    $.ajax({
                        type: 'POST',
                        url: '/reports/csvs/generate/errors_unique_title',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        dataType: 'json',
                        data: {
                            event_id: event_id,
                            event_park_id: event_park_id,
                            event_credential_id: event_credential_id,
                            generate: errors_unique_title
                        },
                        beforeSend: function() {

                            $('#csv-download-table > tbody > #report-unique-title-errors > .report-event > *').remove();
                            $('#csv-download-table > tbody > #report-unique-title-errors > .report-event-park > *').remove();
                            $('#csv-download-table > tbody > #report-unique-title-errors > .report-event-credential > *').remove();

                            if (errors_unique_title.includes('event')) {

                                $('#csv-download-table > tbody > #report-unique-title-errors > .report-event').append('<i class="material-icons">show_chart</i>');

                            } else {

                                $('#csv-download-table > tbody > #report-unique-title-errors > .report-event').append('<i class="material-icons">remove</i>');

                            }

                            if (errors_unique_title.includes('event-park')) {

                                $('#csv-download-table > tbody > #report-unique-title-errors > .report-event-park').append('<i class="material-icons">show_chart</i>');

                            } else {

                                $('#csv-download-table > tbody > #report-unique-title-errors > .report-event-park').append('<i class="material-icons">remove</i>');

                            }

                            if (errors_unique_title.includes('event-credential')) {

                                $('#csv-download-table > tbody > #report-unique-title-errors > .report-event-credential').append('<i class="material-icons">show_chart</i>');

                            } else {

                                $('#csv-download-table > tbody > #report-unique-title-errors > .report-event-credential').append('<i class="material-icons">remove</i>');

                            }

                        },
                        success: function (data) {

                            try {

                                if (errors_unique_title.includes('event')) {

                                    $('#csv-download-table > tbody > #report-unique-title-errors > .report-event > *').remove();
                                    $('#csv-download-table > tbody > #report-unique-title-errors > .report-event').append('<i class="material-icons">file_copy</i>');

                                    data['event'].forEach(function (csv) {

                                        var report_file = csv.header + '\n';

                                        csv.file.forEach(function (line) {

                                            report_file += '' + line['Socio_N'] + ',' + line['NSocio'] + ',' + line['Categoria'] + ',' + line['Titulo'] + ',' + line['Tipo'] + ',' + line['Erro'] + ',' + line['Mensagem_Blacklist'] + '\n';

                                        });


                                        var hiddenElement = document.createElement('a');
                                        var blob = new Blob([report_file], {type: 'text/csv;charset=utf-8;'});
                                        hiddenElement.href = URL.createObjectURL(blob);
                                        hiddenElement.target = '_blank';
                                        hiddenElement.download = csv.filename;
                                        hiddenElement.click();

                                        $('#csv-download-table > tbody > #report-unique-title-errors > .report-event > *').remove();
                                        $('#csv-download-table > tbody > #report-unique-title-errors > .report-event').append('<i class="material-icons">save_alt</i>');

                                    });

                                }

                                if (errors_unique_title.includes('event-park')) {

                                    $('#csv-download-table > tbody > #report-unique-title-errors > .report-event-park > *').remove();
                                    $('#csv-download-table > tbody > #report-unique-title-errors > .report-event-park').append('<i class="material-icons">file_copy</i>');

                                    data['event_park'].forEach(function (csv) {

                                        var report_file = csv.header + '\n';

                                        csv.file.forEach(function (line) {

                                            report_file += '' + line['Socio_N'] + ',' + line['NSocio'] + ',' + line['Categoria'] + ',' + line['Titulo'] + ',' + line['Tipo'] + ',' + line['Erro'] + ',' + line['Mensagem_Blacklist'] + '\n';

                                        });

                                        var hiddenElement = document.createElement('a');
                                        var blob = new Blob([report_file], {type: 'text/csv;charset=utf-8;'});
                                        hiddenElement.href = URL.createObjectURL(blob);
                                        hiddenElement.target = '_blank';
                                        hiddenElement.download = csv.filename;
                                        hiddenElement.click();

                                        $('#csv-download-table > tbody > #report-unique-title-errors > .report-event-park > *').remove();
                                        $('#csv-download-table > tbody > #report-unique-title-errors > .report-event-park').append('<i class="material-icons">save_alt</i>');

                                    });

                                }

                                if (errors_unique_title.includes('event-credential')) {

                                    $('#csv-download-table > tbody > #report-unique-title-errors > .report-event-credential > *').remove();
                                    $('#csv-download-table > tbody > #report-unique-title-errors > .report-event-credential').append('<i class="material-icons">file_copy</i>');

                                    data['event_credential'].forEach(function (csv) {

                                        var report_file = csv.header + '\n';

                                        csv.file.forEach(function (line) {

                                            report_file += '' + line['Socio_N'] + ',' + line['NSocio'] + ',' + line['Categoria'] + ',' + line['Titulo'] + ',' + line['Tipo'] + ',' + line['Erro'] + ',' + line['Mensagem_Blacklist'] + '\n';

                                        });

                                        var hiddenElement = document.createElement('a');
                                        var blob = new Blob([report_file], {type: 'text/csv;charset=utf-8;'});
                                        hiddenElement.href = URL.createObjectURL(blob);
                                        hiddenElement.target = '_blank';
                                        hiddenElement.download = csv.filename;
                                        hiddenElement.click();

                                        $('#csv-download-table > tbody > #report-unique-title-errors > .report-event-credential > *').remove();
                                        $('#csv-download-table > tbody > #report-unique-title-errors > .report-event-credential').append('<i class="material-icons">save_alt</i>');

                                    });

                                }

                                return true;

                            } catch (error) {

                                console.log(error.message);

                                return false;

                            }

                        },
                        error: function(error) {

                            console.log('Error Unique Title');
                            console.log(error);

                            return false;

                        }

                    }),

                    $.ajax({
                        type: 'POST',
                        url: '/reports/csvs/generate/errors_transactions',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        dataType: 'json',
                        data: {
                            event_id: event_id,
                            event_park_id: event_park_id,
                            event_credential_id: event_credential_id,
                            generate: errors_transactions
                        },
                        beforeSend: function() {

                            $('#csv-download-table > tbody > #report-transactions-errors > .report-event > *').remove();
                            $('#csv-download-table > tbody > #report-transactions-errors > .report-event-park > *').remove();
                            $('#csv-download-table > tbody > #report-transactions-errors > .report-event-credential > *').remove();

                            if (errors_transactions.includes('event')) {

                                $('#csv-download-table > tbody > #report-transactions-errors > .report-event').append('<i class="material-icons">show_chart</i>');

                            } else {

                                $('#csv-download-table > tbody > #report-transactions-errors > .report-event').append('<i class="material-icons">remove</i>');

                            }

                            if (errors_transactions.includes('event-park')) {

                                $('#csv-download-table > tbody > #report-transactions-errors > .report-event-park').append('<i class="material-icons">show_chart</i>');

                            } else {

                                $('#csv-download-table > tbody > #report-transactions-errors > .report-event-park').append('<i class="material-icons">remove</i>');

                            }

                            if (errors_transactions.includes('event-credential')) {

                                $('#csv-download-table > tbody > #report-transactions-errors > .report-event-credential').append('<i class="material-icons">show_chart</i>');

                            } else {

                                $('#csv-download-table > tbody > #report-transactions-errors > .report-event-credential').append('<i class="material-icons">remove</i>');

                            }

                        },
                        success: function (data) {

                            try {

                                if (errors_transactions.includes('event')) {

                                    $('#csv-download-table > tbody > #report-transactions-errors > .report-event > *').remove();
                                    $('#csv-download-table > tbody > #report-transactions-errors > .report-event').append('<i class="material-icons">file_copy</i>');

                                    data['event'].forEach(function (csv) {

                                        var report_file = csv.header + '\n';

                                        csv.file.forEach(function (line) {

                                            report_file += '' + line['Socio'] + ',' + line['NSocio'] + ',' + line['Categoria'] + ',' + line['Porta'] + ',' + line['Titulo'] + ',' + line['Tipo'] + ',' + line['Erro'] + ',' + line['Mensagem_Blacklist'] + ',' + line['Datetime'] + ',' + line['DAC'] + '\n';

                                        });


                                        var hiddenElement = document.createElement('a');
                                        var blob = new Blob([report_file], {type: 'text/csv;charset=utf-8;'});
                                        hiddenElement.href = URL.createObjectURL(blob);
                                        hiddenElement.target = '_blank';
                                        hiddenElement.download = csv.filename;
                                        hiddenElement.click();

                                        $('#csv-download-table > tbody > #report-transactions-errors > .report-event > *').remove();
                                        $('#csv-download-table > tbody > #report-transactions-errors > .report-event').append('<i class="material-icons">save_alt</i>');

                                    });

                                }

                                if (errors_transactions.includes('event-park')) {

                                    $('#csv-download-table > tbody > #report-transactions-errors > .report-event-park > *').remove();
                                    $('#csv-download-table > tbody > #report-transactions-errors > .report-event-park').append('<i class="material-icons">file_copy</i>');

                                    data['event_park'].forEach(function (csv) {

                                        var report_file = csv.header + '\n';

                                        csv.file.forEach(function (line) {

                                            report_file += '' + line['Socio'] + ',' + line['NSocio'] + ',' + line['Categoria'] + ',' + line['Porta'] + ',' + line['Titulo'] + ',' + line['Tipo'] + ',' + line['Erro'] + ',' + line['Mensagem_Blacklist'] + ',' + line['Datetime'] + ',' + line['DAC'] + '\n';

                                        });

                                        var hiddenElement = document.createElement('a');
                                        var blob = new Blob([report_file], {type: 'text/csv;charset=utf-8;'});
                                        hiddenElement.href = URL.createObjectURL(blob);
                                        hiddenElement.target = '_blank';
                                        hiddenElement.download = csv.filename;
                                        hiddenElement.click();

                                        $('#csv-download-table > tbody > #report-transactions-errors > .report-event-park > *').remove();
                                        $('#csv-download-table > tbody > #report-transactions-errors > .report-event-park').append('<i class="material-icons">save_alt</i>');

                                    });

                                }

                                if (errors_transactions.includes('event-credential')) {

                                    $('#csv-download-table > tbody > #report-transactions-errors > .report-event-credential > *').remove();
                                    $('#csv-download-table > tbody > #report-transactions-errors > .report-event-credential').append('<i class="material-icons">file_copy</i>');

                                    data['event_credential'].forEach(function (csv) {

                                        var report_file = csv.header + '\n';

                                        csv.file.forEach(function (line) {

                                            report_file += '' + line['Socio'] + ',' + line['NSocio'] + ',' + line['Categoria'] + ',' + line['Porta'] + ',' + line['Titulo'] + ',' + line['Tipo'] + ',' + line['Erro'] + ',' + line['Mensagem_Blacklist'] + ',' + line['Datetime'] + ',' + line['DAC'] + '\n';

                                        });

                                        var hiddenElement = document.createElement('a');
                                        var blob = new Blob([report_file], {type: 'text/csv;charset=utf-8;'});
                                        hiddenElement.href = URL.createObjectURL(blob);
                                        hiddenElement.target = '_blank';
                                        hiddenElement.download = csv.filename;
                                        hiddenElement.click();

                                        $('#csv-download-table > tbody > #report-transactions-errors > .report-event-credential > *').remove();
                                        $('#csv-download-table > tbody > #report-transactions-errors > .report-event-credential').append('<i class="material-icons">save_alt</i>');

                                    });

                                }

                                return true;

                            } catch (error) {

                                console.log(error.message);

                                return false;

                            }

                        },
                        error: function(error) {

                            console.log('Error Transactions');
                            console.log(error);

                            return false;

                        }

                    }),

                    $.ajax({
                        type: 'POST',
                        url: '/reports/csvs/generate/fill_level_minute',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        dataType: 'json',
                        data: {
                            event_id: event_id,
                            event_park_id: event_park_id,
                            event_credential_id: event_credential_id,
                            generate: fill_level_minute
                        },
                        beforeSend: function() {

                            $('#csv-download-table > tbody > #report-filllevel-minute > .report-event > *').remove();
                            $('#csv-download-table > tbody > #report-filllevel-minute > .report-event-park > *').remove();
                            $('#csv-download-table > tbody > #report-filllevel-minute > .report-event-credential > *').remove();

                            if (fill_level_minute.includes('event')) {

                                $('#csv-download-table > tbody > #report-filllevel-minute > .report-event').append('<i class="material-icons">show_chart</i>');

                            } else {

                                $('#csv-download-table > tbody > #report-filllevel-minute > .report-event').append('<i class="material-icons">remove</i>');

                            }

                            if (fill_level_minute.includes('event-park')) {

                                $('#csv-download-table > tbody > #report-filllevel-minute > .report-event-park').append('<i class="material-icons">show_chart</i>');

                            } else {

                                $('#csv-download-table > tbody > #report-filllevel-minute > .report-event-park').append('<i class="material-icons">remove</i>');

                            }

                            if (fill_level_minute.includes('event-credential')) {

                                $('#csv-download-table > tbody > #report-filllevel-minute > .report-event-credential').append('<i class="material-icons">show_chart</i>');

                            } else {

                                $('#csv-download-table > tbody > #report-filllevel-minute > .report-event-credential').append('<i class="material-icons">remove</i>');

                            }

                        },
                        success: function (data) {

                            try {


                                if (fill_level_minute.includes('event')) {

                                    $('#csv-download-table > tbody > #report-filllevel-minute > .report-event > *').remove();
                                    $('#csv-download-table > tbody > #report-filllevel-minute > .report-event').append('<i class="material-icons">file_copy</i>');

                                    data['event'].forEach(function (csv) {

                                        var report_file = csv.header + '\n';

                                        csv.file.forEach(function(line) {

                                            report_file += '' + line['hour_transaction'] + ',' + line['entries'] + '\n';

                                        });

                                        var hiddenElement = document.createElement('a');
                                        var blob = new Blob([report_file],{type: 'text/csv;charset=utf-8;'});
                                        hiddenElement.href = URL.createObjectURL(blob);
                                        hiddenElement.target = '_blank';
                                        hiddenElement.download = csv.filename;
                                        hiddenElement.click();

                                        $('#csv-download-table > tbody > #report-filllevel-minute > .report-event > *').remove();
                                        $('#csv-download-table > tbody > #report-filllevel-minute > .report-event').append('<i class="material-icons">save_alt</i>');

                                    });

                                }

                                if (fill_level_minute.includes('event-park')) {

                                    $('#csv-download-table > tbody > #report-filllevel-minute > .report-event-park > *').remove();
                                    $('#csv-download-table > tbody > #report-filllevel-minute > .report-event-park').append('<i class="material-icons">file_copy</i>');

                                    data['event_park'].forEach(function (csv) {

                                        var report_file = csv.header + '\n';

                                        csv.file.forEach(function (line) {

                                            report_file += '' + line['hour_transaction'] + ',' + line['entries'] + '\n';

                                        });

                                        var hiddenElement = document.createElement('a');
                                        var blob = new Blob([report_file], {type: 'text/csv;charset=utf-8;'});
                                        hiddenElement.href = URL.createObjectURL(blob);
                                        hiddenElement.target = '_blank';
                                        hiddenElement.download = csv.filename;
                                        hiddenElement.click();

                                        $('#csv-download-table > tbody > #report-filllevel-minute > .report-event-park > *').remove();
                                        $('#csv-download-table > tbody > #report-filllevel-minute > .report-event-park').append('<i class="material-icons">save_alt</i>');

                                    });

                                }

                                if (fill_level_minute.includes('event-credential')) {

                                    $('#csv-download-table > tbody > #report-filllevel-minute > .report-event-credential > *').remove();
                                    $('#csv-download-table > tbody > #report-filllevel-minute > .report-event-credential').append('<i class="material-icons">file_copy</i>');

                                    data['event_credential'].forEach(function (csv) {

                                        var report_file = csv.header + '\n';

                                        csv.file.forEach(function (line) {

                                            report_file += '' + line['hour_transaction'] + ',' + line['entries'] + '\n';

                                        });

                                        var hiddenElement = document.createElement('a');
                                        var blob = new Blob([report_file], {type: 'text/csv;charset=utf-8;'});
                                        hiddenElement.href = URL.createObjectURL(blob);
                                        hiddenElement.target = '_blank';
                                        hiddenElement.download = csv.filename;
                                        hiddenElement.click();

                                        $('#csv-download-table > tbody > #report-filllevel-minute > .report-event-credential > *').remove();
                                        $('#csv-download-table > tbody > #report-filllevel-minute > .report-event-credential').append('<i class="material-icons">save_alt</i>');

                                    });

                                }

                                return true;

                            } catch (error) {

                                console.log(error.message);

                                return false;

                            }

                        },
                        error: function(error) {

                            console.log('Error Fill Level Minute');
                            console.log(error);

                            return false;

                        }

                    }),

                    $.ajax({
                        type: 'POST',
                        url: '/reports/csvs/generate/entries_minute_by_title',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        dataType: 'json',
                        data: {
                            event_id: event_id,
                            event_park_id: event_park_id,
                            event_credential_id: event_credential_id,
                            generate: entries_minute_by_title
                        },
                        beforeSend: function() {

                            $('#csv-download-table > tbody > #report-ticket-entries-minute > .report-event > *').remove();
                            $('#csv-download-table > tbody > #report-ticket-entries-minute > .report-event-park > *').remove();
                            $('#csv-download-table > tbody > #report-ticket-entries-minute > .report-event-credential > *').remove();

                            if (entries_minute_by_title.includes('event')) {

                                $('#csv-download-table > tbody > #report-ticket-entries-minute > .report-event').append('<i class="material-icons">show_chart</i>');

                            } else {

                                $('#csv-download-table > tbody > #report-ticket-entries-minute > .report-event').append('<i class="material-icons">remove</i>');

                            }

                            if (entries_minute_by_title.includes('event-park')) {

                                $('#csv-download-table > tbody > #report-ticket-entries-minute > .report-event-park').append('<i class="material-icons">show_chart</i>');

                            } else {

                                $('#csv-download-table > tbody > #report-ticket-entries-minute > .report-event-park').append('<i class="material-icons">remove</i>');

                            }

                            if (entries_minute_by_title.includes('event-credential')) {

                                $('#csv-download-table > tbody > #report-ticket-entries-minute > .report-event-credential').append('<i class="material-icons">show_chart</i>');

                            } else {

                                $('#csv-download-table > tbody > #report-ticket-entries-minute > .report-event-credential').append('<i class="material-icons">remove</i>');

                            }

                        },
                        success: function (data) {

                            try {

                                if (entries_minute_by_title.includes('event')) {

                                    $('#csv-download-table > tbody > #report-ticket-entries-minute > .report-event > *').remove();
                                    $('#csv-download-table > tbody > #report-ticket-entries-minute > .report-event').append('<i class="material-icons">file_copy</i>');

                                    data['event'].forEach(function (csv) {

                                        var report_file = csv.header + '\n';

                                        csv.file.forEach(function (line) {

                                            report_file += '' + line['Socio'] + ',' + line['NSocio'] + ',' + line['Categoria'] + ',' + line['Titulo'] + '\n';

                                        });


                                        var hiddenElement = document.createElement('a');
                                        var blob = new Blob([report_file], {type: 'text/csv;charset=utf-8;'});
                                        hiddenElement.href = URL.createObjectURL(blob);
                                        hiddenElement.target = '_blank';
                                        hiddenElement.download = csv.filename;
                                        hiddenElement.click();

                                        $('#csv-download-table > tbody > #report-ticket-entries-minute > .report-event > *').remove();
                                        $('#csv-download-table > tbody > #report-ticket-entries-minute > .report-event').append('<i class="material-icons">save_alt</i>');

                                    });

                                }

                                if (entries_minute_by_title.includes('event-park')) {

                                    $('#csv-download-table > tbody > #report-ticket-entries-minute > .report-event-park > *').remove();
                                    $('#csv-download-table > tbody > #report-ticket-entries-minute > .report-event-park').append('<i class="material-icons">file_copy</i>');

                                    data['event_park'].forEach(function (csv) {

                                        var report_file = csv.header + '\n';

                                        csv.file.forEach(function (line) {

                                            report_file += '' + line['Socio'] + ',' + line['NSocio'] + ',' + line['Categoria'] + ',' + line['Titulo'] + '\n';

                                        });

                                        var hiddenElement = document.createElement('a');
                                        var blob = new Blob([report_file], {type: 'text/csv;charset=utf-8;'});
                                        hiddenElement.href = URL.createObjectURL(blob);
                                        hiddenElement.target = '_blank';
                                        hiddenElement.download = csv.filename;
                                        hiddenElement.click();

                                        $('#csv-download-table > tbody > #report-ticket-entries-minute > .report-event-park > *').remove();
                                        $('#csv-download-table > tbody > #report-ticket-entries-minute > .report-event-park').append('<i class="material-icons">save_alt</i>');

                                    });

                                }

                                if (entries_minute_by_title.includes('event-credential')) {

                                    $('#csv-download-table > tbody > #report-ticket-entries-minute > .report-event-credential > *').remove();
                                    $('#csv-download-table > tbody > #report-ticket-entries-minute > .report-event-credential').append('<i class="material-icons">file_copy</i>');

                                    data['event_credential'].forEach(function (csv) {

                                        var report_file = csv.header + '\n';

                                        csv.file.forEach(function (line) {

                                            report_file += '' + line['Socio'] + ',' + line['NSocio'] + ',' + line['Categoria'] + ',' + line['Titulo'] + '\n';

                                        });

                                        var hiddenElement = document.createElement('a');
                                        var blob = new Blob([report_file], {type: 'text/csv;charset=utf-8;'});
                                        hiddenElement.href = URL.createObjectURL(blob);
                                        hiddenElement.target = '_blank';
                                        hiddenElement.download = csv.filename;
                                        hiddenElement.click();

                                        $('#csv-download-table > tbody > #report-ticket-entries-minute > .report-event-credential > *').remove();
                                        $('#csv-download-table > tbody > #report-ticket-entries-minute > .report-event-credential').append('<i class="material-icons">save_alt</i>');

                                    });

                                }

                                return true;

                            } catch (error) {

                                console.log(error.message);

                                return false;

                            }

                        },
                        error: function(error) {

                            console.log('Error Entries Minute By Title');
                            console.log(error);

                            return false;

                        }

                    }),

                    $.ajax({
                        type: 'POST',
                        url: '/reports/csvs/generate/transactions_minute_entries',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        dataType: 'json',
                        data: {
                            event_id: event_id,
                            event_park_id: event_park_id,
                            event_credential_id: event_credential_id,
                            generate: transactions_minute_entries
                        },
                        beforeSend: function() {

                            $('#csv-download-table > tbody > #report-transactions-ticket-entry-minute > .report-event > *').remove();
                            $('#csv-download-table > tbody > #report-transactions-ticket-entry-minute > .report-event-park > *').remove();
                            $('#csv-download-table > tbody > #report-transactions-ticket-entry-minute > .report-event-credential > *').remove();

                            if (transactions_minute_entries.includes('event')) {

                                $('#csv-download-table > tbody > #report-transactions-ticket-entry-minute > .report-event').append('<i class="material-icons">show_chart</i>');

                            } else {

                                $('#csv-download-table > tbody > #report-transactions-ticket-entry-minute > .report-event').append('<i class="material-icons">remove</i>');

                            }

                            if (transactions_minute_entries.includes('event-park')) {

                                $('#csv-download-table > tbody > #report-transactions-ticket-entry-minute > .report-event-park').append('<i class="material-icons">show_chart</i>');

                            } else {

                                $('#csv-download-table > tbody > #report-transactions-ticket-entry-minute > .report-event-park').append('<i class="material-icons">remove</i>');

                            }

                            if (transactions_minute_entries.includes('event-credential')) {

                                $('#csv-download-table > tbody > #report-transactions-ticket-entry-minute > .report-event-credential').append('<i class="material-icons">show_chart</i>');

                            } else {

                                $('#csv-download-table > tbody > #report-transactions-ticket-entry-minute > .report-event-credential').append('<i class="material-icons">remove</i>');

                            }

                        },
                        success: function (data) {

                            try {


                                if (transactions_minute_entries.includes('event')) {

                                    $('#csv-download-table > tbody > #report-transactions-ticket-entry-minute > .report-event > *').remove();
                                    $('#csv-download-table > tbody > #report-transactions-ticket-entry-minute > .report-event').append('<i class="material-icons">file_copy</i>');

                                    data['event'].forEach(function (csv) {

                                        var report_file = csv.header + '\n';

                                        csv.file.forEach(function (line) {

                                            report_file += '' + line['datahora'] + ',' + line['tipo'] + ',' + line['porta'] + ',' + line['dac'] + ',' + line['socio'] + ',' + line['nsocio'] + ',' + line['categoria'] + '\n';

                                        });


                                        var hiddenElement = document.createElement('a');
                                        var blob = new Blob([report_file], {type: 'text/csv;charset=utf-8;'});
                                        hiddenElement.href = URL.createObjectURL(blob);
                                        hiddenElement.target = '_blank';
                                        hiddenElement.download = csv.filename;
                                        hiddenElement.click();

                                        $('#csv-download-table > tbody > #report-transactions-ticket-entry-minute > .report-event > *').remove();
                                        $('#csv-download-table > tbody > #report-transactions-ticket-entry-minute > .report-event').append('<i class="material-icons">save_alt</i>');

                                    });

                                }

                                if (transactions_minute_entries.includes('event-park')) {

                                    $('#csv-download-table > tbody > #report-transactions-ticket-entry-minute > .report-event-park > *').remove();
                                    $('#csv-download-table > tbody > #report-transactions-ticket-entry-minute > .report-event-park').append('<i class="material-icons">file_copy</i>');

                                    data['event_park'].forEach(function (csv) {

                                        var report_file = csv.header + '\n';

                                        csv.file.forEach(function (line) {

                                            report_file += '' + line['datahora'] + ',' + line['tipo'] + ',' + line['porta'] + ',' + line['dac'] + ',' + line['socio'] + ',' + line['nsocio'] + ',' + line['categoria'] + '\n';

                                        });

                                        var hiddenElement = document.createElement('a');
                                        var blob = new Blob([report_file], {type: 'text/csv;charset=utf-8;'});
                                        hiddenElement.href = URL.createObjectURL(blob);
                                        hiddenElement.target = '_blank';
                                        hiddenElement.download = csv.filename;
                                        hiddenElement.click();

                                        $('#csv-download-table > tbody > #report-transactions-ticket-entry-minute > .report-event-park > *').remove();
                                        $('#csv-download-table > tbody > #report-transactions-ticket-entry-minute > .report-event-park').append('<i class="material-icons">save_alt</i>');

                                    });

                                }

                                if (transactions_minute_entries.includes('event-credential')) {

                                    $('#csv-download-table > tbody > #report-transactions-ticket-entry-minute > .report-event-credential > *').remove();
                                    $('#csv-download-table > tbody > #report-transactions-ticket-entry-minute > .report-event-credential').append('<i class="material-icons">file_copy</i>');

                                    data['event_credential'].forEach(function (csv) {

                                        var report_file = csv.header + '\n';

                                        csv.file.forEach(function (line) {

                                            report_file += '' + line['datahora'] + ',' + line['tipo'] + ',' + line['porta'] + ',' + line['dac'] + ',' + line['socio'] + ',' + line['nsocio'] + ',' + line['categoria'] + '\n';

                                        });

                                        var hiddenElement = document.createElement('a');
                                        var blob = new Blob([report_file], {type: 'text/csv;charset=utf-8;'});
                                        hiddenElement.href = URL.createObjectURL(blob);
                                        hiddenElement.target = '_blank';
                                        hiddenElement.download = csv.filename;
                                        hiddenElement.click();

                                        $('#csv-download-table > tbody > #report-transactions-ticket-entry-minute > .report-event-credential > *').remove();
                                        $('#csv-download-table > tbody > #report-transactions-ticket-entry-minute > .report-event-credential').append('<i class="material-icons">save_alt</i>');

                                    });

                                }

                                return true;

                            } catch (error) {

                                console.log(error.message);

                                return false;

                            }

                        },
                        error: function(error) {

                            console.log('Error Transactions Minute Entries');
                            console.log(error);

                            return false;

                        }

                    }),

                ).then(function() {

                    console.log('Request Complete');

                    $('#loading-csvs').css('display', 'none');

                });

            }

        });

    </script>

@endsection