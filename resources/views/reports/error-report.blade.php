<style scoped>

    #report-load-charts > .mdl-snackbar__text {

        width: 100%;
        text-align: center;

    }

    #report-load-charts > .mdl-snackbar__action {

        display: none;

    }

    #report-load-charts {
        border-radius: 100px;
    }

</style>

@extends('layouts.dashboard')

@section('template_title')
    {{ trans('reports.showing_reports') }}
@endsection

@section('template_linked_css')
@endsection

@section('header')
    {{ trans('reports.showing_reports') }}
@endsection

@section('breadcrumbs')

    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <a itemprop="item" href="{{ url('/') }}">
            <span itemprop="name">
                {{ trans('titles.app') }}
            </span>
        </a>
        <i class="material-icons">chevron_right</i>
        <meta itemprop="position" content="1" />
    </li>

    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <a itemprop="item" href="/reports">
            <span itemprop="name">
                {{ trans('reports.reports_text')  }}
            </span>
        </a>
        <i class="material-icons">chevron_right</i>
        <meta itemprop="position" content="2" />
    </li>

    <li class="active" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <a itemprop="item" href="/reports/{{ $report->id }} . /generate">
        <span itemprop="name">
            {{ $report->description }}
        </span>
        </a>
        <meta itemprop="position" content="3" />
    </li>

@endsection

@section('content')

    <div id="report-options" class="mdl-grid full-grid margin-top-0 padding-0">
        <div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--12-col-desktop mdl-card mdl-shadow--3dp margin-top-0 padding-top-0">
            <div class="mdl-card card-new-report" style="width:100%;" itemscope itemtype="http://schema.org/Person">

                <div class="mdl-card__title mdl-card--expand mdl-color--primary mdl-color-text--white">
                    <h2 class="mdl-card__title-text logo-style">{{ trans('reports.generate_report') }}</h2>
                </div>

                <div class="mdl-card__supporting-text">
                    <div class="mdl-grid full-grid padding-0">
                        <div class="mdl-cell mdl-cell--12-col-phone mdl-cell--12-col-tablet mdl-cell--12-col-desktop">

                            <div id="report-details" class="mdl-grid ">

                                <div class="mdl-cell mdl-cell--12-col-tablet mdl-cell--12-col-desktop">
                                    <div class="event-options mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-select mdl-select__fullwidth is-dirty">
                                        <select id="report-event" class="mdl-selectfield__select mdl-textfield__input" name="report-event">
                                        </select>
                                        <label for="report-event">
                                            <i class="mdl-icon-toggle__label material-icons">arrow_drop_down</i>
                                        </label>
                                        {!! Form::label('event', trans('forms.label-event_id'), array('class' => 'mdl-textfield__label mdl-selectfield__label')); !!}
                                        <span id="error-select-event" class="mdl-textfield__error">{{ trans('reports.error_event')  }}</span>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>

                <div class="mdl-card__menu mdl-color-text--white">

                    <span class="save-actions">
                        {!! Form::button('<i class="material-icons">vertical_align_bottom</i>', array('id' => 'download-report', 'class' => 'dialog-button-icon-save mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect', 'disabled' => 'disabled', 'title' => trans('reports.view_report')  )) !!}
                    </span>

                    <a href="{{ url('/reports/') }}" class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect mdl-color-text--white" title="{{ trans('reports.back_to_reports') }}">
                        <i class="material-icons">reply</i>
                        <span class="sr-only">{{ trans('reports.back_to_reports') }}</span>
                    </a>

                </div>

            </div>
        </div>
    </div>

    <div id="report-load-charts" class="mdl-js-snackbar mdl-snackbar">
        <div class="mdl-snackbar__text"></div>
        <button class="mdl-snackbar__action" type="button"></button>
    </div>

@endsection

@section('footer_scripts')

    <script>

        $('#report-event').on('change', function () {

            removeSelectionError('event');
            validateReportLoading();

        });

        function validateReportLoading() {

            var event_id = $('#report-event option:selected').val();

            if (event_id !== "undefined" && event_id != '') {

                removeSelectionError('event');

                loadReport(event_id);

            } else {

                eventsSelectionError(event_id);

            }

        }

        function removeSelectionError(event_type) {

            if (event_type == 'event') {

                var event_id = $('#report-event option:selected').val();

                if (event_id !== "undefined" && event_id != '') {

                    document.getElementById("error-select-event").style.visibility = "hidden";
                    $('#report-event').parent().removeClass('is-invalid');

                }

            }

        }

        function eventsSelectionError(event_id) {

            if (event_id == "undefined" || event_id == '') {
                document.getElementById("error-select-event").style.visibility = "visible";
                $('#report-event').parent().addClass('is-invalid');
            }

        }

        function loadReport(event_id) {

            var snackbarContainer = document.querySelector('#report-load-charts');

            var data = {message: '{{ trans('reports.loading_charts') }}' };

            snackbarContainer.MaterialSnackbar.showSnackbar(data);

            $('#imports-entries-by-ticket-type tbody > *').remove();
            $('#entries-by-masters tbody > *').remove();
            $('#ticket-types-entries-list tbody > *').remove();
            $('#top-entities-credentials tbody > *').remove();

            $('#report-event').attr('disabled', 'disabled');
            $('#report-event-park').attr('disabled', 'disabled');
            $('#report-event-credential').attr('disabled', 'disabled');
            $('#view-report').attr('disabled', 'disabled');

            $('.logo-style').parent().append('<div id="loading-report-animation" class="mdl-spinner mdl-spinner--single-color mdl-js-spinner is-active"></div>');

            componentHandler.upgradeDom();

            $.ajax({

                url: '/event/' + event_id + '/report/errors/',
                dataType: 'json',
                success: function (data) {

                    var csv = 'Socio,NSocio,Categoria,Porta,Titulo,Tipo,Erro,Mensagem_Blacklist,Mensagem_blacklist_DAC,Datetime,DAC\n';

                    data.errors.forEach(function(error) {

                        csv += '' + error.Socio + ',' + error.NSocio + ',' + error.Categoria + ',' + error.Porta + ',' + error.Titulo + ',' + error.Tipo + ',' + error.Erro + ',' + error.Mensagem_Blacklist + ',' + error.Mensagem_Blacklist_DAC + ',' + error.Datetime + ',' + error.DAC + '\n';

                    });

                    var hiddenElement = document.createElement('a');
                    var blob = new Blob([csv],{type: 'text/csv;charset=utf-8;'});
                    hiddenElement.href = URL.createObjectURL(blob);
                    hiddenElement.target = '_blank';
                    hiddenElement.download = data.filename;
                    hiddenElement.click();

                },
                error: function(error) {

                   console.log('Error');
                   console.log(error);

                }

            });

            $('#report-event').removeAttr('disabled');
            $('#download-report').removeAttr('disabled');

        }

    </script>

@endsection