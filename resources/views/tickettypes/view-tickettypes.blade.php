@extends('layouts.dashboard')

@section('template_title')
    {{ trans('titles.view-ticket-type') }} . ' [ ' {{ $tickettype->description}} . ' ]'
@endsection

@section('header')
    {{ trans('titles.view-ticket-type') }} [ {{ $tickettype->description}} ]
@endsection

@section('breadcrumbs')
    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <a itemprop="item" href="{{url('/')}}">
        <span itemprop="name">
            {{ trans('titles.app') }}
        </span>
        </a>
        <i class="material-icons">chevron_right</i>
        <meta itemprop="position" content="1" />
    </li>
    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <a itemprop="item" href="/tickettypes">
        <span itemprop="name">
            {{ trans('tickettypes.showing_tickettypes') }}
        </span>
        </a>
        <i class="material-icons">chevron_right</i>
        <meta itemprop="position" content="2" />
    </li>
    <li class="active" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <a itemprop="item" href="/tickettypes/{{ $tickettype->id }} . /show">
        <span itemprop="name">
            {{ $tickettype->description }}
        </span>
        </a>
        <meta itemprop="position" content="3" />
    </li>
@endsection

@section('content')

<div class="mdl-grid full-grid margin-top-0 padding-0">

    <div class="mdl-card mdl-shadow--2dp mdl-cell mdl-cell--12-col mdl-cell--8-col-tablet mdl-cell--12-col-desktop margin-top-0">

        <div class="mdl-card__title mdl-color--primary mdl-color-text--white">
            <h2 class="mdl-card__title-text logo-style">
                {{ trans('tickettypes.details') }}
            </h2>
        </div>

        <div class="mdl-card__supporting-text mdl-color-text--grey-600 padding-0 context">

            <div class="table-responsive material-table">

                <table id="tickettype_table" class="mdl-data-table mdl-js-data-table data-table" cellspacing="0" width="100%">

                    <thead>
                        <tr>
                            <th class="mdl-data-table__cell--non-numeric">{{ trans('tickettypes.description') }}</th>
                            <th class="mdl-data-table__cell--non-numeric">{{ trans('tickettypes.value') }}</th>
                            <th class="mdl-data-table__cell--non-numeric">{{ trans('tickettypes.available_filters') }}</th>
                            <th class="mdl-data-table__cell--non-numeric">{{ trans('tickettypes.active') }}</th>
                        </tr>
                    </thead>

                    <tbody>
                           
                        <tr>

                            <td class="mdl-data-table__cell--non-numeric">{{ $tickettype->description }}</td>
                            <td class="mdl-data-table__cell--non-numeric">{{ $tickettype->value }}</td>
                            <td class="mdl-data-table__cell--non-numeric">
                                @if ($tickettype->available_filters)

                                    <i class="material-icons">done</i>

                                @else

                                    <i class="material-icons">clear</i>

                                @endif
                            </td>
                            <td class="mdl-data-table__cell--non-numeric">
                                @if ($tickettype->active)

                                    <i class="material-icons">done</i>

                                @else

                                    <i class="material-icons">clear</i>

                                @endif
                            </td>

                        </tr>

                    </tbody>

                </table>

            </div>
        
        </div>

        <div class="mdl-card__menu">

            <a href="{{ url('tickettypes/' . $tickettype->id . '/delete') }}" class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect mdl-color-text--white" title="{{ trans('tickettypes.delete') }}">
                <i class="material-icons">delete</i>
                <span class="sr-only">{{ trans('tickettypes.delete') }}</span>
            </a>

            <a href="{{ url('/tickettypes/' . $tickettype->id . '/edit') }}" class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect mdl-color-text--white" title="{{ trans('tickettypes.edit') }}">
                <i class="material-icons">edit</i>
                <span class="sr-only">{{ trans('tickettypes.edit') }}</span>
            </a>

            <a href="{{ url('/tickettypes') }}" class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-efect mdl-color-text--white" title="{{ trans('tickettypes.return') }}">
                <i class="material-icons">reply</i>
                <span class="sr-only"> {{ trans('tickettypes.return') }}</span>
            </a>

        </div>

    </div>

</div>

@endsection 