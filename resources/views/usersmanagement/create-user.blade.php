@extends('layouts.dashboard')

@section('template_title')
  Create New User
@endsection

@section('header')
  Create New User
@endsection

@section('breadcrumbs')
  <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
    <a itemprop="item" href="{{url('/')}}">
      <span itemprop="name">
        {{ trans('titles.app') }}
      </span>
    </a>
    <i class="material-icons">chevron_right</i>
    <meta itemprop="position" content="1" />
  </li>
  <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
    <a itemprop="item" href="/users">
      <span itemprop="name">
        Users List
      </span>
    </a>
    <i class="material-icons">chevron_right</i>
    <meta itemprop="position" content="2" />
  </li>
  <li class="active" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
    <a itemprop="item" href="/users/create">
      <span itemprop="name">
        Create New User
      </span>
    </a>
    <meta itemprop="position" content="3" />
  </li>
@endsection

@section('content')

  <div class="mdl-grid full-grid margin-top-0 padding-0">
    <div class="mdl-cell mdl-cell mdl-cell--12-col mdl-cell--12-col-phone mdl-cell--8-col-tablet mdl-cell--12-col-desktop mdl-card mdl-shadow--3dp margin-top-0 padding-top-0">
        <div class="mdl-card card-new-user" style="width:100%;" itemscope itemtype="http://schema.org/Person">

        <div class="mdl-card__title mdl-card--expand mdl-color--green mdl-color-text--white">
          <h2 class="mdl-card__title-text logo-style">Create New User</h2>
        </div>

        {!! Form::open(array('action' => 'UsersManagementController@store', 'method' => 'POST', 'role' => 'form')) !!}

          <div class="mdl-card__supporting-text">
            <div class="mdl-grid full-grid padding-0">
              <div class="mdl-cell mdl-cell--12-col-phone mdl-cell--12-col-tablet mdl-cell--12-col-desktop">

                <div class="mdl-grid ">

                  <div class="mdl-cell mdl-cell--4-col-tablet mdl-cell--6-col-desktop">
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label {{ $errors->has('name') ? 'is-invalid' :'' }}">
                      {!! Form::text('name', NULL, array('id' => 'name', 'class' => 'mdl-textfield__input', 'pattern' => '[A-Z,a-z,0-9]*')) !!}
                      {!! Form::label('name', trans('auth.name') , array('class' => 'mdl-textfield__label')); !!}
                      <span class="mdl-textfield__error">Letters and numbers only</span>
                    </div>
                  </div>

                  <div class="mdl-cell mdl-cell--4-col-tablet mdl-cell--6-col-desktop">
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label {{ $errors->has('email') ? 'is-invalid' :'' }}">
                      {!! Form::email('email', NULL, array('id' => 'email', 'class' => 'mdl-textfield__input')) !!}
                      {!! Form::label('email', trans('auth.email') , array('class' => 'mdl-textfield__label')); !!}
                      <span class="mdl-textfield__error">Please Enter a Valid {{ trans('auth.email') }}</span>
                    </div>
                  </div>
                  <div class="mdl-cell mdl-cell--4-col-tablet mdl-cell--6-col-desktop">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label {{ $errors->has('first_name') ? 'is-invalid' :'' }}">
                            {!! Form::text('first_name', NULL, array('id' => 'first_name', 'class' => 'mdl-textfield__input', 'pattern' => '[A-Z,a-z]*')) !!}
                            {!! Form::label('first_name', 'First Name', array('class' => 'mdl-textfield__label')); !!}
                            <span class="mdl-textfield__error">Letters only</span>
                        </div>
                    </div>
                    <div class="mdl-cell mdl-cell--4-col-tablet mdl-cell--6-col-desktop">
                      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label {{ $errors->has('last_name') ? 'is-invalid' :'' }}">
                          {!! Form::text('last_name', NULL, array('id' => 'last_name', 'class' => 'mdl-textfield__input', 'pattern' => '[A-Z,a-z]*')) !!}
                          {!! Form::label('last_name', 'Last Name', array('class' => 'mdl-textfield__label')); !!}
                          <span class="mdl-textfield__error">Letters only</span>
                      </div>
                    </div>
                    {{-- <div class="mdl-cell mdl-cell--4-col-tablet mdl-cell--6-col-desktop">
                      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label {{ $errors->has('twitter_username') ? 'is-invalid' :'' }}">
                          {!! Form::text('twitter_username', NULL, array('id' => 'twitter_username', 'class' => 'mdl-textfield__input')) !!}
                          {!! Form::label('twitter_username', 'Twitter Username', array('class' => 'mdl-textfield__label')); !!}
                      </div>
                    </div>
                    <div class="mdl-cell mdl-cell--4-col-tablet mdl-cell--6-col-desktop">
                      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label {{ $errors->has('github_username') ? 'is-invalid' :'' }}">
                          {!! Form::text('github_username', NULL, array('id' => 'github_username', 'class' => 'mdl-textfield__input')) !!}
                          {!! Form::label('github_username', 'GitHub Username', array('class' => 'mdl-textfield__label')); !!}
                      </div>
                    </div> --}}

                    <div class="mdl-cell mdl-cell--4-col-tablet mdl-cell--6-col-desktop">
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label margin-bottom-1 {{ $errors->has('location') ? 'is-invalid' :'' }}">
                        {!! Form::text('location', NULL, array('id' => 'location', 'class' => 'mdl-textfield__input' )) !!}
                        {!! Form::label('location', 'User Location', array('class' => 'mdl-textfield__label')); !!}
                      <span class="mdl-textfield__error">Please Enter a Valid Location</span>
                    </div>
                  </div>

                  <div class="mdl-cell mdl-cell--4-col-tablet mdl-cell--6-col-desktop">
                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-select mdl-select__fullwidth {{ $errors->has('role') ? 'is-invalid' :'' }}">
                      <select class="mdl-selectfield__select mdl-textfield__input" name="role" id="role">
                        <option value=""></option>
                        @if ($roles->count())
                          @foreach($roles as $role)
                            <option value="{{ $role->id }}">{{ $role->name }}</option>
                          @endforeach
                        @endif
                      </select>
                      <label for="role">
                          <i class="mdl-icon-toggle__label material-icons">arrow_drop_down</i>
                      </label>
                      {!! Form::label('role', trans('forms.label-userrole_id'), array('class' => 'mdl-textfield__label mdl-selectfield__label')); !!}
                      <span class="mdl-textfield__error">Select user access level</span>
                    </div>
                  </div>

                  <div class="mdl-cell mdl-cell--4-col-tablet mdl-cell--6-col-desktop">
                      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label {{ $errors->has('password') ? 'is-invalid' :'' }}">
                          {!! Form::password('password', array('id' => 'password', 'class' => 'mdl-textfield__input')) !!}
                          {!! Form::label('password', 'Password', array('class' => 'mdl-textfield__label')); !!}
                        <span class="mdl-textfield__error">Please enter a valid password</span>
                      </div>
                  </div>

                  <div class="mdl-cell mdl-cell--4-col-tablet mdl-cell--6-col-desktop">
                      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label {{ $errors->has('password_confirmation') ? 'is-invalid' :'' }}">
                          {!! Form::password('password_confirmation', array('id' => 'password_confirmation', 'class' => 'mdl-textfield__input')) !!}
                          {!! Form::label('password_confirmation', 'Confirm Password', array('class' => 'mdl-textfield__label')); !!}
                        <span class="mdl-textfield__error">Must match password</span>
                      </div>
                  </div>

                  <div class="mdl-cell mdl-cell--12-col">
                      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label {{ $errors->has('bio') ? 'is-invalid' :'' }}">
                          {!! Form::textarea('bio',  NULL, array('id' => 'bio', 'class' => 'mdl-textfield__input')) !!}
                          {!! Form::label('bio', 'Bio', array('class' => 'mdl-textfield__label')); !!}
                      </div>
                  </div>

                  <div class="mdl-cell mdl-cell--6-col">

                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-select mdl-select__fullwidth {{ $errors->has('installation') ? 'is-invalid' :'' }}">
                      <select class="mdl-selectfield__select mdl-textfield__input" name="installation" id="installation">
                        <option value="0"></option>
                        @if ($installations)
                          @foreach($installations as $installation)
                            <option value="{{ $installation->installation_id }}">{{ $installation->name }}</option>
                          @endforeach
                        @endif
                      </select>
                      <label for="installation">
                        <i class="mdl-icon-toggle__label material-icons">arrow_drop_down</i>
                      </label>
                      {!! Form::label('role', trans('usersmanagement.installation'), array('class' => 'mdl-textfield__label mdl-selectfield__label')); !!}
                      <span class="mdl-textfield__error">{{ trans('usersmanagement.required') }}</span>
                    </div>

                  </div>

                  <div class="mdl-cell mdl-cell--6-col">

                      <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label mdl-select mdl-select__fullwidth {{ $errors->has('venue') ? 'is-invalid' :'' }}">
                        <select class="mdl-selectfield__select mdl-textfield__input" name="venue" id="venue">
                          <option value="0"></option>
                          @if ($venues)
                            @foreach($venues as $venue)
                              <option value="{{ $venue['venue_id'] }}">{{ $venue['name'] }}</option>
                            @endforeach
                          @endif
                        </select>
                        <label for="venue">
                          <i class="mdl-icon-toggle__label material-icons">arrow_drop_down</i>
                        </label>
                        {!! Form::label('role', trans('usersmanagement.venueAssociated'), array('class' => 'mdl-textfield__label mdl-selectfield__label')); !!}
                        <span class="mdl-textfield__error">{{ trans('usersmanagement.required') }}</span>
                      </div>

                  </div>

                </div>

              </div>

            </div>
          </div>

          <div class="mdl-card__actions padding-top-0">
            <div class="mdl-grid padding-top-0">
              <div class="mdl-cell mdl-cell--12-col padding-top-0 margin-top-0 margin-left-1-1">

                {{-- SAVE BUTTON--}}
                <span class="save-actions">
                  {!! Form::button('<i class="material-icons">save</i> Save New User', array('class' => 'dialog-button-save mdl-button mdl-js-button mdl-js-ripple-effect mdl-color--green mdl-color-text--white mdl-button--raised margin-bottom-1 margin-top-1 margin-top-0-desktop margin-right-1 padding-left-1 padding-right-1 ')) !!}
                </span>

              </div>
            </div>
          </div>

            <div class="mdl-card__menu mdl-color-text--white">

              <span class="save-actions">
                {!! Form::button('<i class="material-icons">save</i>', array('class' => 'dialog-button-icon-save mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect', 'title' => 'Save New User')) !!}
              </span>

              <a href="{{ url('/users/') }}" class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect mdl-color-text--white" title="Back to Users">
                  <i class="material-icons">reply</i>
                  <span class="sr-only">Back to Users</span>
              </a>

            </div>

            @include('dialogs.dialog-save')

          {!! Form::close() !!}

        </div>
    </div>
  </div>

@endsection

@section('footer_scripts')

  @include('scripts.mdl-required-input-fix')
  @include('scripts.gmaps-address-lookup-api3')

  <script type="text/javascript">
    mdl_dialog('.dialog-button-save');
    mdl_dialog('.dialog-button-icon-save');

    window.onload = function() {

      //loadVenues();

    }

    function loadVenues() {

      $.ajax({
        url: '/venues/get',
        dataType: 'json',
        beforeSend: function () {

          //$('#events-list > *').remove();

          //var loading = '<div class="spinner"><div class="mdl-spinner mdl-spinner--single-color mdl-js-spinner is-active"></div></div>';
          //$('#events-list').append(loading);

          //componentHandler.upgradeDom();

        },
        success: function (data) {

          console.log(data);

          var venues = '';

          data.forEach(function (venue) {

            //venues += '<li class="mdl-menu__item event-option" data-val="' + venue.venue_id + '">' + venue.name + '</li>';

            venues += '	<li class="mdl-menu__item event-option" data-val="' + venue.venue_id + '">' + venue.name + '</li>';

          });

          $('.venue-options').append(venues);

          componentHandler.upgradeDom();

          /*

          if (data['error']) {

              $('#events-list > .spinner').remove();

              var events =	''+
                  '<div id="event-options" class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height">' +
                  '	<input type="text" value="" class="mdl-textfield__input" id="event-select" readonly>' +
                  '	<input type="hidden" value="" name="event-select">' +
                  '	<label for="event-select" class="mdl-textfield__label">' + trans_pt.error_loading + '</label>' +
                  '	<span id="error-select-event" class="mdl-textfield__error">' + trans_pt.event_required + '</span>' +
                  '</div>';

              $('#events-list').append(events);

          } else {

              $('#events-list > .spinner').remove();

              var events =	''+
                  '<div id="event-options" class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height">' +
                  '	<input type="text" value="" class="mdl-textfield__input" id="event-select" readonly>' +
                  '	<input type="hidden" value="" name="event-select">' +
                  '	<label for="event-select" class="mdl-textfield__label">' + trans_pt.event + '</label>' +
                  '	<span id="error-select-event" class="mdl-textfield__error">' + trans_pt.event_required + '</span>' +
                  '	<ul for="event-select" class="mdl-menu mdl-menu--bottom-left mdl-js-menu">';

              data.forEach(function (event) {

                  events += '	<li class="mdl-menu__item event-option" onclick="selectEvent(this)" data-val="' + event.id + '" data-code="' + event.code + '" data-lotation="' + event.total + '" data-open-gates="' + event.open_gates + '">' + event.descricao + '</li>';

              });

              events += 	'	</ul>' +
                  '</div>';



              $('#events-list').append(events);

          }

          componentHandler.upgradeDom();

          */

        },
        error: function (error) {

          $('#events-list > .spinner').remove();

          var events =	''+
                  '<div id="event-options" class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height">' +
                  '	<input type="text" value="" class="mdl-textfield__input" id="event-select" readonly>' +
                  '	<input type="hidden" value="" name="event-select">' +
                  '	<label for="event-select" class="mdl-textfield__label">' + trans.error_loading + '</label>' +
                  '	<span id="error-select-event" class="mdl-textfield__error">' + trans.event_required + '</span>' +
                  '</div>';

          $('#events-list').append(events);

          componentHandler.upgradeDom();

        }
      });

    }

  </script>

@endsection