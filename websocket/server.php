<?php

use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
use SocketEK\Websocket;

require dirname(__DIR__) . '/vendor/autoload.php';
require dirname(__DIR__) . '/websocket/Websocket.php';

$port = 8030;

$server = IoServer::factory(
    new HttpServer(
        new WsServer(
            new Websocket()
        )
    ),
    8030
);

$server->run();